# -*- coding: utf-8 -*-
import argparse
import cPickle
import torch
import json

# from model import MwAN
# from model_modifier import MwAN
# from painternerCollection.dmn import MwAN
# from painternerCollection.dmnPlus import MwAN
# from painternerCollection.SeparateDmn3 import MwAN
# from painternerCollection.alphaCnn6 import MwAN
# from painternerCollection.alphaCnn7 import MwAN
# from painternerCollection2.attentionPool2 import MwAN
# from painternerCollection2.CRNN import MwAN
# from painternerCollection2.CRNN2_1d5 import MwAN
# from painternerCollection2.fullrnn3 import MwAN, NoCudaEmbedding
# from painternerCollection2.beta0 import MwAN
# from ptmodel.model1 import MwAN
# from ptmodel.model3_SeperateSkip import MwAN
# from ptmodel.model5_SeperateSkip import MwAN
from temp_11_3.model6_11_3  import MwAN

from preprocess import process_data
from utils import *
from painternerCollection.utils import handle_data_for_train
from painternerlib.HyparaDebug import fgetHDebug,fputHDebug,fputHypara
from termcolor import colored
from painternerCollection.utils import printonece
from ptmodel.dataprocess.datalogic import DataLogic
from ptColl2.swats import Swats
from ptmodel.somedebug import fGrad_Debug
import random
import time


parser = argparse.ArgumentParser(description='PyTorch implementation for Multiway Attention Networks for Modeling '
                                             'Sentence Pairs of the AI-Challenges')

parser.add_argument('--data', type=str, default='data/',
                    help='location directory of the data corpus')
parser.add_argument('--threshold', type=int, default=5,
                    help='threshold count of the word')
parser.add_argument('--epoch', type=int, default=5000,
                    help='training epochs')
parser.add_argument('--emsize', type=int, default=128,
                    help='size of word embeddings')
parser.add_argument('--nhid', type=int, default=128,
                    help='hidden size of the model')
parser.add_argument('--batch_size', type=int, default=32, metavar='N',
                    help='batch size')
parser.add_argument('--log_interval', type=int, default=300,
                    help='# of batches to see the training error')
parser.add_argument('--dropout', type=float, default=0.3,
                    help='dropout applied to layers (0 = no dropout)')
parser.add_argument('--cuda', action='store_true',
                    help='use CUDA')
parser.add_argument('--patitialcuda', action='store_true',
                    help='use patitial CUDA')
parser.add_argument('--save', type=str, default='model.pt',
                    help='path to save the final model')
parser.add_argument('--model', type=str, default='model.pt',
                    help='model path')
parser.add_argument('--picklebuild', action='store_true',
                    help='build picklefile')
parser.add_argument('--restore', action='store_true',
                    help='build picklefile')
parser.add_argument('--plot', action='store_true',
                    help='print in plot')
parser.add_argument('--datascope', type=int, default=0,
                    help='# train and dev data ratio')                   


args = parser.parse_args()


global_Debug = fputHDebug({
    "__Seg__0": False,
    "__Seg__1": False
})
global_Hypara = fputHypara({
    "datalogic": 'direct',    ## 'direct' or 'logic1'
    "epoch_to_update_embedding": -1,
    "save_half_model":False,
    "eval_period": 1,
    "grad_debug": True,
    "grad_debug_period": -1,
    "continue_update_period": 1
})
global global_grad_debug_count
global_grad_debug_count = 0


if args.plot:
    from ptColl2.graphic import GraphicUpdate

if args.picklebuild:
    process_data(args.data, args.threshold)

with open("data/word2id.obj",'rb') as f:
    word2id_data = cPickle.load(f)
# vacab_size = 98745
vocab_size = len(word2id_data)
print("vacab_size",vocab_size)

if args.restore:    
    with open(args.model, 'rb') as f:
        print colored("restoring from model.pt","blue")
        model = torch.load(f)
else:              
    model = MwAN(vocab_size=vocab_size, embedding_size=args.emsize, encoder_size=args.nhid, drop_out=args.dropout)
    print('MwAN Model total parameters(explicit):', get_model_parameters(model))
    # embedmodel = NoCudaEmbedding(vocab_size=vocab_size,embedding_size=args.emsize)
    # print('embedmodel Model total parameters:', get_model_parameters(embedmodel))

if args.cuda:
    model.cuda()  ## will take several seconds
    model.setcuda()
elif args.patitialcuda:
    model.setcuda(patitial=True)
optimizer = torch.optim.Adamax(model.parameters())

# optimizer = torch.optim.Adam(model.parameters())
# optimizer = Swats(model.parameters())
# optimizer = torch.optim.SGD(model.parameters(),lr=0.001,momentum=0.9)
# torch.optim.lr_scheduler.MultiStepLR(optimizer,[1,2,3,4],gamma=0.2)
# scheduler = torch.optim.lr_scheduler.LambdaLR(optimizer,lr_lambda=lambda epoch: 0.2**epoch)
scheduler = torch.optim.lr_scheduler.LambdaLR(optimizer,lr_lambda=lambda epoch: 1)

with open(args.data + 'train.pickle', 'rb') as f:
    # train_data = cPickle.load(f)

    # # 从training中剥夺的30000 line数据，copy代码后只需运行一次。
    # from preprocess import seg_data, transform_data_to_id
    # with open('data/word2id.obj', 'rb') as f__:
    #     word2id = cPickle.load(f__)
    # raw_data = seg_data('data/develop/training_30000line.json',False)
    # transformed_data = transform_data_to_id(raw_data, word2id)
    # train_data = [x + [y[2]] for x, y in zip(transformed_data, raw_data)]  ## inference 可以index to word
    # with open('data/develop/training_30000line.pickle','wb') as f___:
    #     cPickle.dump(train_data,f___)

    with open('data/develop/training_30000line.pickle','rb') as f___:
        train_data = cPickle.load(f___)

with open(args.data + 'dev.pickle', 'rb') as f:
    dev_data = cPickle.load(f)

if args.datascope != 0:
    dev_data = dev_data[:args.datascope]
    train_data = train_data[:args.datascope]

dev_data = sorted(dev_data, key=lambda x: len(x[1]))

print('train data size {:d}, dev data size {:d}'.format(len(train_data), len(dev_data)))


assert global_Hypara['datalogic'] == "direct" or global_Hypara['datalogic'] == "logic1"
def get_iter(data,epoch=None,mode='train'):
    assert mode == 'train' or mode == 'eval'
    if global_Hypara['datalogic'] == "direct":
        for num, i in enumerate(range(0, len(data), args.batch_size)):
            one = data[i:i + args.batch_size]
            ## e.g.
            ##[[69463, 649, 70688, 27208, 133, 4457], [69463, 97, 1234, 141, 76338, 4987, 162, 19080, 2348, 27208, 2341,
            # 70688, 1287, 36034, 69463, 97, 1234, 1761, 14, 6094, 30931, 385, 20748, 52127, 588, 162, 69463, 162, 63491,
            #  202, 44085, 162, 6094, 15625, 49591, 91334, 70688, 20443, 94654], [[74162], [70688], [47052]], 563]
            yield num,one 
    elif global_Hypara['datalogic'] == "logic1":
        datalogic = DataLogic(data,vocab_size)
        if mode == 'eval':
            for num, one in enumerate(datalogic.eval_iter(args.batch_size)):
                yield num, one
        else:
            # 关于len(data)/args.batch_size的说明: 不要求精确，因为datalogic每取一项都是随机的，所以实际上不存在epoch的概念，所以也就没有缺少数据一说。
            for num, one in enumerate(datalogic(len(data)/args.batch_size,args.batch_size,epoch)):
                yield num, one
        
    else:
        raise Exception("get_iter error")

def train(epoch):
    point = global_Hypara['epoch_to_update_embedding']
    if point != -1:
        if epoch >= point:   
            for p in model.embedding.parameters():
                p.requires_grad = True 
        # if epoch <= 2:
        #     for p in model.embedding.parameters():
        #         p.requires_grad = False     ## 错了吧，没有训练过的要么标记<unk>要么设为True    
        # else:
        #     for p in model.embedding.parameters():
        #         p.requires_grad = True 

    scheduler.step()
    print(optimizer.state_dict()['param_groups'][0]['lr'])  ## 查看一下学习率。

    model.train()
    data = shuffle_data(train_data, 1)
    total_loss = 0.0    
    save_one_flag = False
    continue_update_times = 0
    continue_update_period = global_Hypara['continue_update_period']
    for num,one in get_iter(data,epoch):
        timeA = time.time()
        query,passage,answer,ids,label = handle_data_for_train(one,padding,pad_answer,global_Hypara,'train',True)
        timeB = time.time() - timeA
        if global_Debug['__Seg__0']: print "handle_data_for_train time", timeB
                
        continue_update_times += 1
        if continue_update_times > continue_update_period:
            continue_update_times = 0
        if num == 0:
            optimizer.zero_grad() ## 有可能上一次epoch没有optimizer.step()而残留
        if continue_update_times == 0:
            optimizer.zero_grad()  #Clears the gradients of all optimized torch.Tensor s.
        
        timeA = time.time()
        loss = model([query, passage, answer,ids,label,True],optimizer)
        timeB = time.time() - timeA
        if global_Debug['__Seg__1']: print "forward time", timeB

        timeA = time.time()
        loss.backward()        # 告诉优化器需要更新的方式？因为初始化optimizer把model所有parameters都加入，但不一定都想更新.
        timeB = time.time() - timeA
        if global_Debug['__Seg__1']: print "backward time", timeB

        global global_grad_debug_count
        global_grad_debug_count += 1
        if global_Hypara['grad_debug']:
            if global_Hypara['grad_debug_period'] != -1:
                if global_grad_debug_count % global_Hypara['grad_debug_period'] == 0:
                    fGrad_Debug(optimizer)

        total_loss += loss.item()

        if continue_update_times == continue_update_period:
            optimizer.step()       # parameter update

        timeA = time.time()
        optimizer.step()       # parameter update
        timeB = time.time() - timeA
        if global_Debug['__Seg__1']: print "optimizer step time", timeB

        if (num + 1) % args.log_interval == 0:
            print '|------epoch {:d} train error is {:f}  eclipse {:.2f}%------|'.format(epoch,
                                                                                         total_loss / args.log_interval,
                                                                                         num*args.batch_size * 100.0 / len(data))

            if args.plot:
                GraphicUpdate(total_loss/args.log_interval,1)
            total_loss = 0

            
            
        if (num*args.batch_size*100.0/len(data) >= 50):
            if(not save_one_flag):
                save_one_flag=True
                if global_Hypara['save_half_model']:
                    with open('half'+args.save, 'wb') as f:
                        print colored('save half model',"green")
                        torch.save(model, f)


def test():
    shuffle_answer = True
    model.eval()
    r, a = 0.0, 0.0
    r_relation = 0.0
    with torch.no_grad():
        for num,one in get_iter(dev_data,mode='eval'):

            query,passage,answer,ids,label = handle_data_for_train(one,padding,pad_answer,global_Hypara,'eval',shuffle_answer)
       
            output = model([query, passage, answer,ids,label, False],optimizer)

            predict_answer = output[0]
            predict_relation = output[1]

            label = torch.Tensor(label) 
            pos0 = torch.argmax(label[:,0:3,:].squeeze(-1),1)
            if global_Hypara['datalogic'] == "logic1":
                pos1 = torch.argmax(label[:,3:,:].squeeze(-1),1)
            if args.cuda:
                pos0 = pos0.cuda()
                if global_Hypara['datalogic'] == "logic1":
                    pos1 = pos1.cuda()
            r += torch.eq(predict_answer,pos0).sum().item()
            if global_Hypara['datalogic'] == "logic1":
                r_relation += torch.eq(predict_relation,pos1).sum().item()
            a += len(one)
            # if shuffle_answer:
            #     assert len(output.size()) == 1
            #     pos =torch.tensor(pos)
            #     if args.cuda:
            #         pos= pos.cuda()
            #     r += torch.eq(output,pos).sum().item()
            # else:
            #     r += torch.eq(output, 0).sum().item()
            # a += len(one)


            if (num + 1) % (args.log_interval*50) == 0:
                print num*args.batch_size * 100.0 / len(dev_data)
    return r * 100.0 / a, r_relation * 100.0/a


def main():
    best = 0.0
    eval_count = 0
    assert global_Hypara['eval_period'] > 0
    for epoch in range(args.epoch):
        train(epoch)
        eval_count += 1

        if eval_count % global_Hypara['eval_period'] == 0:
            acc, rela_acc= test()
            if args.plot:
                GraphicUpdate(acc,0)
            if acc > best:
                best = acc
                with open(args.save, 'wb') as f:
                    torch.save(model, f)
            print 'epcoh {:d} dev acc is {:f}, best dev acc {:f}, rela_acc {:f}'.format(epoch, acc, best,rela_acc)


if __name__ == '__main__':
    main()

"""
检查word2id 的0,1状态。
vocabsize 在datalogic没有加1，可能微小的问题。
检查 ]*n 的状态，这个东西由于共享n无法正确赋值。
a=[1]
a[0:] = [1] ( 不是 1)
"""