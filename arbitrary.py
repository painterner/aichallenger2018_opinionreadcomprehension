#!-*-coding:utf-8-*-
# from painternerCollection.utils4net import preEmbedding
import torch
import torch.nn as nn

# input = torch.LongTensor([1,2,3])
# A = preEmbedding(300)
# print A(input)


# import torch

# a=torch.tensor([1,2])
# b=torch.tensor([[1,2]])

# torch.cat([a,b])

# from torchtext.vocab import FastText

## put language model to jar path befor run
## 速度有点慢,后续常识大一点的文件一块处理看一看速度。
# from nltk.tree import Tree
# from stanfordcorenlp import StanfordCoreNLP as STNLP 
# s = "我叫小米"
# s = "蝙蝠是翼手目动物的总称，翼手目是哺乳动物中仅次于啮齿目动物的第二大类群，是唯一一类演化出真正有飞翔能力的哺乳动物，现生物种类共有19科185属962种，除极地和大洋中的一些岛屿外，分布遍于全世界，在热带和亚热带蝙蝠最多。蝙蝠是哺乳动物，体温恒定的"
# with STNLP(r'/mnt/u16/home/ka/Documents/ref/standfordcorenlp/stanford-corenlp-full-2018-10-05',lang='zh') as nlp:
#     S = nlp.parse(s)
#     print S
#     Tree.fromstring(S).draw()

# import time

# timeA = time.time()
# print timeA


# class A(nn.Module):
#     def __init__(self):
#         super(A,self).__init__()

#         self.m = nn.ModuleList([
#             nn.Linear(2,3)
#         ])
#         self.m1 = nn.Linear(3,4)

#         self.inition()
#     def inition(self):
#         for i in self.modules():
#             if isinstance(i,nn.Linear):
#                 print i

# a=A()

# ## optim merge 测试
# import torch
# import torch.nn as nn
# import random
# i = torch.ones(4,3,2)
# m = nn.Linear(2,3)
# m.weight.data.fill_(torch.ones(1)[0])
# m.bias.data.fill_(torch.ones(1)[0])
# optim = torch.optim.Adamax(m.parameters())

# print m.weight,m.bias
# print "every"
# for _ in range(1):
#     optim.zero_grad()
#     loss = m(i).view(i.size(0),-1).mean(-1)
#     loss = loss.sum()
#     loss.backward()
#     optim.step()
#     print m.weight,m.bias

# print "merge"
# optim.zero_grad()
# m.weight.data.fill_(torch.ones(1)[0])
# m.bias.data.fill_(torch.ones(1)[0])
# for _ in range(4):
#     loss  = m(i[0:1,:,:]).mean()
#     loss.backward()
# optim.step()
# print m.weight,m.bias

# """
# problem: 只要同时改变i.size(0)和 merge迭代次数，打印值就保持不变，为何. 与batch大小有关？
# """

# class A(nn.Module):
#     def __init__(self):
#         super(A,self).__init__()
#         self.m = nn.ParameterList([
#             nn.Parameter(torch.randn(2,2)) for _ in range(2)
#         ])
#     def forward(self):
#         pass


# a = A()
# print a.m[0].type()
# a.cuda()
# print a.m[0].type()

class A(nn.Module):
    def __init__(self):
        super(A,self).__init__()
        self.m = nn.ParameterList([
            nn.Parameter(torch.randn(2,2)) for _ in range(2)
        ])
    def inition(self):
        for p in self.parameters():
            print nn.init.xavier_uniform_(p,0.1)
    def forward(self):
        pass


a = A()
for n,p in a.named_parameters():
    print n,p
a.inition()
