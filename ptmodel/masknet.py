#!-*-coding:utf-8-*-
import torch
import torch.nn as nn
import torch.nn.functional as F
import random

class MaskOut(nn.Module):
    def __init__(self):
        super(MaskOut,self).__init__()

class MaskInplace(nn.Module):
    """
        考虑过使用dropout代替，但是dropout会scale数据，而我们需要的只是替换一些数据而已.
    """
    def __init__(self,p=0.1,zero_value=False):
        """
            p : probabilty
            zero_value： if True, use zero in-place, else use random in-place
        """
        super(MaskInplace,self).__init__()
        self.p = p
        self.zero_value = zero_value
        print("use MaskInplace !! probability is",self.p)
        # self.m = nn.Dropout(p,inplace=True)
    def randpos(self,tensor_shape):
        toreturn = []
        for m in tensor_shape:
            pos = random.randint(0,m-1)
            toreturn.append(pos)
        return tuple(toreturn)
            
    def BernoulliSample(self,tensor):
        sampler = torch.distributions.bernoulli.Bernoulli(torch.ones_like(tensor)*self.p)
        return int(sampler.sample().sum())

    def forward(self,model):
        if not self.training:
            # print "forward eval"
            return
        else:
            # print "forward train"
            for param in model.parameters():
                cuda = False
                if 'cuda' in param.type():
                    cuda = True
                # print param.sum()
                count = self.BernoulliSample(param)
                # print count
                for _ in range(count):
                    assert param.size() > 0
                    pos = self.randpos(param.size())
                    # print "pos", pos
                    if self.zero_value:
                        value = torch.tensor(0.,dtype=param.dtype)
                        # print "zero"
                    else:
                        value = torch.tensor(random.random()-0.5, dtype = param.dtype )
                    if cuda:
                        value = value.cuda()
                    param.data[pos] = value
                # print param.sum()

class MaskInplaceTest(nn.Module):
    def __init__(self,p,zero_value=False):
        super(MaskInplaceTest,self).__init__()
        self.model = nn.Linear(2,3)
        self.maskinplace = MaskInplace(p,zero_value=zero_value)
    def forward(self):
        print "TrainTest", self.training
        print list(self.parameters())
        self.maskinplace(self)
        print list(self.parameters())
        
def TestMaskInplace():
    m= MaskInplaceTest(0.1,True)
    print "train"
    m.train()
    m()
    print "eval"
    m.eval()
    m()


class MaskDropoutTest(nn.Module):
    def __init__(self,p=0.5,inplace=False):
        """
        注意baseline的F.dropout(x,p)有错，应该写成F.dropout(x,p,training=True)
        另外可能用到F.dropout2D
        """
        super(MaskDropoutTest,self).__init__()
        self.model = nn.Linear(2,3)
        self.inputs = torch.ones(3,2)
        self.dropout = nn.Dropout(p=p,inplace=inplace)
    def output(self):
        return self.model(self.inputs)
    def forward(self):
        for _ in range(2):
            z = self.model(self.inputs)
            print z
            z = self.dropout(z)
            # z = F.dropout(z,p=0.5,training=True)
            print z


def TestDropoutOpsIsOnlyInputTensor():
    a=torch.ones(2,3)
    b=torch.ones(2,3)
    c = a+b
    m = nn.Dropout(p=0.2)
    for _ in range(10):  
        print m(c)       ## have 2.5 (not have 1.25 )
    for _ in range(10):
        print m(a)+m(b)  ## have 1.25 or 2.5 or 0

def TestDropoutModelEval():
    m = MaskDropoutTest()
    print "internal training"
    m()
    m.eval()
    print "internal eval"
    m()
    d = nn.Dropout(p=0.5)
    print "external dropout", d(m.output())
    ## 外部的dropout不受m的影响。
    ## 另外如果模块类内部使用的是F.dropout也是不受m.train, m.eval影响的，需要自己定义逻辑。
    ## 总结，用nn.Dropout比较好，但是要在模块内部初始化函数中初始化才行。



if __name__ == "__main__":
    # TestDropoutModelEval()
    TestMaskInplace()



