#!-*-coding:utf-8-*-
import torch
import torch.nn as nn

class NewEncoder(nn.Module):
    def __init__(self,indim,outdim,mode='simple'):
        """
            mode : 'cnn', 'simple', or '3W'
            simple:
            1       2       3       4
            |_______|_______|       |
                |   |_______|_______|
                |___|
            前3个的总结作为下一次的输入（所以每次输入长度为4)


        """
        super(NewEncoder,self).__init__()
        self.indim = indim
        self.outdim = outdim
        self.model = nn.GRU(input_size=2*self.indim,hidden_size=self.outdim,
                                batch_first=True,bidirectional=True)
        self.Wsynthesize = nn.Linear(2*self.indim,2*self.indim)   ## '三角形??'
        # self.Wextend = nn.Linear(indim,2*self.indim)

    def forward(self,inputs):
        """
            input shape: [B,L,M]
            output shape: [B,Ls/3,M]
            提醒，取消最大长度限制
        """
        ohi = None
        toreturn = []
        synthe = torch.zeros_like(inputs[:,0:1,:])
        for i in range(inputs.size(1)):
            condi1 =  (i+1)%3 == 0 
            condi2 =  i==inputs.size(1)-1 and (i+1)%3 != 0
            if condi1 or condi2 :
                if condi1:
                    inp = torch.cat([synthe,inputs[:,i-2:i+1,:]],1) ## length: 1+3
                else:
                    inp = torch.cat([synthe,inputs[:,-inputs.size(1)%3:,:]],1) ## length: 1+3
                out,ohi = self.model(inp,ohi)
                synthe = self.Wsynthesize(out).mean(1,keepdim=True)
                toreturn.append(synthe)
        toreturn = torch.cat(toreturn,1)
        # toreturn = self.Wextend(toreturn)
        return toreturn,ohi
        

class TwoGru(nn.Module):
    def __init__(self,indim,outdim,share=False):
        super(TwoGru,self).__init__()
        self.indim = indim
        self.outdim = outdim
        self.flag1 = True
        self.falg2 = True
        self.share = share
        self.modellength = 6-1
        self.first = nn.GRU(input_size=2*self.indim,hidden_size=self.outdim,
                                batch_first=True,bidirectional=True)
        if not self.share:
            self.model = nn.ModuleList([nn.GRU(input_size=2*self.indim,hidden_size=self.outdim,
                                    batch_first=True,bidirectional=True)
                for _ in range(self.modellength)]
            )
    def forward(self,inputs):
        r = inputs
        for iterer in range(self.modellength+1):
            model = self.first
            if not self.share:
                model = self.model[iterer-1]
            r,hi = model(r)
            r = torch.cat( [r[:,:,:self.outdim], torch.flip(r[:,:,self.outdim:],[1])],-1 )
            r1 = [r[:,i:i+1,:] for i in range(r.size(1)) if i%2==1] 
            r1.append(r[:,-1:,:])
            # r = r1.append(r[:,-1:,:]) if r.size(1)%2==1 else r1  ## 测试这样写错误，必须先append，否则结果为None
            r = r1 if r.size(1)%2==1 else r1[:-1]
            # print "TwoGru",len(r)
            r = torch.cat(r,1)
            if r.size(1) <=8:
                break

        return r,hi

class TwoNoGateRnn(nn.Module):
    def __init__(self):
        super(TwoNoGateRnn,self).__init__()
    #     self.para[] = nn.Parameter(torch.tensor())
    #     self.pool[]


    # def mix(self,inputs):
    #     self.para(inputs) + self.pool 

    # def forward(self,inputs):
    #     r = inputs
    #     for i in range(10):
    #         r = self.mix(r)
