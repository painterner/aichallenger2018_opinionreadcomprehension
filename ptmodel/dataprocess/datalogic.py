#!-*-coding:utf-8-*-
import torch
import torch.nn as nn
import numpy as np
import random
import copy
import cPickle
import time

"""
  最后应该还需要利用以前的方法，把id的位置shuffle

  现在策略是每次一个sample(minibatch?)都随机，不过看样子按照顺序也应该没多大区别。

  multinomial 一次采一个，可能浪费时间，后续优化。

  iter 应该是可以递归调用的。

  没想到怎样写单元测试，函数测试方法，打开对应的print，查看逻辑是否一致，并且最终输出是否正确即可。
"""

class DataLogic():
    def __init__(self,dataset,vocabsize,state_print=False):
        """
        鉴于比较麻烦，先不采用动态dataset.
        先不考虑是否有记忆的情况。所有的都归于一个随机方法。
        Args:
            vocabsize: must not larger than vocabsize that in embedding
        """
        self.activebert_count = 0
        self.vocabsize = vocabsize
        self.dataset = dataset
        self.current_state = None

        self.STATE_base = self.base.__name__
        self.STATE_unRelated = self.unRelated.__name__
        self.STATE_SingleQ = self.SingleQ.__name__
        self.STATE_SingleP = self.SingleP.__name__
        self.STATE_BERT = self.BERT.__name__

        if state_print:
            print "datalogic states:", self.STATE_unRelated, \
                    self.STATE_base, \
                    self.STATE_SingleQ, \
                    self.STATE_SingleP, \
                    self.STATE_BERT
        state_sets=set()
        state_sets.update( [self.STATE_unRelated], \
                           [ self.STATE_base], \
                           [self.STATE_SingleQ], \
                           [self.STATE_SingleP], \
                           [ self.STATE_BERT])
        # print len(state_sets)
        assert len(state_sets) == 5

    def __random(self,count):
        randid = np.random.randint(0,len(self.dataset),size=count)
        ## random.randint [a,b] np.random.randint [a,b)
        return randid

    def unRelated(self,count):
        """
        the unrelated QP pairs that not be Saw in facts
        """
        randid = self.__random(count)  ## shape [count]
        toreturn = []

        for i in randid:
            while True:
                v = np.random.randint(0,len(self.dataset))  ## shape int 
                if v != i:
                    zq = copy.deepcopy ( self.dataset[i] ) ## o=self.dataset[i], o[0]=query,o[1]=passage,o[2]=answer,o[3]=ids
                    zp = copy.deepcopy ( self.dataset[v] )
                    # print "unrelated test zq", zq  ## 查看是否符合所定义的逻辑
                    # print "unrelated test zp", zp
                    zq[1] = zp[1]                ## q+p
                    z= zq
                    # print "unrelated test z", z
                    toreturn.append(z+[[ [0.],[0.],[1.],[0.],[1.] ]]+[self.unRelated.__name__])
                    break
        
        return toreturn      ## [0,1] : one-hot for related, unrelated label

    def SingleQ(self,count):
        randid = self.__random(count)
        toreturn = []
        for i in randid:
            data = copy.deepcopy(self.dataset[i])
            data[1] = [0]
            # print data           
            toreturn.append(data+[[[1./3],[1./3],[1./3],[0.],[1.]]]+[self.SingleQ.__name__])
        return toreturn

    def SingleP(self,count):
        randid = self.__random(count)
        toreturn = []
        for i in randid:
            data = copy.deepcopy(self.dataset[i])
            data[0] = [0]  
            # print data 
            toreturn.append(data + [[ [1./3],[1./3],[1./3],[0.],[1.] ]]+ [self.SingleP.__name__])      
        return toreturn ## 如果要联合真实label推导question,可能还要用到seq2seq的知识。

    def BERT(self,count,vocabsize):
        """
        count: counts of samples
        vocabsize: vocabulary length, have assumed that all id is lines that from 0 to vocabsize
        """
        def mask(data):
            """
            15% to chose  
                80% time to mask(if 0, represent a space likes that in cloze), 10% time to change others, 10% time unchanged 
            --> [0.85,0.15*0.8,0.15*0.10,0.15*0.10] --> 0,3 position merge --> [0.865,0.12,0.015]
            """
            l = len(data)
            for i in range(l):
                prob = [0.865,0.12,0.015]
                rand = np.random.multinomial(1,prob)
                pos = list(rand).index(1)
                if pos == 1:
                    data[i] = 0
                elif pos == 2:
                    data[i] = np.random.randint(0,vocabsize)
            return data
        eta = np.random.random()*0.15
        lb = [ [1.],[0.],[0.],[1.],[0.] ]
        for i,v in enumerate(lb):
            if i < 3:
                if v == 1:
                    lb[i][0] = 1-eta
                else:
                    lb[i][0] = eta/2
            else:
                break

        randid = self.__random(count)
        toreturn = []
        for i in randid:
            data = copy.deepcopy(self.dataset[i])
            data[1] = mask(data[1])   ## only mask passage  
            toreturn.append(data+[lb]+[self.BERT.__name__])

        return toreturn

    def base(self,count):
        randid = self.__random(count)
        toreturn = []
        for i in randid:
            data = copy.deepcopy(self.dataset[i]) 
            toreturn.append(data+[[ [1.],[0.],[0.],[1.],[0.] ]]+[self.base.__name__]) 
                ## "+"表示后面的给包含到data里面去,本来打算分开的，但是分开后在训练前还要合并共同shuffle answer,有点麻烦。
        return toreturn
    def eval_get(self,start,length):
        toreturn = []
        for i in range(length):
            data = copy.deepcopy(self.dataset[start+i])
            toreturn.append(data+[[ [1.],[0.],[0.],[1.],[0.] ]]+[self.base.__name__]) 
        return toreturn


    def get_batch(self,batch_size,epoch=None):
        """
        调度:
            每一个Sample都在在dataset随机采样,这样就没有epoch概念了(或者是自己自定义)。
            第一次epoch,前30%不调用BERT

            BERT:   label 在正确时减去0-0.15之间随机值，用来表示不太确定。 e.g. 0.88867531270, 0.055662343, 0.0556623, 1.0, 0.0
            unrelated: label 0,0,1,0,1
            singlep:    label 0.33,0.33,0.33,0,1
            singleq:    label 0.33,0.33,0.33,0,1
            base:       label 1,0,0,1,0

            ratio:
                BERT   unrelated  singlep  singleq  base
                15%     10%         10%       10%    50%
            
        """
        batch=[]
        def extend__(item):
            """
            or we can append first, then use for loop to extend all
            """
            batch.extend(item)

        prob = [0.70,0.10,0.10,0.10]
        rand = np.random.multinomial(batch_size,prob) ## sample batch_size times shape [batch_size]
    
        for i,count in enumerate(rand):
            if count == 0:
                continue 
            if i == 0:
                if epoch is not None:
                    """
                    第一次epoch,前30%不调用BERT
                    """
                    if self.activebert_count >= len(self.dataset)/3 or epoch > 0:
                        p_ = [0.79,0.21]  ## 0.55,0.15 --> normalize
                        r_ = np.random.multinomial(count,prob)[1]
                        if r_ > 0:
                            item = self.BERT(r_,self.vocabsize)
                            extend__(item)
                            count = count - r_
                    else:
                        self.activebert_count += batch_size
                        
                item = self.base(count)
                extend__(item)
            elif i == 1:
                item = self.unRelated(count)
                extend__(item)
            elif i == 2:
                item = self.SingleQ(count)
                extend__(item)
            elif i == 3:
                item  = self.SingleP(count)
                extend__(item)
        # print batch
        return batch

    def eval_iter(self,batch_size):
        l = len(self.dataset)
        for i in range(l/batch_size):
            batch = self.eval_get(start=i*batch_size,length=batch_size)
            yield batch
        if l % batch_size != 0:
            batch = self.eval_get(start=(i+1)*batch_size,length= l % batch_size)
            yield batch

    def __call__(self,length,batch_size,epoch=None):
        for _ in range(length):
            yield self.get_batch(batch_size,epoch)

        
if __name__ == "__main__":
    """
    测试是否正确
    """
    with open('data/develop/training_30000line.pickle','rb') as f___:
        train_data = cPickle.load(f___)

    train_data = train_data[0:2]
    datalogic = DataLogic(train_data,1000,'train')

    for i in range(1):
        timeA = time.time()
        datalogic.get_batch(200,1)
        print time.time() - timeA