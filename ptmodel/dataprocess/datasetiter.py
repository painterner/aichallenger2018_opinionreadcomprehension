#!-*-coding:utf-8-*-
class DatasetIter():
    def __init__(self,dataset=None):
        self.__data_idx = 0
        if dataset is not None:
            assert isinstance(dataset,tuple)  ## dataset should can't be change,so can't be list
            self.dataset = dataset  ## tuple is value copy or pointer copy ? print value address ?
    
    def get_data_idx(self):
        return self.__data_idx
    
    def set_data_idx(self,v):
        self.__data_idx = v

    def set_data_set(self,dataset):
        self.dataset = dataset
        assert self.__data_idx == 0

    def fiter(self,dataset=None,dataset_partitial_length=None,batch_size=1):
        """
        Args:
            dataset_partitial_length: 
                As name，每次迭代dataset中的data个数.
                if use this,you need to calculate the times of dataset-iters,i.e. 
                    dataset_partitial_length/batch_size + ee(dataset_partitial_length%batch_size)
                    其中ee为 输入<=0时为0的单位阶跃函数。
            batch_size: As name
        Return:
        """
        if dataset is None:
            dataset = self.dataset
        res = 0
        if dataset_partitial_length is None:
            l = len(dataset)
            res = l%batch_size
        else:
            l = dataset_partitial_length
            assert l%batch_size == 0
        b = batch_size
        for i in range(l/batch_size):
            s = i*b+self.__data_idx
            yield dataset[s:s+b]

        last_index = i*l
        if res != 0:                             ## 剩余的data
            yield dataset[i:]
            last_index = len(dataset)

        self.__data_idx += last_index              ## 下一个索引
        if self.__data_idx  >= len(dataset):       
            ## all data has been searched . if use dataset_partitial_length, self.data_idx may exceed the length of dataset
            self.__data_idx = 0

def train_with_Bigrad():
    pass
