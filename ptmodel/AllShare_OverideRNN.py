#!-*-coding:utf-8-*-
"""
测试手段:
    1. 简单的把所有W都share
    2. 存在一个主的W(永久记忆区), 分裂成各种小型W，然后计算，先用LSTM来测试， 然后用MNIST, MultiMNIST来测试
    3. 存在一个support W, 在各个网络之间share(临时记忆区)
"""
import torch
import torch.nn as nn
import math
import copy

class MainW(nn.Module):
    def __init__(self,dim):
        """
        Args:
            dim : outdim * deepth
            deepth给外界因子提供多样性，outdim是为了拟合外界需要的大小。
            deepth: int or tuple or list of MainW pool size
        """
        super(MainW,self).__init__()
        if isinstance(dim,int):
            self.dim = [dim,dim]
        elif isinstance(dim,tuple) or isinstance(dim,list):
            assert len(dim) == 2
            self.dim = dim
        else:
            raise Exception('must int tuple or list')
        self.model = nn.Parameter(torch.randn(self.dim[0],self.dim[1]))
        self.initialize()
    def initialize(self):
        for p in self.parameters():
            nn.init.xavier_uniform_(p,0.01)

    @property
    def transfactor(self):
        return self.dim[1]

    def getSubW(self,submodel):
        """
        Args: 
                   对于输出W来说， inW.dim(0) == outdim, outW,dim(1) == outdim
                
        """
        ## BiLinear内部可能也是这样。
        # m1 = torch.sigmoid((self.model * submodel.inW).sum(0))

        assert submodel.inW.dim() == 1
        m1 = torch.sigmoid(self.model.matmul(submodel.inW))  ## [outdim, deepth] [deepth] = [outdim]
        m2 = m1*submodel.outW  ## [outdim] * [outWindim, outdim] = [outWindim,outdim]
        r = m2
        return r

    def forward(self):
        """
        MainW无ops
        """
        pass

class SubWfactor(nn.Module):
    def __init__(self,indim,outdim,transdim):
        """
            use: MainW.getTransfactor() get the transdim
        """
        super(SubWfactor,self).__init__()
        self.indim = indim
        self.outdim = outdim
        self.transdim = transdim
        self.W = None
        self.inW  = nn.Parameter(torch.randn(self.transdim))
        self.outW = nn.Parameter(torch.randn(self.indim ,self.outdim))
        self.outb = nn.ParameterList([nn.Parameter(torch.randn(self.outdim)) for _ in range(2)])

        # self.testW = nn.Parameter(torch.randn(self.indim,self.outdim))
        self.initialize()
        
    def initialize(self):
        stdv = 1. / math.sqrt(self.outW.size(1))
        for p in self.parameters():
            p.data.uniform_(-stdv*0.1,stdv*0.1)
            if p.dim() == 2:
                nn.init.xavier_uniform_(p,0.01)
        """
            初始值影响acc或者收敛?:
                1.self.testW测试： randn * 0.1  --> 收敛及其缓慢，epoch0-4 acc 一直57% 
                2.如果self.test改成nn.Linear, 在eopch2 acc 就可以达到60%
                3.然而如果所有的bias也进行nn.Linear中类似的uniform_处理和2的情况就一致了。
        """

    def forward(self,MainW,X,reuse=False):
        if not reuse:
            self.W = MainW.getSubW(self)
        O = X.matmul(self.W) + self.outb[1]
        return O

class GRUWithShare(nn.Module):
    def __init__(self,indim,outdim,transdim,mode='GRU',bidirectional=False):
        """
        Args:
            mode: 'Simple' or 'GRU'

            This is designs for RNN that batch_first=True
        """
        super(GRUWithShare,self).__init__()
        assert mode == 'Simple' or mode == 'GRU'
        self.mode = mode
        self.indim = indim
        self.outdim = outdim
        self.transdim = transdim
        self.bidirectional = bidirectional
        self.count0 = 3 if mode == 'GRU' else 2
        self.Wi = nn.ModuleList([SubWfactor(indim,outdim,transdim) for _ in range(self.count0)])
        self.Wh = nn.ModuleList([SubWfactor(outdim,outdim,transdim) for _ in range(self.count0)])
        if self.bidirectional:
            self.Wi_r = nn.ModuleList([SubWfactor(indim,outdim,transdim) for _ in range(self.count0)])
            self.Wh_r = nn.ModuleList([SubWfactor(outdim,outdim,transdim) for _ in range(self.count0)])

    def forward(self,MainW,X,hx=None,reuse=False):
        # X [B,L,M]

        ## 2798 None (8, 8, 200) (8, 8, 200) (8, 8, 200) ?? 2798 ??
        assert X.dim() == 3
        def execut(hx,Wi,Wh,X):
            if hx is None:
                hi = torch.cat( [torch.zeros_like(self.Wh[0].outW.data[0,:].view(1,1,-1)) for _ in range(X.size(0))],0)
                    ## 不能zeros_like(X[:,0:1,:]),因为hi是根据outdim而定的，不是indim >> ??
            else:
                hi = hx.transpose(1,0)
            toreturn = []
            for i in range(X.size(1)):
                inp = X[:,i:i+1,:]
                if i == 0 and not reuse: ## 为了节省计算，两种方式reuse,第一种是外部直接控制，第二种是默认的 i ！= 0时。
                    reuseflag = False
                else:
                    reuseflag = True
                if self.mode == 'GRU':
                    gr = torch.sigmoid(Wi[0](MainW,inp,reuseflag)  + Wh[0](MainW,hi,reuseflag)) # Wh[0](MainW,inp*hi,reuseflag)
                    gn = torch.tanh   (Wi[2](MainW,inp,reuseflag)  + gr*Wh[2](MainW,hi,reuseflag) ) 
                                ## gr 应该是限制hidden的输入数据，减弱hidden对于输入门的影响。
                elif self.mode == 'Simple':
                    gn = torch.tanh( Wi[0](MainW,inp,reuseflag) + Wh[0](MainW,hi,reuseflag) )
                else:
                    raise Exception("must be Simple or GRU")
                gz = torch.sigmoid ( Wi[1](MainW,inp,reuseflag)  + Wh[1](MainW,hi,reuseflag) ) #[B,1,M]
                # hi = gz*hi + (1-gz)*gn
                hi = gz*(hi-gn) + gn
                toreturn.append(hi)
            toreturn = torch.cat(toreturn,1)
            return toreturn, hi
        hx1=None
        hx2=None
        if self.bidirectional and hx is not None:
            hx2 = hx[1:2,:,:].transpose(1,0)
        elif hx is not None:
            hx1 = hx[0:1,:,:].transpose(1,0)
         
        rforward,rfdhx = execut(hx1,self.Wi,self.Wh,X)
        output = rforward
        hi = rfdhx
        if self.bidirectional:
            rbackward,rbkhx = execut(hx2,self.Wi_r,self.Wh_r,torch.flip(X,[1]))
            output = torch.cat([output,rbackward],-1)
            hi = torch.cat([hi,rbkhx],1) # [B,2,M]
        hi = hi.transpose(1,0) # [1,B,M] or [2,B,M]
        return output,hi

def TestMainW_and_Subfactor():
    pass

def fTestModifyGRU():
    """
    简单测试: 1.只测试了inW,如果测试其他的情况可以重新代码,更改传入test()的参数
             2. 分别torch.ones和torch.randn赋值给self.inp来测试看看正反ModifyGRU输出是否一致,
                并查看hi是否对应最后一个状态输出。(已测试符合。git hash:c9a10b3e6f79e1f1e7be37a56e68bbf36c07133a 大约时间: 2018.11.10 15:13)
    """
    class CTestModifyGRU(nn.Module):
        def __init__(self):
            super(CTestModifyGRU,self).__init__()
            self.MW = MainW(3)
            self.inp = torch.ones(2,5,2)
            self.m = GRUWithShare(2,2,self.MW.transfactor,bidirectional=True)
        def forward(self):
            r = self.m(self.MW,self.inp)
            return r

    c = CTestModifyGRU()
    print "input", c.inp
    print "MainW",list(c.MW.parameters())

    def test():
        print "before inplace"
        print c.m.Wi[0].inW
        print c.m.Wi_r[0].inW
        print c()
        right = False
        for a,b in zip(c.m.Wi[0].inW.view(-1),c.m.Wi_r[0].inW.view(-1)):
            if a != b:
                right = True ## 只要有一个不一样就认为通过
        assert right

        print "inplace instead reversal Wi_r Wh_r to Wi Wh, and test if output is the same"
        for a,b in zip(c.m.Wi_r,c.m.Wi):
            for pa,pb in zip(a.parameters(),b.parameters()):
                pa.data = nn.Parameter(pb)
        for a,b in zip(c.m.Wh_r,c.m.Wh):
            for pa,pb in zip(a.parameters(),b.parameters()):
                pa.data = nn.Parameter(pb)
        # print "GRU", list(c.m.parameters())  ## 24组参数: 6 x 2(bi) x 2(SubWfactor) = 24
        print "after inplace"
        print c.m.Wi[0].inW
        print c.m.Wi_r[0].inW
        right = True
        for a,b in zip(c.m.Wi[0].inW.view(-1),c.m.Wi_r[0].inW.view(-1)):
            if a != b:
                right = False  ## 只要有一个不一样就认为不通过
        assert right
        # c.cuda()
        print c()
    test()

class AllNetUseHidde(nn.Module):
    """
    把所有的网络都乘以一个share因子，这个因子在整个网络中优化（借鉴于RNN的隐藏层)
    """
    def __init__(self):
        super(AllNetUseHidde,self).__init__()

if __name__ == "__main__":
    fTestModifyGRU()

"""
11.10:
不知道为何
    nn.Parameter(torch.randn(self.transdim ,self.indim)).data.normal_() 不会自动加入cuda
                                                                     # 应该是因为没有先赋值的原因
                                                                     # A != A.data.normal_()
    nn.Parameter(torch.randn(self.transdim ,self.indim)) 会自动加入cuda

RNN参考路径:
    /home/ka/.local/lib/python2.7/site-packages/torch/nn/_functions/rnn.py

参数总和最小?
    非绝对值
"""