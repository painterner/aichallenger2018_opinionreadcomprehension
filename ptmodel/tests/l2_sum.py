#!-*-coding:utf-8-*-
import sys,os
def ChangePath2Cwd():
    A = sys.path[0]
    sys.path.remove(sys.path[0])
    sys.path.append(os.getcwd())   ## then you can import preprocess in fanterfolder
    B = sys.path[-1]
    print 'environment change:',A,'to',B
ChangePath2Cwd()

import torch
import torch.nn as nn
from libpainterner.torch.function import l2_sum

m = nn.Linear(1,2)

l2 = l2_sum('weight',m)

print l2