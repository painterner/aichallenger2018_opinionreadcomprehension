#!-*-coding:utf-8-*-
import torch
import torch.nn as nn
from painternerlib.printf import printf,getPrintf
# getPrintf().cancel()

class NWlinear(nn.Module):
    def __init__(self,n,indim,outdim):
        super(NWlinear,self).__init__()
        self.model = nn.ModuleList(
            [nn.Linear(indim,outdim) for i in range(n)]
        )
    def forward(self,inputs):
        return self.model(inputs)

class ThreeCat():
    def __init__(self):
        pass
    def __call__(self,inputs): ## [B,L,M]
        # for loop:
           # 相邻3个cat
        pass
##200*200*3 == 600*200    1,3,5 相邻的1,或者3个，或者5个进行concatenate,然后进行gru实验。
##                         融合方式，这n个
# class 对照类():
#     def __init__(self):
#         pass

class selfattention(nn.Module):
    def __init__(self,indim,middim):
        super(selfattention,self).__init__()
        self.indim = indim
        self.middim = middim
        self.model =  nn.Sequential(
            nn.Linear(self.indim,self.middim),
            nn.Tanh(),
            nn.Linear(self.middim,1),
            nn.Softmax(1)
        )
    def forward(self,inputs):
        return self.model(inputs)

class MultiAttention(nn.Module):
    def __init__(self,indim,midim):
        super(MultiAttention,self).__init__()
        self.indim = indim
        self.middim = midim

        self.Wphase1 = nn.ModuleDict({
            'mi':  nn.ModuleList(nn.Linear(self.indim,self.middim)   for _ in range(2)),
            'bi':  nn.ModuleList(nn.Linear(self.indim,self.indim)   for _ in range(2)),
            'dot': nn.ModuleList(nn.Linear(self.indim,self.middim)   for _ in range(2)),
            'cat': nn.ModuleList(nn.Linear(self.indim,self.middim)   for _ in range(3)),
        })
        self.Vphase1 = nn.ModuleDict({
            'mi':  nn.ModuleList([nn.Linear(self.indim,1) for _ in   range(2 )]),
            'dot': nn.ModuleList([nn.Linear(self.indim,1)  for _ in  range(2 )]),
            'bi':  nn.ModuleList([nn.Linear(self.indim,1) for _ in   range(2 )]),
            'cat': nn.ModuleList([nn.Linear(self.indim,1)  for _ in  range(2 )])
        })
        self.Wagger = nn.ModuleList([nn.Linear(self.indim,self.middim) for _ in range(4)])
        self.Vshare = nn.ModuleList([nn.Linear(self.middim,1) for _ in range(4)])
        self.Wg = nn.ModuleList([nn.Linear(2*self.indim,self.indim) for _ in range(4)])

    def fempty(self,inputs):
        return inputs

    def phase2(self,inputs,inputs2):
        mi,dot,bi,cat = inputs
        p = inputs2
        gmi,gdot,gbi,gcat = (self.Wg[0](torch.cat([mi, p],-1)),
                             self.Wg[1](torch.cat([dot, p],-1)),
                             self.Wg[2](torch.cat([bi, p],-1)),
                             self.Wg[3](torch.cat([cat, p],-1)))
        return gmi,gdot,gbi,gcat
    
    def phase3(self,inputs):
        gmi,gdot,gbi,gcat = inputs  #[B,L,M]
        def mode(a,V,W):
            l = V( torch.tanh(W(a))).transpose(2,1)    #[B,L,M] to [B,1,L] 
            return l
        # print gbi.size(),gmi.size(), gdot.size(),gcat.size()
        aggermi = mode(gmi,     self.Vshare[0],self.Wagger[0])
        aggerdot = mode(gdot,   self.Vshare[1],self.Wagger[1])
        aggerbi = mode(gbi,     self.Vshare[2],self.Wagger[2])
        aggercat = mode(gcat,   self.Vshare[3],self.Wagger[3])

        # print aggerbi.size(),aggermi.size(), aggerdot.size(),aggercat.size()
        agger = torch.cat([aggermi,aggerdot,aggerbi,aggercat],1) ## [B,1,L] to [B,4,L]
        agger_normal = torch.softmax(agger,1).unsqueeze(-1)  ## [B,4,L] to [B,4,L,1]
        gmi,gdot,gbi,gcat = (gmi.unsqueeze(1),gdot.unsqueeze(1),
                                gbi.unsqueeze(1),gcat.unsqueeze(1)) ## [B,L,M] to [B,1,L,M]
        agger = torch.cat([gmi,gdot,gbi,gcat],1) ## [B,1,L,M] to [B,4,L,M]
        agger = agger_normal * agger
        agger = agger.mean(1)    ## [B,4,L,M] to [B,L,M]
        return agger

    def forward(self,q,p):
        uq = q.unsqueeze(1) 
        up = p.unsqueeze(2)
        def mode(a,v,b,bi=False):
            # print a.size(),b.size()
            if not bi:
                a = v( torch.tanh( a  )  ).squeeze(-1) 
            else:
                a = v( a  )     
            n = torch.softmax(a,-1).bmm(b)
            return n 
        def mode2(a,v):
            a1 = v( torch.tanh( a  )  )  
            n = torch.softmax(a,1) * a
            return n
        # print uq.size(), up.shape
        mi =    mode(self.Wphase1['mi']  [0](uq-up),                         self.Vphase1['mi'] [0],     q)
        dot =   mode(self.Wphase1['dot'] [0](uq*up),                         self.Vphase1['dot'][0],     q)
        bi =    mode(self.Wphase1['bi']  [0](p).bmm(q.transpose(2,1)),       self.fempty,   q,        True)    # (32, 350, 256) (32, 12, 256) (128, 256)
        cat =   mode(self.Wphase1['cat'] [0](uq)+self.Wphase1['cat'][1](up), self.Vphase1['cat'][0],     q)

        # print mi.size(),dot.size(),bi.size(),cat.size()
        mi =    mode2(self.Wphase1['mi'] [1](mi),     self.Vphase1['mi']    [1])
        dot =   mode2(self.Wphase1['dot'][1](dot),     self.Vphase1['dot']  [1])
        bi =    mode2(self.Wphase1['bi'] [1](bi),     self.Vphase1['bi']    [1])    
        cat =   mode2(self.Wphase1['cat'][2](cat),     self.Vphase1['cat']  [1])
        # print mi.size(),dot.size(),bi.size(),cat.size()

        r = self.phase2([mi,dot,bi,cat],p)
        r = self.phase3(r )
        return r

class RnnResNet(nn.Module):
    def __init__(self,n,embedding_size,encoder_size):
        """
        ARGS:
            n: gru_numers
        """
        super(RnnResNet,self).__init__()

        self.n = n
        self.Hypara={
            "shareqa": False,
            "q_shrink_to":3,
            # "p_shrink_to":50, # have use scheduler to instead,refer to cat_normL().schedular()
            "gru_dim":0,
            'WpInsteadMultiAttn':True
        }
        self.model_size = {
            "Encoder": 3,
            "GRU":     self.n,
            "MutiAttn":self.n+1,
            "SelfAttn":self.n+4,
            "Wp":      self.n
        }
        self.model = nn.ModuleDict({
            # "Encoder":      nn.ModuleList([nn.GRU(input_size=embedding_size, hidden_size=encoder_size, batch_first=True,
                                # bidirectional=True)         for _ in range(self.model_size['Encoder']  )] ),

            "GRU":          nn.ModuleList([nn.GRU(input_size=2*encoder_size, hidden_size=encoder_size, batch_first=True,
                                bidirectional=True)         for _ in range(self.model_size['GRU']      )] ),

            "MultiAttn":    nn.ModuleList([MultiAttention(2*encoder_size,2*encoder_size) for _ in range(self.model_size['MutiAttn'] )] ),

            "SelfAttn":     nn.ModuleList([selfattention(2*encoder_size,encoder_size)    
                                                            for _ in range(self.model_size['SelfAttn'] )] ),

            "Wp":           nn.ModuleList([nn.Linear(2*encoder_size,2*encoder_size)        
                                                            for _ in range(self.model_size['Wp']       )] )
        })

        self.initiation()

    def initiation(self):
        for module in self.modules():
            if isinstance(module, nn.Linear):
                nn.init.xavier_uniform_(module.weight, 0.1)
                # nn.init.xavier_normal_(module.weight, 0.1)

    def setcuda(self):
        self.vcuda = True

    def cat_normL(self,X,normL):
        """
        tensor indice 之后还能优化吗，比如这里把normL拆开了去prob.
        记住这个开发时候的状态-->提高复杂条件设计效率，选择性忘记，聚怪。
        """
        def scheduler(X):
            """
                12 -> 12  50 -> 27 150 -> 39 200 -> 43 :  formula 2.2*(x-12)**0.5 + 12
            """
            assert X.dim()==3
            l = X.size(1)
            return 12 if l <= 12 else int(2.2*(l-12)**0.5+12)

        # shrinkl = self.Hypara['p_shrink_to']
        shrinkl = scheduler(X)
        l = X.size(1)
        seg_prob = 1.  / shrinkl  ## any seg length probility is 1/50 = 0.02
        all_toreturn = []
        all_torprob = []
        all_pos_record=[]
        one_zero = torch.zeros(1,dtype=torch.float32)[0]
        if hasattr(self,'vcuda') and self.vcuda:
            one_zero = one_zero.cuda()
        for i in range(normL.size(0)):
            toreturn = [] 
            torprob = []
            pos_record = []
            prob = one_zero
            last_prob = one_zero
            # print one_zero
            current_total_prob = 0.
            count = 0
            last_count = 0
            correction_shrinkl = shrinkl
            seg_prob_base = seg_prob
            seg_prob_base = seg_prob_base - seg_prob_base/correction_shrinkl  ## 这样能保证最后一个prob在每个都不过current seg_prob是current seg_prob组中最大的。
            next_indx = 0
            for ii in range(l):
                """
                    e.g.  prob = 0.019 , 0.02xx, 0.2
                """
                cur_max_over_prob = seg_prob_base/correction_shrinkl 
                # max_over_prob = cur_max_over_prob*(count+1-last_count) ## if 50, then it's (0.02 / 50) x cnt
                max_over_prob = cur_max_over_prob 
                last_prob = prob
                prob = prob + normL[i,ii]   
                """
                tensor += 是in-place的，切记。
                """
                current_total_prob = current_total_prob + normL[i,ii]
                # print prob
                # print current_total_prob
                if prob >= seg_prob_base and prob < seg_prob_base + max_over_prob:   
                # if prob >= seg_prob_base and current_total_prob < seg_prob_base*(count+1) + max_over_prob:   
                    toreturn.append(X[i:i+1,next_indx:ii+1,:].mean(1,keepdim=True)); torprob.append(prob.unsqueeze(0)); count += 1 ;pos_record.append(next_indx)
                    next_indx = ii+1
                    prob = one_zero
                elif prob >= seg_prob_base + max_over_prob:
                    if next_indx == ii:  ##由于next_indx==ii,prob过大，为空,所以要保留一个
                        toreturn.append(X[i:i+1,next_indx:ii+1,:].mean(1,keepdim=True)); torprob.append(prob.unsqueeze(0));count += 1 ;pos_record.append(next_indx)
                        next_indx = ii+1
                        prob = one_zero
                        correction_shrinkl = shrinkl-count              ## A
                        seg_prob_base = (1 - current_total_prob) / (shrinkl-count)
                        seg_prob_base = seg_prob_base - seg_prob_base/correction_shrinkl
                        last_count = count
                    elif prob - last_prob >= seg_prob_base:##由于prob过大，为空,所以要保留一个
                        toreturn.append(X[i:i+1,next_indx:ii,:].mean(1,keepdim=True)); torprob.append(last_prob.unsqueeze(0)); count += 1 ;pos_record.append(next_indx)
                        toreturn.append(X[i:i+1,ii:ii+1,:].mean(1,keepdim=True)); torprob.append((prob-last_prob).unsqueeze(0)); count += 1 ;pos_record.append(next_indx)
                        prob = one_zero
                        if count == shrinkl: ## 连续两次append需要提前判断一次。
                            break
                        if prob-last_prob >= seg_prob_base + max_over_prob:
                            next_indx = ii+1
                            correction_shrinkl = shrinkl-count          ##B    A and B操作是一样的。
                            seg_prob_base = (1 - current_total_prob) / (shrinkl-count)
                            seg_prob_base = seg_prob_base - seg_prob_base/correction_shrinkl
                            last_count = count
                        else:
                            next_indx = ii+1
                    else: ##ii的数据prob没达到阈值，所以放到下一次
                        toreturn.append(X[i:i+1,next_indx:ii,:].mean(1,keepdim=True)); torprob.append(last_prob.unsqueeze(0)); count += 1 ;pos_record.append(next_indx)
                        next_indx = ii
                        prob = prob - last_prob

                ## 两种结束的条件。
                if ii == l-1 and count <= shrinkl - 1:
                    if next_indx <= ii:  ## 说明最后的没有被put进list
                        empty_res = shrinkl-count-1
                    else:
                        empty_res = shrinkl-count
                    zeros = torch.zeros(1,empty_res,X.size(2)) 
                    torprob_zeros = torch.zeros(empty_res)
                    if hasattr(self,'vcuda') and self.vcuda:
                        zeros = zeros.cuda()
                        torprob_zeros = torprob_zeros.cuda()
                    if next_indx <= ii:
                        toreturn.append(X[i:i+1,next_indx:,:].mean(1,keepdim=True)) ;torprob.append(prob.unsqueeze(0)); pos_record.append(next_indx);count += 1
                        next_indx = l ##表示需要补零
                    toreturn.append(zeros); torprob.append(torprob_zeros); count += 1 ;pos_record.append(next_indx);  # the variant of count may less 50                                                                                                                                     
                    break
                elif count == shrinkl-1:
                    assert (1-current_total_prob) >= 0.
                    toreturn.append(X[i:i+1,next_indx:,:].mean(1,keepdim=True)); torprob.append((1-current_total_prob).unsqueeze(0)); count += 1 ;pos_record.append(next_indx)
                    break   

            all_pos_record.append(pos_record)
            all_toreturn.append(torch.cat(toreturn,1))
            # print torch.cat(torprob,0).unsqueeze(0).size()
            all_torprob.append(torch.cat(torprob,0).unsqueeze(0))
            print i,"pos_record",  len(pos_record)
            print i,"toreturn",    len(toreturn)
            print i,"toprob",      len(torprob)
            print i,"current count", count 
            print i,"current next_indx", next_indx
            print i,"shrinkl", shrinkl

        try:
            all_pos_record = torch.tensor(all_pos_record)
            all_toreturn = torch.cat(all_toreturn,0) #[B,50,M]
            # print all_toreturn.size()
            all_torprob  = torch.cat(all_torprob,0) 
            # print all_torprob.size()
            # print all_torprob
            # exit()
        except Exception, e:
            print "all pos record", all_pos_record
            print "all toreturn",all_toreturn
            print "all toprob",all_torprob
            print "current count",count,"i",i,"ii",ii
            print e            
            raise Exception('error')
        return all_toreturn,all_torprob

    def q_shrink(self,X):
        assert X.dim() ==3
        assert self.Hypara['q_shrink_to'] == 3
        shrinkl = self.Hypara['q_shrink_to'] 
        l = X.size(1)
        seg = l  / self.Hypara['q_shrink_to']  ## any length shrink to 3
        toreturn = []
        for i in range(shrinkl): 
            """
                采取策略： 有Res的时候，放到最后一个tensor里面mean
            """
            if i == seg-1:
                toreturn.append(  X[:,i*seg:,:].mean(1,keepdim=True)  )
            else:
                toreturn.append( X[:,i*seg:(i+1)*seg,:].mean(1,keepdim=True) )

        if seg == 0:
            """
                采取策略: 等于1的时候放到第一个pos，等于2的时候放到前两个 (及其没有可能，特别是含有batch并且被补零等长的情况下)
            """
            zeros = torch.zeros(X.size(0),shrinkl-1,X.size(2)) 
            if hasattr(self,'vcuda') and self.vcuda:
                zeros = zeros.cuda()
            toreturn.append( X)
            toreturn .append(zeros[:,0:shrinkl-l,:])

        return torch.cat(toreturn,1)

    def forward(self,q,p):
        """
        Args:
            q: query (tokens: allmost less than 20 length)
            p: passage ( tokens: length <= 500 )
        """
        # q = self.model['Encoder'][0](q)
        # p = self.model['Encoder'][1](p)
        
        ## prepare q
        q_ = self.model['SelfAttn'][0](q)
        q = self.q_shrink (q_ * q)   #[B,L,1] -> [B,L,1]* [B,L,M] # to 3    (mean or cat)

        ## prepare p
        p = self.model['MultiAttn'][0](q,p)   # to 50 (ThreshHold提取主要的段落。)
        # p = self.model['MultiAttn'][0](self.model['Wp'][0](up) + uq)   # to 50 (ThreshHold提取主要的段落。)
        pLdist = self.model['SelfAttn'][1](p)
        pLdist = pLdist.squeeze(2)  # [B,L1,1] --> [B,L1]    
        #p Length Distribution
        p,prob =  self.cat_normL(p,pLdist)

        z = p
        for i in range(self.n):
            index = i
            # z = z.unsqueeze(2)
            # uq = q.unsqueeze(1)
            """
            修改此处的MultiAttn 为简单的线性相加（Z+P）不是（z+q)？
            """
            if not self.Hypara['WpInsteadMultiAttn']:
                z =     self.model['MultiAttn'][1+index](q, z ) ## 这里不使用Resnet，
            else:
                z =     self.model['Wp'][index](p)+z
                                                                                    ## 因为q含义明确易学习，不需要进一步处理
            z1 =    self.model['SelfAttn'][2+index](z)
            z1 =    z1 * z
            z,_ =     self.model['GRU'][index](z1)

        # z = self.model['Wp'][1+self.n](z)
        # z = self.model['Wp'][0](z)
        z1 = self.model['SelfAttn'][2+self.n](z) 
        pred1 = z1.transpose(2,1).bmm(z)               ## classifier
        z2 = self.model['SelfAttn'][3+self.n](z)
        pred2 = z2.squeeze(2)               ## expected Length Distribution 

        # print "prob pred2",prob.size(),pred2.size()
        # print "prob pred2",prob,pred2
        # loss_length_prob = ( -torch.log(prob)*pred2 ).sum(-1).mean()
        loss_length_prob = (  (prob-pred2)**2/(prob.size(0)*prob.size(1))  ).mean()
        # print loss_length_prob
        return pred1, loss_length_prob   # [B,1,M]  []

"""
错误记录:
    11.5  cat_normal_ 总是有几率输出的list大小不一致，需要详细打印数据测试。
"""
"""
思考:
    11.10  这样把猜测的平等概率的合并有什么必要吗？与随机合成几段没有区别，重点是合并一定要是
    正确的关注段才行。不过可以调试当作bug修复，以后可能用到。
"""