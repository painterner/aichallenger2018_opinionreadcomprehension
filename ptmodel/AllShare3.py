#!-*-coding:utf-8-*-
"""
测试手段:
    1. 简单的把所有W都share
    2. 存在一个主的W(永久记忆区), 分裂成各种小型W，然后计算，先用LSTM来测试， 然后用MNIST, MultiMNIST来测试
    3. 存在一个support W, 在各个网络之间share(临时记忆区)
"""
import torch
import torch.nn as nn
import math
import copy

class MainW(nn.Module):
    def __init__(self,dim):
        super(MainW,self).__init__()
        assert isinstance(dim,int)
        self.dim = dim
        self.model = nn.Parameter(torch.randn(self.dim))
        self.initialize()
    def initialize(self):
        stdv = 1. / math.sqrt(self.dim)
        self.model.data.uniform_(-stdv,stdv)

    @property
    def transfactor(self):
        return self.dim

    def getfactor(self,submodel):
        assert submodel.inW.dim() == 1
        r = torch.sigmoid((self.model*submodel.inW).sum())
        return r

    def forward(self):
        pass

class Linear(nn.Module):
    def __init__(self,in_features, out_features,transdim=200*200*8, bias=True):
        super(Linear,self).__init__()
        self.transdim = transdim
        self.inW  = nn.Parameter(torch.randn(self.transdim))
        stdv = 1. / math.sqrt(self.transdim)
        self.inW.data.uniform_(-stdv,stdv)

        self.model = nn.Linear(in_features, out_features, bias)
    def setMainW(self,MainW):
        self.MainW = MainW
    def forward(self,X,reuse=False):
        y = self.model(X)
        if not reuse:
            self.f = self.MainW.getfactor(self)
        y = y*self.f
        return y

class GRU(nn.Module):
    def __init__(self,input_size, hidden_size,transdim=200*200*8, batch_first=True,
                                bidirectional=False,mode='GRU'):
        super(GRU,self).__init__()
        self.input_size = input_size
        self.hidden_size = hidden_size
        self.bidirectional = bidirectional
        self.transdim = transdim
        self.inW  = nn.Parameter(torch.randn(self.transdim))
        stdv = 1. / math.sqrt(self.transdim)
        self.inW.data.uniform_(-stdv,stdv)

        self.model = nn.GRU(input_size=input_size, hidden_size=hidden_size, 
                            batch_first=batch_first,bidirectional=bidirectional)
    def setMainW(self,MainW):
        self.MainW = MainW
    def forward(self,X,hx=None):
        mode = 'NoStep'
        assert X.dim() == 3
        if mode == 'Step':
            hi = hx
            toreturn = []
            f = self.MainW.getfactor(self)
            for i in range(X.size(1)):
                y,hi = self.model(X[:,i:i+1,:],hi) ## gru 含有非线性，所以不能直接最终output直接乘以f
                y = y*f
                hi = hi*f
                toreturn.append(y)
            toreturn = torch.cat(toreturn,1)
        else:
            y,hi = self.model(X)
            f = self.MainW.getfactor(self)  ## 注意这样就不是元素级别的了，因为是在复杂的GRU外部总体乘上一个因子。
            y = y*f
            hi = hi*f
            toreturn = y
        return toreturn,hi