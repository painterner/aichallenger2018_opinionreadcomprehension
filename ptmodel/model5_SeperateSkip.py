# -*- coding: utf-8 -*-
import torch
from torch import nn
from torch.nn import functional as F
from termcolor import colored
import logging
from painternerCollection.utils import printonece
from torch.nn.utils.rnn import pad_sequence
from painternerlib.HyparaDebug import fputHypara,fputHDebug
from painternerlib.torch.DWrapModel import fWrapModel,DWrapModel
from painternerlib.torch.function import l2_sum
from painternerCollection.utils4net import preEmbedding
from painternerlib.torch.modelcollection import selfattention
import time
logger = logging.getLogger(__name__)
logging.basicConfig(level = logging.DEBUG,
                    format = '%(asctime)s[%(levelname)s] ---- %(message)s',
                    )
class Print():
    def __init__(self):
        self.action = True
    def cancel(self):
        self.action = False
    def open(self):
        self.action = True
    def exit(self):
        if self.action:
            exit()
    def __call__(self,*inputs):
        if not self.action:
            return
        print inputs

printf = Print()
printf.cancel()


## 比较git存储的和现在的差异,什么原因导致不能使训练收敛?
## max 50 350  --> None, 如果还按照原来的方法，可以取encoder后面一部分的数据(如350)。
## 测试embedding 初始化1 和 0.1训练是否有影响
## 分割后，每次输入的数据都小数据，所以cuda的memory显著降低?。
## 修改torch.zero --> requires_grad = True

## batch共同优化如果是mean，那么分割batch处理的时候就要mean处理了。

## learn
## is_packed --> from ..utils.rnn import PackedSequence
## GRU 公式，看是否有优化的空间。

"""
    临时决定，动态的model,单个model可以Wrap,但是Sequential,ModelList等还是预定义吧，否则太过于麻烦
    测试各种模块在优化过程中的w的正则值变化,有可能帮助分析收敛性。
"""

class MwAN(nn.Module):
    def __init__(self, vocab_size, embedding_size, encoder_size, drop_out=0.2):
        super(MwAN,self).__init__()

        self.drop_out = drop_out
        self.embedding_size = embedding_size
        self.encoder_size = encoder_size
        self.vocab_size = vocab_size
        self.class_size =  3
        self.drop_out=drop_out
        self.Hypara = fputHypara({      ## --> 移到trainnew.py 
            'sharegru_qa': True,
            "sharegru_qpa": False, ## 先测试一下上面超参，然后再修改测试。
            "fusion_with_selfattention": False,
            "bigru": False,
            "pass_after_q_conv_p": False,  ## 卷积后用线性pass
            "pretrained_embedding":True,
            "pretrained_embedding_fix": True,
            "pretrainedpath": 'data/pretrained_word2id_tencent.obj',
            "embedding2GPU": True,
            "l2_gru_ih": True,
            "l2_loss_gamma": 0.01,
            "W_before_conv": True,
            ## 11.3
            "conv_full":True,
            "Gru_after_conv": False,
            'loss_relation':False
        })
        self.Debug = fputHDebug({
            "forward_count":0,
            "loss_count_freq": 500
        })

        print "Hypara dict",self.Hypara
        print "Debug dict",self.Debug
        self.Debug_l2_gru_freq = 0
        self.Debug_loss_count_freq = 0
        
        # self.SWid = [6094,26301,73247,20460,14608]
        # ，6094 # 。26301# ；73247 # ？ 20460 # ：14608

        self.loss_count = {
            'loss1':0 ,
            'loss2':0 ,
            'l2_loss':0 ,
            'loss':0
        }
        self.embedding = None

        BiGRU = self.Hypara['bigru']

        if BiGRU == True:
            ## 双端GRU，这里采用方法：输入另一个gru前需要转化维度。
            self.W_encoder_p  = nn.ModuleList([
                nn.Linear(2*self.encoder_size,self.encoder_size) for _ in range(1)
            ])
        self.q_encoder = nn.GRU(input_size=embedding_size, hidden_size=encoder_size, batch_first=True,
                                bidirectional=BiGRU)
        self.p_encoder = nn.GRU(input_size=embedding_size, hidden_size=encoder_size, batch_first=True,
                                bidirectional=BiGRU)
        self.p2_endecoder = nn.GRU(input_size=embedding_size, hidden_size=encoder_size, batch_first=True,
                                bidirectional=BiGRU)
            
        if self.Hypara['sharegru_qa']:
            self.a_encoder = self.q_encoder
        else:
            self.a_encoder = nn.GRU(input_size=embedding_size, hidden_size=encoder_size / 2, batch_first=True,
                                    bidirectional=True)

        ## pq 联合attention
        factor = int(BiGRU)+1
        self.a_attention     =   nn.Linear(self.encoder_size*factor,1)

        self.selfattn = selfattention(encoder_size*factor,self.encoder_size*factor/2)

        if self.Hypara['loss_relation']:
            self.selfattn_relation = selfattention(encoder_size*factor,self.encoder_size*factor/2)
            self.Wrelation = nn.Linear(encoder_size*factor,2)
        
        if self.Hypara["W_before_conv"]:
            self.W_before_conv = nn.ModuleList(
                [nn.Linear(self.encoder_size*factor,self.encoder_size*factor) for _ in range(2) ]
            )
        if not self.Hypara['conv_full']:  ## We use add fusion
            assert not self.Hypara['W_before_conv']  ## this two is conficting to each other
            self.Wfusion_a = nn.ModuleList(
                [nn.Linear(self.encoder_size*factor,self.encoder_size*factor/2) for _ in range(2)]
            )
            self.Vfusion_a = nn.Linear(self.encoder_size*factor/2,1)

        self.initiation()

        ## dynamic module
        # """"
        #     不要用DWrapModel,这个方法暂时还没有加入参数组
        # """"
        # self.W_before_conv = [DWrapModel(), DWrapModel()] ## This is Error !!!!

        if self.Hypara["Gru_after_conv"]: 
            self.Gru_after_conv = nn.GRU(input_size=self.encoder_size*factor, hidden_size=self.encoder_size, batch_first=True,
                                bidirectional=BiGRU)

        self.init_embedding()

    def init_embedding(self):
        if self.embedding == None:
            if  self.Hypara["pretrained_embedding"]: 
                if  self.Hypara["pretrained_embedding_fix"]: 
                    dy_flag = False
                else:
                    dy_flag = True
                self.embedding = preEmbedding(self.vocab_size + 2,self.embedding_size,\
                                dynamic_embedding=dy_flag,path=self.Hypara['pretrainedpath'])
                if self.Hypara['embedding2GPU']:
                    self.embedding.cuda()
                    self.embedding.setcuda()
  
            else:
                self.embedding = nn.Embedding(self.vocab_size + 2,embedding_dim=self.embedding_size)
                initrange = 0.1
                nn.init.uniform_(self.embedding.weight, -initrange, initrange)
                if self.Hypara['embedding2GPU']:
                    self.embedding.cuda()
    def initiation(self):
        for module in self.modules():
            if isinstance(module, nn.Linear):
                nn.init.xavier_uniform_(module.weight, 0.1)


    def setcuda(self,patitial=False):  ## or use inherited function cuda
        self.vcuda = True
    def checkcuda(self):
        if not hasattr(self,'vcuda'):
            self.vcuda = False
        return self.vcuda

    def fempty(self,inputs):
        return inputs

    def conv_full(self,p,q):
        """
        Args:
            p,q:  shape is respectively [B,L1,M], [B,L2,M]
            it doesn't matther what's the order of p,q,because  p conv q === q conv p
        """
        assert  p.dim() == 3 and \
                p.dim() == p.dim() and \
                p.size(0) == q.size(0) and \
                p.size(2) == q.size(2)

        def execute(p,q,step,l1,l2):
            """
                patitial p conv q
            """
            l=l1+l2
            dim = p.dim()
            if dim == 3:
                if i <= l2:
                    temp = p[:,0:i,:] * q[:,l2-i:,:]
                elif i > l2 and i <= l1:
                    temp = p[:,i-l2:i,:] * q[:,0:,:]
                else:  ## i> l1
                    temp = p[:,i-l2:,:] * q[:,0:l-i,:]
                temp = temp.sum(1,keepdim=True)
            else:
                if dim == 1:
                    if i <= l2:
                        print p[0:i].size(), q[l2-i:].size()
                        temp = p[0:i] * q[l2-i:]
                    elif i > l2 and i <= l1:
                        temp = p[i-l2:i] * q[0:]
                    else:  ## i> l1
                        temp = p[i-l2:] * q[0:l-i]
                    temp = temp.sum(keepdim=True)

            return temp

        fusion_results = []
        l1 = p.size(1)
        l2 = q.size(1)
        for i in range(1,l1+l2):
            ## p conv q  === q conv p
            if l1 >= l2:
                reversed_ = torch.flip(q,[1])  ## reverse 1 dim
                output = execute(p,reversed_,i,l1,l2)
            else:
                reversed_ = torch.flip(p,[1])
                output = execute(q,reversed_,i,l2,l1)

            fusion_results.append(output)  ## After cat 1 dim, This will be shape [B,L1+L2,M]

        return fusion_results
        

    def passage_encoder(self,inputs,embedding,embedding2GPU):
        """
            encoder1, encoder3
            inputs假设分割成2份,每份50个单词,第一次输入first50 -->encoder1 --> states1,
                                                  states1 --> encoder3 --> states31
                                         第二次输入states31+first50 -->encoder1 --> states2
                                                  states1+states2 --> encoder3 --> states32 (可以保留上一次的结果，直接输入states2)
            
          ----------------------<-------------------------
          \-->--encoder1             \-----encoder2       |
                |      |                   |      |       |    ------->
        x-->----   out   hid----->---------    out hid---> --->|
                                                |_out or hid___|

            关于states1, states2要不要detach()，现在考虑是不需要的。
            假设 P(net1 | net2) 是net2的条件下，net1的概率。 就算这个net2的states是错误的，但是优化后也能使net2向着合理
            的方向前进。（即，我是在错误的条件下推导出来的，但是对于这个错误假设是合理的)
        
        """

        def padding(content,use_built_in=True): 
            """
            Args:
                content:
                    A list,  assume [N,128] a list length of B contains some tensors
                use_built_in:
                    是否使用内建的函数来自动pad 0
            """
            if use_built_in == False:
                # print len(content), content[0].size()
                maxlen = max( [ x.size(0) for x in content ] )
                r = torch.zeros(len(content),maxlen,content[0].size()[-1])
                if self.checkcuda():
                    r = r.cuda()
                for i,v in enumerate(content):
                    # print r.size(), v.size()
                    r[i,:len(v),:] = v
            else:
                r = pad_sequence(content,batch_first=True)
            return r

        #[B,N,Mt] --> N->被符号分割的段数，Mt->每一段的word数量, Mt 随N的索引而变化。      
        #[1,N,M,128] --> N->被符号分割的段数，M->每一段的word数量, 128->每一个word的维数
        coouts = []
        cohistates = []
        bhtl = []
        for ii in range(len(inputs)): ## batch 分开处理 [B,N,Mt] -> [N,Mt]
            # print inputs[ii][0]
            for i, input in enumerate(inputs[ii]):  ## 分段处理  [N,Mt] -> [Mt]
                input = torch.tensor(input)
                if embedding2GPU:
                    input = input.cuda()
                em = embedding(input).unsqueeze(0)  ## [1,Mt,128]
                if not embedding2GPU and self.checkcuda():
                    em = em.cuda()  
                if i == 0:
                    p,ht = self.p_encoder(em)   
                else:   
                    ## 修复encoder1用encoder2的state用做输入的bug,若不修复, 分段后状态就不能连续了，第一个总是0状态。
                    # p = torch.cat([cohistate, em],1) 
                    # p,ht = self.p_encoder(p)  
                    p,ht = self.p_encoder(em,cohistate)    ## hidden_state是双端也可以直接输入吗？
                    # print p.size(),em.size() p [1,Mt+1,128]      ht [1,b,state] (b==1)
                if self.Hypara['bigru']:
                    assert ht.size(1) == 1
                    ht2p2 = torch.cat([ht[0:1,:,:],ht[1:,:,:]],-1)
                    assert ht.dim() == 3
                    ## or we can use p[:,-1,:] directly
                    ht2p2 = self.W_encoder_p[0](ht2p2) 
                else:
                    ht2p2 = ht

                if i == 0:
                    inpstate = None
                else:          
                    inpstate = cohistate        ## hi_state原先的1维为batch大小，作为输入的话就是长度。
                coout,cohistate = self.p2_endecoder(ht2p2,inpstate)
                coouts.append(coout)
                
            cohistates.append(cohistate)  
            bhtl.append(torch.cat(coouts,1).squeeze(0))  # contains [N,128]
            coouts = []
        bhtl = padding(bhtl) ## [B,N,128]
 
            ## 以前测试htlist没有进行htlist=[]
            ## 以前测试的时候没有在q-b等的时候把为0的mask
            ## 添加测试计时

            ## 返回收集到的padding过的所有分段的output,或者每个sample的最后一个hi_state. 可以选择一种进行后续使用
        return bhtl, cohistates

    def forward(self,inputs,optimizer):
        [query, passage, answer,ids,label,is_train] = inputs
        query, answer,ids,label = (torch.LongTensor(query), 
                                        torch.LongTensor(answer),torch.LongTensor(ids),
                                        torch.Tensor(label))
        # print "query",query
        # print "passage",passage
        # print "answer",answer
        # print "label",label
        # exit()
        timeA = time.time()
        zp,p_state = self.passage_encoder(passage,self.embedding,self.Hypara['embedding2GPU'])
        timeB = time.time() - timeA
        if self.Debug['__Seg__0']:  print "passage_encoder time",timeB
        
        if self.Hypara['embedding2GPU']:
            query= query.cuda()
            answer = answer.cuda()
            label = label.cuda()
            ids = ids.cuda()

        q_embedding = self.embedding(query)
        a_embeddings = self.embedding(answer)

        if not self.Hypara['embedding2GPU'] and self.checkcuda():
            q_embedding= q_embedding.cuda()
            a_embeddings = a_embeddings.cuda()
            label = label.cuda()
            ids = ids.cuda()

        q,_ = self.q_encoder(q_embedding)  ## [32,10,128]

        ## fusion 方法: np.trapz ?? 不知道如何做
        ##        方法2：卷积.
        # print q.size(), p.size()  ## one test (8, 14, 64) (8, 42, 64)  
        timeA = time.time()
        if self.Hypara['W_before_conv']:
            # zp = self.W_before_conv[0](nn.Linear,zp,zp.size(-1),zp.size(-1))
            # q = self.W_before_conv[1](nn.Linear,q,q.size(-1),q.size(-1))
            zp = self.W_before_conv[0](zp)
            q = self.W_before_conv[1](q)

        if self.Hypara["conv_full"]:
                    ## test conv_full
            # zp,q = [torch.Tensor([[[1.,1],[2,2],[3,3],[4,4],[5,5]],[[1.,1],[2,2],[3,3],[4,4],[5,5]]]),
            #                 torch.tensor([[[1.,1],[2,2],[3,3],[4,4],[5,5]],[[1.,1],[2,2],[3,3],[4,4],[5,5]]])]
            zp = self.conv_full(zp,q) # length LL of [B,Lt,M]
        else:
            zp = torch.tanh(self.Wfusion_a[0](zp.unsqueeze(2))+self.Wfusion_a[1](q.unsqueeze(1)))
            zp = self.Vfusion_a(zp).squeeze(-1).bmm(q)

        if self.Hypara["Gru_after_conv"]:
            zp,_ = self.Gru_after_conv(zp)

        timeB = time.time() - timeA
        if self.Debug['__Seg__0']: print "conv_full time",timeB

        if self.Hypara["conv_full"]:
            zp = torch.cat(zp,1)  #[B,L1,M]

        if self.Hypara['fusion_with_selfattention']:
            pq = self.selfattn(zp)  #[B,L1,1]
            pq = pq.transpose(2,1).bmm(zp) # [b,1,M]
        else:
            pq = torch.sigmoid(zp).mean(1,keepdim=True) ## 注意，经过推导，如果直接求均值，与q,p全连接求均值的结果是一样的。
            pq = torch.softmax(pq,-1)

        a_embedding, _ = self.a_encoder(a_embeddings.view(-1, a_embeddings.size(2), a_embeddings.size(3)))
        a_score = F.softmax(self.a_attention(a_embedding), 1)
        a_output = a_score.transpose(2, 1).bmm(a_embedding).squeeze()
        a_embedding = a_output.view(a_embeddings.size(0), 3, -1)

        printf (a_embedding.size(), pq.size())
        predict = a_embedding.bmm( F.leaky_relu(pq.transpose(2,1),-1) ).squeeze(-1)  ## model.py用的是leaky_relu
        
        if self.Hypara['loss_relation']:
            relation_pq = self.selfattn_relation(zp)
            zp = relation_pq.transpose(2,1).bmm(zp).squeeze(1)
            relation_predict = torch.softmax( self.Wrelation(zp) ,-1)
        predict = torch.softmax(predict,-1)     # [32,3]                                          ## 用softmax比较好?
        if not is_train:
            if label.dim() == 5:
                return predict.argmax(1), relation_predict.argmax(1)
            else:
                return predict.argmax(1)

        # gather = predict.gather(-1,pos.unsqueeze(-1))
        # loss = -torch.log(gather).mean()
        if label.dim() == 3:  # [32,3,1]
            label = label.squeeze(-1)
            assert label.dim() == 2
        # print predict  ## >> ?? 查看每次预测是否平滑
        # print torch.log(predict)
        loss_every = (-torch.log(predict) * label[:,0:3]).sum(-1)  ## batch mean之前别忘了sum
        loss1 = ( loss_every ).mean(); self.loss_count['loss1'] += loss1
        loss = loss1
        if label.size(-1) == 5:  ## incle two relations label
            if  self.Hypara['loss_relation']:
                loss2 = (-torch.log(relation_predict) * label[:,3:]).sum(-1).mean(); self.loss_count['loss2'] += loss2
                loss += loss2
        else: 
            assert label.size(-1) == 3

        if self.Hypara['l2_gru_ih']:
            l2_loss = l2_sum("weight_ih_l0",[m for m in self.modules() if isinstance(m,nn.GRU)])
            loss = loss + l2_loss*self.Hypara['l2_loss_gamma']
            self.loss_count['l2_loss'] += l2_loss
            
        self.loss_count['loss'] += loss
        ## 打印监测各种loss
        self.Debug_loss_count_freq += 1
        if self.Debug_loss_count_freq % self.Debug['loss_count_freq'] == 0:
            for key in self.loss_count:
                print key, self.loss_count[key] / self.Debug['loss_count_freq']
                self.loss_count[key] = 0

        self.Debug["forward_count"] += 1
        # print(self.Debug['forward_count'])
        while False:
            time.sleep(1)

        printf.exit()
        return loss


"""
python trainnew.py --cuda --emsize 200 --nhid 200 --batch_size 8 --datascope 3000
计时测试:
    从epoch 2开始迅速变慢
        第一种记录: __Seg_0
        handle_data_for_train time 0.000269174575806
        passage_encoder time 0.0394749641418
        conv_full time 0.00345087051392
        其中 passage_encoder 的time最大,目测 mean 0.06 --> freq = 1/0.06 = 18.3
    
        第二种记录: __Seg_1
        handle_data_for_train time 0.0492260456085
        handle_data_for_train time 0.0587599277496
        handle_data_for_train time 0.0021231174469
        epcoh 1 dev acc is 51.666667, best dev acc 52.000000
        0.002
        77
        handle_data_for_train time 0.0307738780975
        handle_data_for_train time 1.08957004547    
        handle_data_for_train time 0.00225901603699
        目测handle_data_for_train time mean 1.4s

    找到原因:
        if epoch >= 2:    
        for p in model.embedding.parameters():
            p.requires_grad = True 

    比率：
        1.4/0.06 = 23
    应该与嵌入层没有放入GPU有关
    放入GPU时间测试。
        optimizer step time 0.00186491012573
        forward time 0.0254211425781
        backward time 0.0286309719086
        optimizer step time 0.00190591812134
        |------epoch 1 train error is 1.061524  eclipse 79.73%------|
        forward time 0.022901058197
        backward time 0.0281341075897
        optimizer step time 0.00228190422058


    网吧 --> stanford 命名实体识别，


    理解:
        假设别人一次给你不确定数个batch_size测试案例：(batch_size = Any) (多个batch之间更新应该简单利用optimizer.step即可
                    (注意之前不要使用optimizer.zero_grad()，待验证   >>?? ))
        监督 <--> 非监督的
        Q -> 不相关P --> （假设这是别人打乱了顺序) 讨论一下优化标签:
            如果是没有见过的Q-P对
                第一层： '我‘必须要选择一个答案，但是我并不知道别人打乱了顺序，所以我只能选择"无法确定“,而且如果”我“是权威，那么标签必须是0,0,1
                第二层： 真实判断了这是“无法确定”，那么我就需要反向传播记住这是一个“无法确定”的例子。（真实与“我”判断一定是一样的）
                第三层： 如果'我'是有一定记忆的，结果发现，记忆中之前有过Q，与另一个P之间的关系，但是现在却让我回答Q与P1的关系，所以我推断
                        这个问题一定是某人弄错了，所以我必须要从非监督的记忆池和外界中提取相关的梯度，把这两种情况分别重新输入，得分高的就是
                        应该是根据经验有关系的。视情况可能需要反梯度恢复。 -- 这应该是不需要的，因为即使不相关，判断是正确也是可以的。
            如果Q见过了（对应的P也见过了（优化过了)，而且此Q-P是配对的），而P1没有见过
                则此P1是伪造的，那么第一条： 优化标签应该是 33.3% 33.3% 33.3%
                                 第二条： 是不相关的，相关标签应该为 0， 1（不相关)
            相反如果P见过了，而Q1没有见过，情况也一样。
            
        P -> 单独 --> 33.3% 33.3% 33.3% 注意“无法确定”是其他的总和情况，而不是不明觉历，即不能是0,0,1
            即需要分成三层思维: 第一层： “我”必须要做出一个选择，“我”知道这个例子缺少了问题，那么这三个选项皆有1/3的概率,、
                                    那么就不能在不确定的情况下选择“无法确定”
                           : 第二层:  真实判给了PA，”我“选择了PB。知道了答案，先不急着记住，现在有一定概率推导出最有可能提出的这个问题，猜测问题。
                           ： 第三层： 猜对了问题，我可以总体来更新了。猜错了问题，
                    
        Q -> 单独 --> 33.3% 33.3% 33.3% 
                    第一层： 与上面的一致，但是是缺少了答案。
                    第二层: “我” 尝试已经给我的案例的答案集中得到一个答案，找了一个有可能是答案的语句。并开始猜测answer。
                    第三层: 这个语句是对应的问题回答， reward + 1，跳转到第四层, 如果不是，之前的answer记忆丢弃。 reward -1. 反向传播
                    第四层: 根据answer的结果来反向传播。
        重复输入 -> 来检测自己的记忆是否准确，合理。别人？ 第一阶网络处理的结果（输入的浓缩，比如变为原来的1/2,但是里面重要的思想都保存了下来)
                  是自己的印象，随机选择一些passage,由这个印象经过演算，应当能够推导出.
        
        MemoryPool反梯度恢复:
            （如果一件与事情”不符“，但是能推导出所有现有结论，说明这件事情必然有意义，所以只需要优化判断错误的）

        Generactive

        假设你有所有的权限看到所有的数据集： (batch_size = any, 动态batch_size)

        中间休息，更换一些结构，方法（如修改batch_size,梯度方向)，抛弃一些数据。
        
        所有的做完，剩下的就是让数据集收敛即可了。人工智能的下一步应该包括: 找到这些条件。
    翻译:
        A -> B
            B -> A --> loss 传播 

"""

"""
python trainnew.py --cuda --emsize 200 --nhid 200 --batch_size 8 --datascope 3000
        l2_gru_loss tensor(0.7529, device='cuda:0', grad_fn=<ThAddBackward>)
        10400  (forward 次数)
        epcoh 272 dev acc is 52.333333, best dev acc 54.333333

python trainnew.py --cuda --emsize 200 --nhid 200 --batch_size 8 --datascope 30000
Hypara dict {'sharegru_qpa': False, 'pretrained_embedding': True, 'sharegru_qa': True, 'fusion_with_selfattention': False, 'l2_gru_ih': True, 'embedding2GPU': True, 'l2_loss_gamma': 0.1, 'bigru': False, 'pretrainedpath': 'data/pretrained_word2id_tencent.obj', 'pretrained_embedding_fix': True, 'pass_after_q_conv_p': False}
Debug dict {'__Seg__1': False, '__Seg__0': False, 'forward_count': 0, 'epoch_to_update_embedding': -1, 'l2_gru_freq': 0}
    l2_gru_loss tensor(11.9945, device='cuda:0', grad_fn=<ThAddBackward>)
    |------epoch 22 train error is 1.068606  eclipse 95.97%------|
    l2_gru_loss tensor(9.1160, device='cuda:0', grad_fn=<ThAddBackward>)
    epcoh 22 dev acc is 56.470000, best dev acc 56.930000
    0.002
python trainnew.py --cuda --emsize 200 --nhid 200 --batch_size 8 --datascope 30000
Hypara dict {'sharegru_qpa': False, 'pretrained_embedding': True, 'sharegru_qa': True, 'fusion_with_selfattention': False, 'l2_gru_ih': True, 'embedding2GPU': True, 'l2_loss_gamma': 1, 'bigru': False, 'pretrainedpath': 'data/pretrained_word2id_tencent.obj', 'pretrained_embedding_fix': True, 'pass_after_q_conv_p': False}
Debug dict {'__Seg__1': False, '__Seg__0': False, 'forward_count': 0, 'epoch_to_update_embedding': -1, 'l2_gru_freq': 0}
    l2_gru_loss tensor(0.4751, device='cuda:0', grad_fn=<ThAddBackward>)
    l2_gru_loss tensor(0.4000, device='cuda:0', grad_fn=<ThAddBackward>)
    epcoh 7 dev acc is 56.243333, best dev acc 56.286667
    0.002
    “但是数据是波动的，可以波动到dev acc is 19.xxx
"""

"""
python trainnew.py  --cuda --emsize 200 --nhid 200 --batch_size 8 --datascope 300
('vacab_size', 96973)
Hypara dict {'sharegru_qpa': False, 'pretrained_embedding': True, 'sharegru_qa': True, 'fusion_with_selfattention': False, 'l2_gru_ih': True, 'embedding2GPU': True, 'l2_loss_gamma': 0.01, 'bigru': False, 'pretrainedpath': 'data/pretrained_word2id_tencent.obj', 'pretrained_embedding_fix': True, 'pass_after_q_conv_p': False}
Debug dict {'__Seg__1': False, '__Seg__0': False, 'datalogic': 'logic1', 'forward_count': 0, 'loss_count_freq': 50, 'epoch_to_update_embedding': -1}
('MwAN Model total parameters(explicit):', 744002)
train data size 300, dev data size 300
0.002
datalogic states: unRelated base SingleQ SingleP BERT

epcoh 0 dev acc is 50.666667, best dev acc 50.666667  --> 300
epcoh 0 dev acc is 54.800000, best dev acc 54.800000  --> 3000
epcoh 0 dev acc is 56.786667, best dev acc 56.786667  --> 30000 --> 后两轮并没有提升。

共同问题: loss 并不会降低，维持在某个值左右。
    loss tensor(0.3450, device='cuda:0', grad_fn=<DivBackward0>)
    loss1 tensor(0.3434, device='cuda:0', grad_fn=<DivBackward0>)
    l2_loss tensor(0.1527, device='cuda:0', grad_fn=<DivBackward0>)

--datascope 30000
W_before_conv': True --> 经过30%epoch,loss稍微降低，可能与BERT的标签概率平缓化有关。
    loss tensor(0.3676, device='cuda:0', grad_fn=<DivBackward0>)
    loss1 tensor(0.3657, device='cuda:0', grad_fn=<DivBackward0>)
    l2_loss tensor(0.1989, device='cuda:0', grad_fn=<DivBackward0>)
    |------epoch 0 train error is 0.365166  eclipse 31.97%------|
    loss2 0
    loss tensor(0.3634, device='cuda:0', grad_fn=<DivBackward0>)
    loss1 tensor(0.3618, device='cuda:0', grad_fn=<DivBackward0>)
    l2_loss tensor(0.1641, device='cuda:0', grad_fn=<DivBackward0>)
"""

"""
11.2 
    修复了conv_full bug: 1 没有sum的bug,
                        2  for i in range(1) -->  for i in range(1,l1+l2)  
                    并造了一个简单数据来测试完毕。
                    这两个bug导致完全没有正常进行conv
      use fusion_with_selfattention: True  datascope 3000
        epcoh 73 dev acc is 55.666667, best dev acc 56.200000
        0.002
        datalogic states: unRelated base SingleQ SingleP BERT
        loss2 0
        loss tensor(0.3503, device='cuda:0', grad_fn=<DivBackward0>)
        loss1 tensor(0.3382, device='cuda:0', grad_fn=<DivBackward0>)
        l2_loss tensor(1.2164, device='cuda:0', grad_fn=<DivBackward0>)
    修复encoder1用encoder2的state用做输入的bug:
                    应当用作encoder1的state


11.3
    修复了一个 self.init_embedding() 没有放入init()中的bug（这样不会被加入optimizer中的)

    零梯度是哪些东西？
    tensor(1.0587e-07, device='cuda:0')
    tensor(0.0000, device='cuda:0')
    tensor(7.5865e-07, device='cuda:0')
    tensor(5.3666e-09, device='cuda:0')
    tensor(0.0000, device='cuda:0')
    tensor(3.2154e-06, device='cuda:0')
    tensor(2.8211e-06, device='cuda:0')
    tensor(1.8832e-08, device='cuda:0')
    tensor(0.0000, device='cuda:0')
    tensor(4.9409e-06, device='cuda:0')
    tensor(3.1612e-09, device='cuda:0')
    tensor(1.9308e-11, device='cuda:0')
    tensor(2.2431e-07, device='cuda:0')
    tensor(5.5449e-08, device='cuda:0')
    tensor(1.1143e-07, device='cuda:0')
    tensor(7.4203e-09, device='cuda:0')
    tensor(6.5731e-06, device='cuda:0')
    tensor(1.6044e-06, device='cuda:0')
    tensor(1.8578e-07, device='cuda:0')
    tensor(1.0475e-08, device='cuda:0')
    tensor(0.0000, device='cuda:0')
    tensor(8.8027e-06, device='cuda:0')
    tensor(1.3385e-07, device='cuda:0')
    tensor(3.8283e-08, device='cuda:0')
    tensor(0.0000, device='cuda:0')
    tensor(6.3157e-06, device='cuda:0')
    tensor(0., device='cuda:0')
    tensor(0., device='cuda:0')
    tensor(1.8064e-09, device='cuda:0')
    tensor(7.6697e-12, device='cuda:0')
    tensor(6.6859e-06, device='cuda:0')
    tensor(3.4694e-16, device='cuda:0')
    tensor(5.2893e-07, device='cuda:0')
    tensor(0.0002, device='cuda:0')
    tensor(1.6352e-07, device='cuda:0')
    tensor(0.0000, device='cuda:0')

"""
