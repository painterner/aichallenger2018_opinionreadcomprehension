# -*- coding: utf-8 -*-
import torch
from torch import nn
import torch.nn.init as init
from torch.autograd import Variable
from torch.nn import functional as F
from termcolor import colored
import logging
import sys,os
import random
from Dlinear import DLinear_ParaGroup
from painternerCollection.utils4net import preEmbedding

from painternerCollection.RLqtest2 import RLqtest
from painternerCollection.utils import printonece1
logger = logging.getLogger(__name__)
logging.basicConfig(level = logging.DEBUG,
                    format = '%(asctime)s[%(levelname)s] ---- %(message)s',
                    )
grapstr="""
        em      wid
        |         |
        |       dlinear     64 -> 64*12
        |         |
64*2   full     full       64*12  -> 64*2
        |________|
            |
            gru             64*2 -> 64*2
            |
            full            64*2 -> 64
            |
            dlinear         64 -> 1   

"""

class NoCudaEmbedding(nn.Module):
    """ use this seperate with main function, spare cuda memory"""
    def __init__(self,vocab_size, embedding_size):
        super(NoCudaEmbedding,self).__init__()
        self.vocab_size = vocab_size
        # self.embedding = preEmbedding(vocab_size + 2,embedding_size)
        self.embedding = nn.Embedding(vocab_size + 2,embedding_size)
        initrange = 0.1
        nn.init.uniform_(self.embedding.weight, -initrange, initrange)

    def forward(self,inputs):
        [query, passage, answer,ids,pos, is_train] = inputs

        tail_tensor_q = torch.tensor([[self.vocab_size+1]]*query.shape[0])
        # if(self.vcuda==True):
        #     tail_tensor_q = tail_tensor_q.cuda()
        qp_em_id = torch.cat([query,tail_tensor_q,passage],-1)

        qp_em = self.embedding(qp_em_id)
        a_embeddings = self.embedding(answer)
        qp_em_id = qp_em_id.float().unsqueeze(1)
        return qp_em,a_embeddings,qp_em_id,answer

## noting: from torch.optim.optimizer import * 导入了required，注意不要覆盖它,否则Dlinear会有判断隐患。
class MwAN(nn.Module):
    def __init__(self,vocab_size, embedding_size,encoder_size,drop_out):
        super(MwAN, self).__init__()

        self.vpatitialcuda = False
        self.vcuda = False
        self.onece = False
        self.vcount = 0

        self.drop_out = drop_out
        self.embedding_size = embedding_size
        self.encoder_size = encoder_size
        self.hidden_size = encoder_size
        self.vocab_size = vocab_size
        self.class_size =  3
        self.out_size=[
            8,
            4,
            1, ## 2*bi =4
            1
        ]

        # self.embedding = nn.Embedding(vocab_size + 2, embedding_dim=embedding_size)
        # self.embedding = preEmbedding(vocab_size + 2,embedding_size)

        self.dlinear0 = DLinear_ParaGroup(self.embedding_size*self.out_size[0])
        # self.dlinear01 = DLinear_ParaGroup(self.embedding_size*self.out_size[0])
        # self.dlinear02 = DLinear_ParaGroup(self.embedding_size*self.out_size[0])
        # self.dlinear03 = DLinear_ParaGroup(self.embedding_size*self.out_size[0])
        self.full0 = nn.Linear(self.embedding_size*self.out_size[0],self.embedding_size*self.out_size[1])
        # self.full01 = nn.Linear(self.embedding_size*self.out_size[0],self.embedding_size*self.out_size[1])
        # self.full02 = nn.Linear(self.embedding_size*self.out_size[0],self.embedding_size*self.out_size[1])
        # self.full03 = nn.Linear(self.embedding_size*self.out_size[0],self.embedding_size*self.out_size[1])
        
        self.full1 = nn.Linear(self.embedding_size,self.embedding_size*self.out_size[1])
        self.gru0 = nn.GRU(self.embedding_size*self.out_size[1],self.embedding_size*self.out_size[2],bidirectional=True)
        
        self.full2 = nn.Linear(self.embedding_size*self.out_size[2]*2,self.embedding_size*self.out_size[3])
        self.dlinear1 = DLinear_ParaGroup(1)

        self.initiation()

    def initiation(self):
        initrange = 0.1
        # nn.init.uniform_(self.embedding.weight, -initrange, initrange)
        for module in self.modules():
            if isinstance(module, nn.Linear):
                nn.init.xavier_uniform_(module.weight, 0.1)

    def analyse(self,inputs,wid,optimizer):
        dlinear0 = self.dlinear0(wid,optimizer)   ## 对于answer来说可能过大，因为有可能passage 500,answer 1,稀疏矩阵。
        # dlinear01 = self.dlinear01(wid,optimizer)   ## 对于answer来说可能过大，因为有可能passage 500,answer 1,稀疏矩阵。
        # dlinear02 = self.dlinear02(wid,optimizer)   ## 对于answer来说可能过大，因为有可能passage 500,answer 1,稀疏矩阵。
        # dlinear03 = self.dlinear03(wid,optimizer)   ## 对于answer来说可能过大，因为有可能passage 500,answer 1,稀疏矩阵。
        full0_wid = torch.sigmoid(self.full0(dlinear0))
        # full01_wid = torch.tanh(self.full01(dlinear01))
        # full02_wid = torch.tanh(self.full02(dlinear02))
        # full03_wid = torch.tanh(self.full03(dlinear03))

        full1_w = self.full1(inputs)
        # print full1_w.size(),full0_wid.size()
        i = full1_w * full0_wid
        gru0,_ = self.gru0(i)
        gru0 = gru0 
        outputs = torch.tanh(self.full2(gru0))

        return outputs

    def setcuda(self):
        self.vcuda = True
        self.dlinear0.setcuda()
        self.dlinear1.setcuda()
        # self.embedding.setcuda()
        pass

    def forward(self,inputs,inputs2,optimizer):
        [query, passage, answer,ids,pos, is_train] = inputs
        qp_em,a_embeddings,qp_em_id,answer = inputs2

        # tail_tensor_q = torch.tensor([[self.vocab_size+1]]*query.shape[0])
        # if(self.vcuda==True):
        #     tail_tensor_q = tail_tensor_q.cuda()
        # qp_em_id = torch.cat([query,tail_tensor_q,passage],-1)

        # qp_em = self.embedding(qp_em_id)
        # a_embeddings = self.embedding(answer)
        # qp_em_id = qp_em_id.float().unsqueeze(1)

        qp = self.analyse(qp_em,qp_em_id,optimizer).transpose(2,1)            
        a=[]
        for i in range(3):
            answer_id = answer[:,i,:].float().unsqueeze(1)
            x = self.analyse(a_embeddings[:,i,:,:],answer_id,optimizer)
            a.append(x.transpose(2,1))    

        def calda(a,optimizer):
            return [torch.sigmoid(self.dlinear1(a[i],optimizer)) for i in range(3)]
        # if(qp.size()[1] >= a[0].size()[1]):
        #     c1,c2 = calda(a,optimizer),self.dlinear1(qp,optimizer)  
        # else:
        #     c2,c1 = self.dlinear1(qp,optimizer),calda(a,optimizer)
        # print qp.shape , a[0].shape
        c2,c1 = torch.sigmoid(self.dlinear1(qp,optimizer)),calda(a,optimizer)

        a_cos = []
        for i in range(3):
            c1[i] = c1[i].squeeze(1).squeeze(-1)
            c2 = c2.squeeze(1).squeeze(-1)
            cos = (c1[i]*c2).sum(-1) / ( torch.sqrt((c1[i]**2).sum(-1)) * torch.sqrt((c2**2).sum(-1)) )
            a_cos.append(cos.unsqueeze(-1))

        self.vcount += 1
        if self.vcount > 10:
            pass
            # exit()

        predict = torch.softmax( torch.cat(a_cos,-1) ,-1)
        if not is_train:
            return predict.argmax(1)

        gather = predict.gather(-1,pos.unsqueeze(-1))
        loss = -torch.log(gather).mean()

        return loss
