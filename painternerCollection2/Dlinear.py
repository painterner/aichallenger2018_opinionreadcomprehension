# -*- coding: utf-8 -*-
import torch
from torch import nn
import torch.nn.init as init
from torch.autograd import Variable
from torch.nn import functional as F
from termcolor import colored
import logging
import sys,os
import random
from torch.optim.optimizer import *


class DLinear(nn.Module):
    """
    Noting !!!!
    we will remove and add parameters to optimizer
    """
    def __init__(self,outputsize = 1):
        super(DLinear,self).__init__()
        self.inputsize = 1
        self.outputsize = outputsize
        ## 由于初始化的时候所有模块都加入到一个param_group，所以需要利用一个trick。
        self.once = False
        self.vcuda = False

        self.dlinear = nn.Linear(self.inputsize,self.outputsize)
        self.useoptimizer = True
        
        if (not self.useoptimizer):
            for p in self.dlinear.parameters():
                p.requires_grad = False

    def cuda(self, device=None):
        super(DLinear,self).cuda(device)
        self.vcuda = True

    def prermPara_returnId(self,optimizer):
        ## optimizer.param_groups content:
        ## [{'params':list1,'eps':1e-8}, {'params':list2,'eps':1e-6}]
        ## 初始化时所以模块参数都在list1中。
        param_set = set()
        for i,group in enumerate(optimizer.param_groups):
            for ii,para in enumerate(group['params']):
                lpara = list(para) 
                param_set.update(set(lpara))
                if not param_set.isdisjoint(set(list(self.dlinear.parameters()))):
                    # print i,ii
                    return i,ii
        # print optimizer.param_groups,len(optimizer.param_groups)
        # print list(self.dlinear.parameters())
        raise Exception("No find pre param in optimizer")

    def insteadParam(self,optimizer,index):
        ## 假设在相应的参数组中是线性排列的,并且每次更新不会改变tensor的个数。
        for i in range(len(list(self.dlinear.parameters()))):
            optimizer.param_groups[index[0]][index[1]+i] = list(self.dlinear.parameters())[i]

        # print optimizer.param_groups[index]
        # exit()

    ## 有个问题: 如果同一次优化中输入两次不同的形状，会不会导致graph丢失? 有待利用梯度结果来验证一下。
    def forward(self,inputs,optimizer):
        # print self.dlinear.weight
        isize = inputs.size()[-1]
        if(self.inputsize < isize):
            if (self.useoptimizer):
                print "entry"
                # print list(self.dlinear.parameters())
                index = self.prermPara_returnId(optimizer)
            x = [torch.tensor(x.detach()) for x in list(self.dlinear.parameters())]
            zero0 = torch.zeros((x[0].size()[0],isize-self.inputsize))
            if self.vcuda:
                zero0 = zero0.cuda()
            ## 补零时注意weight参数与实际是转置的关系。
            self.dlinear = nn.Linear(isize,self.outputsize)
            self.dlinear.weight=nn.Parameter(torch.cat([x[0],zero0],-1))  ## exchange bias and weight to test
            self.dlinear.bias = nn.Parameter(x[1])
            if(self.useoptimizer):
                self.insteadParam(optimizer,index)

            self.inputsize = isize
        elif self.inputsize > isize:
            zero0 = torch.zeros(inputs.size()[0],inputs.size()[1],self.inputsize-isize)
            if self.vcuda:
                zero0 = zero0.cuda()
            # print zero0.size(), inputs.size(), zero0.dtype, inputs.dtype
            inputs = torch.cat([inputs,zero0],-1)

        outputs = self.dlinear(inputs)
        return outputs

class DLinear_ParaGroup(nn.Module):
    """
    Noting !!!!
    we will remove and add parameters to optimizer
    use similary as optimizer.add_param_group()"""
    def __init__(self,outputsize = 1):
        super(DLinear_ParaGroup,self).__init__()
        self.inputsize = 1
        self.outputsize = outputsize
        ## 由于初始化的时候所有模块都加入到一个param_group，所以需要利用一个trick。
        self.once = False
        self.vcuda = False

    # def cuda(self, device=None):
    #     super(DLinear_ParaGroup,self).cuda(device)
    #     self.vcuda = True
    # no effect from wrap model(top model) unless direct invoke: 
    # https://discuss.pytorch.org/t/why-does-nn-module-cuda-use--apply-instead-of-apply/8912

    def setcuda(self):
        self.vcuda = True

    def getDlinear(self,optimizer):
        if not self.once:
            self.once = True

            self.dlinear = nn.Linear(self.inputsize,self.outputsize)
            if(self.vcuda):
                self.dlinear.cuda()
            self.useoptimizer = True
            self.lastcheck = self.dlinear.bias[:10]
            self.insteadParam(optimizer,index="hello",append=True)            

            if (not self.useoptimizer):
                for p in self.dlinear.parameters():
                    p.requires_grad = False

    def prermPara_returnId(self,optimizer):
        ## optimizer.param_groups content:
        ## [{'params':list1,'eps':1e-8}, {'params':list2,'eps':1e-6}]
        ## 初始化时所以模块参数都在list1中。
        param_set = set()
        for i,group in enumerate(optimizer.param_groups):
            param_set.update(set(group['params']))
            if not param_set.isdisjoint(set(list(self.dlinear.parameters()))):
                print i
                return i
        # print optimizer.param_groups,len(optimizer.param_groups)
        # print list(self.dlinear.parameters())
        raise Exception("No find pre param in optimizer")

    def insteadParam(self,optimizer,index,append=False):
        ## for name, default in self.defaults.items()这句话有没有影响?
        # print optimizer.param_groups[index]
        if append:
            ## check Dlinear是否在某个参数组中
            param_set = set()
            for group in optimizer.param_groups:
                param_set.update(set(group['params']))
            if not param_set.isdisjoint(set(list(self.dlinear.parameters()))):
                raise Exception("param append already existed error")

        ## 如果没有在某个参数组中，则新建一个包含Dlinear的参数组
        param_group = {'params':list(self.dlinear.parameters()) }
        for name, default in optimizer.defaults.items():
            if default is required and name not in param_group:
                raise ValueError("parameter group didn't specify a value of required optimization parameter " +
                                 name)
            else:
                param_group.setdefault(name, default)

        ## 添加或者更新参数组.
        if append:
            optimizer.param_groups.append(param_group)
        else:
            optimizer.param_groups[index] = param_group

        # print optimizer.param_groups[index]
        # exit()
        pass

    ## 有个问题: 如果同一次优化中输入两次不同的形状，会不会导致graph丢失? 有待利用梯度结果来验证一下。
    def forward(self,inputs,optimizer):
        self.getDlinear(optimizer)
        # print (self.dlinear.bias[:10] == self.lastcheck)
        # self.lastcheck = torch.tensor(self.dlinear.bias[:10])
        isize = inputs.size()[-1]
        if(self.inputsize < isize):
            if (self.useoptimizer):
                print "entry dlinear size"
                # print list(self.dlinear.parameters())
                index = self.prermPara_returnId(optimizer)
            x = [torch.tensor(x.detach()) for x in list(self.dlinear.parameters())]
            zero0 = torch.zeros((x[0].size()[0],isize-self.inputsize))
            if(self.vcuda):
                zero0 = zero0.cuda()
            ## 补零时注意weight参数与实际是转置的关系。
            self.dlinear = nn.Linear(isize,self.outputsize)
            self.dlinear.weight=nn.Parameter(torch.cat([x[0],zero0],-1))  ## exchange bias and weight to test
            self.dlinear.bias = nn.Parameter(x[1])
            if(self.useoptimizer):
                self.insteadParam(optimizer,index)

            self.inputsize = isize
        elif self.inputsize > isize:
            zero0 = torch.zeros(inputs.size()[0],inputs.size()[1],self.inputsize-isize)
            if self.vcuda:
                zero0 = zero0.cuda()
            # print zero0.size(), inputs.size(), zero0.dtype, inputs.dtype
            inputs = torch.cat([inputs,zero0],-1)

        outputs = self.dlinear(inputs)
        return outputs
