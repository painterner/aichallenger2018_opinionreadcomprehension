# -*- coding: utf-8 -*-
import torch
from torch import nn
import torch.nn.init as init
from torch.autograd import Variable
from torch.nn import functional as F
from termcolor import colored
import logging
import sys,os
import random

from painternerCollection.RLqtest2 import RLqtest
from painternerCollection.utils import printonece1
logger = logging.getLogger(__name__)
logging.basicConfig(level = logging.DEBUG,
                    format = '%(asctime)s[%(levelname)s] ---- %(message)s',
                    )

str_str_str = """

"""

## 这个网络是假设gru1可以同时优化两种功能（或者3种），使得快速判断问题与第一阶段的passage判断
class MwAN(nn.Module):
    def __init__(self,vocab_size, embedding_size,encoder_size,drop_out):
        super(MwAN, self).__init__()

        self.vcuda = False

        self.drop_out = drop_out
        self.embedding_size = embedding_size
        self.encoder_size = encoder_size
        self.hidden_size = encoder_size
        self.vocab_size = vocab_size
        self.class_size =  3

        self.a = None
        self.q = None

        self.embedding = nn.Embedding(self.vocab_size + 2, embedding_dim=embedding_size)

        self.gru1 = nn.GRU(input_size=embedding_size, hidden_size=encoder_size, batch_first=True,
                        bidirectional=False)

        self.gru2 = nn.GRU(input_size=encoder_size, hidden_size=encoder_size, batch_first=True,
                        bidirectional=False)

        # self.W = nn.Linear(self.encoder_size,self.encoder_size,bias=False)


    def phase1(self,inputs,oresult,batch_size=None):
        r,hd= self.gru1(inputs)  ## 因为结果有 tensor ，所以需要处理两遍，bidirection 能解决此问题吗？？
        r,hd = self.gru1(r,hx=hd)   ## hx 是hidden的意思吗?
        # print r.shape
        if (oresult == 'a'):
            self.a = torch.tensor( r[:,-1,:] ) ## [N*L1,L+1,C] --> [N*L1,C]
            # print self.a.shape,r.size(2)
            self.a = self.a.view(batch_size,-1,r.shape[2])
            return
        elif oresult == 'q':
            self.q = torch.tensor(r[:,-1,:]).unsqueeze(1)        ## [N, 1, C]
            return
        assert oresult == 'p'
        return r                                      ## [N, L+1 ,C]

    def setcuda(self):
        self.vcuda=True
        
    def forward(self,inputs):
        [query, passage, answer,ids, is_train] = inputs
        ## add a tensor for informing gru this is end flag, 'you' need to judge right now!
        tail_tensor_p = torch.tensor([[self.vocab_size+1]]*passage.shape[0])
        tail_tensor_q = torch.tensor([[self.vocab_size+1]]*query.shape[0])  ### how insert batch
        tail_tensor_a = torch.tensor([[[self.vocab_size+1]]*answer.shape[1]]*answer.shape[0])

        if(self.vcuda==True):
            tail_tensor_p = tail_tensor_p.cuda()
            tail_tensor_q = tail_tensor_q.cuda()
            tail_tensor_a = tail_tensor_a.cuda()
        passage = torch.cat([passage,tail_tensor_p],-1)
        query = torch.cat([query,tail_tensor_q],-1)
        answer = torch.cat([answer,tail_tensor_a],-1)
        if(self.embedding_size > 1):
            q_embedding = self.embedding(query)                    ## N, L, C  --> ## N, C, L
            p_embedding = self.embedding(passage)
            a_embeddings = self.embedding(answer)                  ## N, L1，L，C  --> ## N,L1_L,C
        else:    
            q_embedding  = query.unsqueeze(2).float()
            p_embedding  = passage.unsqueeze(2).float()
            a_embeddings = answer.unsqueeze(3).float()

        a = a_embeddings.view(-1, a_embeddings.size(2), a_embeddings.size(3))
        self.phase1(a,'a',batch_size=a_embeddings.shape[0])
        self.phase1(q_embedding,'q')
        p = self.phase1(p_embedding,'p')   ## [N,L,C]  

        # q_p = p-self.W(q)
        # print self.q.size(), p.size()
        q_p = p*self.q
        # q_p = p-self.q    ## 可以测试，如果相减的形式导致self.q没有对梯度有贡献的，那么从两者应该能看出差距。
                          ## check  softmax 等函数有没有能让梯度捕捉到函数形式的操作     >> ?? 
                          ## 双向循环网络是输入反向？
                          ## 两者之间信息交互应该是怎么样的，这里假设只需相减或相除（因为由相同的映射转化
                          ## 而来，所以特性应当是类似的，所以不相加或相乘），但是相除时会有除零的问题，改成求之间的
                          ## 几阶梯度？ 可以搜索物理上和生物上的这些现象，找到一个好的联系方式。
        q_p = torch.cat([q_p[:,-1,:].unsqueeze(1),q_p[:,:,:]], 1)  ## 最后一个向量转换到最前面,保留最后一个以便下一个模块判断。
        q_p,_= self.gru2( q_p )         ## [N,L,C]  
        
        tail_q_p = q_p[:,-1,:].unsqueeze(2)         ## [N,C,1]
                                       ## [N,L1,C]
        # print self.a.shape, tail_q_p.shape
        q_p_q =self.a.bmm(tail_q_p).squeeze()         ## a^2-ab 好，还是 a-b好？ 即线性好还是非线性好的问题。
        predict = torch.softmax(q_p_q,-1)

        if not is_train:
            return predict.argmax(-1)

        loss  = -torch.log(predict[:,0]).mean()

        return loss

