# -*- coding: utf-8 -*-
import torch
from torch import nn
import torch.nn.init as init
from torch.autograd import Variable
from torch.nn import functional as F
from termcolor import colored
import logging
import sys,os
import random

from painternerCollection.RLqtest2 import RLqtest
from painternerCollection.utils import printonece1
logger = logging.getLogger(__name__)
logging.basicConfig(level = logging.DEBUG,
                    format = '%(asctime)s[%(levelname)s] ---- %(message)s',
                    )


class CRNN(nn.Module):
    def __init__(self,rnnisize,rnnhsize,rnnbidirect=False,ichannels=1,ochannels=1,k=0,dilation=1,stride=1,padding=0):
        super(CRNN,self).__init__()
        self.ochannels = ochannels
        self.k = k

        if isinstance(k,int) and k == 0:
            pass
        elif isinstance(k,int):
            self.cnn = nn.Conv1d(ichannels,ochannels,k,stride=stride,padding=padding,dilation=dilation)
        elif len(k) == 2:
            self.cnn = nn.Conv2d(ichannels,ochannels,k,stride=stride,padding=padding,dilation=dilation)
        
        self.rnn = nn.GRU(input_size=rnnisize, hidden_size=rnnhsize, batch_first=True,
                    bidirectional=rnnbidirect)

    def forward(self,inputs):
        if isinstance(self.k,int) and self.k == 0:
            rnn,ho = self.rnn(inputs)
        else: 
            cnn = self.cnn(inputs)
            rnn,ho = self.rnn(cnn[:,0,:,:])
        for i in range(1,self.ochannels):  ## concatenate输入?
            rnn,ho = self.rnn(cnn[:,i,:,:],ho)

        return rnn

class LinearLadder(nn.Module):
    def __init__(self,ladderLength,maxisize,stride=2):
        super(LinearLadder,self).__init__()
        self.ladderLength = ladderLength
        self.ladder = []
        for i in range(ladderLength):
            osize = maxisize-stride-i*stride
            assert osize > 0
            linear = nn.Linear(maxisize-i*stride,osize)
            self.ladder.append(linear)

    def forward(self,inputs):
        assert len(inputs) == 3
        outputs = []
        for i in range(self.ladderLength):
            output = self.ladder[i](inputs[:,:,i])
            outputs.append(output)
        
        return outputs


class DLinear():
    """
    Noting !!!!
    Must Not used in init process,cause we will remove and add parameters to optimizer
    use optimizer.add_param_group()"""
    def __init__(self,optimizer):
        # super(DLinear,self).__init__()
        self.inputsize = 1
        self.outputsize = 1
        self.dlinear = nn.Linear(self.inputsize,self.outputsize)
        self.dlinear.cuda()

        optimizer.add_param_group({'params':self.dlinear.parameters()})

    ## 有个问题: 如果同一次优化中输入两次不同的形状，会不会导致graph丢失? 有待利用梯度结果来验证一下。
    def __call__(self,inputs,optimizer):
        isize = inputs.size()[-1]
        if(self.inputsize < isize):
            optimizer.param_groups = optimizer.param_groups[:-1] ## 移除最后一个参数组。
            x = [torch.tensor(x.detach()) for x in list(self.dlinear.parameters())]
            zero0 = torch.zeros((x[0].size()[0],isize-self.inputsize))
            zero0 = zero0.cuda()
            ## 补零时注意weight参数与实际是转置的关系。
            self.dlinear = nn.Linear(isize,self.outputsize)
            self.dlinear.weight=nn.Parameter(torch.cat([x[0],zero0],-1))  ## exchange bias and weight to test
            self.dlinear.bias = nn.Parameter(x[1])
            optimizer.add_param_group({'params': self.dlinear.parameters()})

            self.inputsize = isize
        elif self.inputsize > isize:
            zero0 = torch.zeros(inputs.size()[0],inputs.size()[1],self.inputsize-isize)
            zero0 = zero0.cuda()
            inputs = torch.cat([inputs,zero0],-1)

        outputs = self.dlinear(inputs)
        return outputs


class MwAN(nn.Module):
    def __init__(self,vocab_size, embedding_size,encoder_size,drop_out):
        super(MwAN, self).__init__()

        def same1d(k):
            assert k%2 == 1
            padding = k/2
            return k,padding

        self.vpatitialcuda = False
        self.vcuda = False
        self.onece = False

        self.drop_out = drop_out
        self.embedding_size = embedding_size
        self.encoder_size = encoder_size
        self.hidden_size = encoder_size
        self.vocab_size = vocab_size
        self.class_size =  3

        self.embedding = nn.Embedding(vocab_size + 2, embedding_dim=embedding_size)
        
        self.gru0 = nn.GRU(embedding_size,encoder_size/2)

        self.CATLAYER1 =  nn.Linear(encoder_size/2,encoder_size)
        self.crnn0 = CRNN(embedding_size,encoder_size*3/2)
        ## k = (3,encoder_size*3/2）后会把-1维度压缩，不是想要的
        self.crnn1 = CRNN(encoder_size*3/2,encoder_size*3/2,rnnbidirect=True,ochannels=3,k=(3,3),padding=(1,1))

        self.full0 = nn.Linear(encoder_size*3,encoder_size*3/2)

    def analyse(self,inputs):
        gru0,_ = self.gru0( inputs )
        
        cat1 = self.CATLAYER1(gru0)
        crnn0 = self.crnn0(cat1)
        crnn0 = crnn0.unsqueeze(1)
        crnn1 = self.crnn1(crnn0)

        extract = crnn1.size()[1]/3
        if extract < 1:
            extract == 1
        last = crnn1[:,-extract:,:]
        outputs = self.full0(last)
        return outputs

    def setcuda(self):
        self.vcuda = True
        pass

    def forward(self,inputs,optimizer):
        [query, passage, answer,ids, is_train] = inputs

        tail_tensor_q = torch.tensor([[self.vocab_size+1]]*query.shape[0])
        if(self.vcuda==True):
            tail_tensor_q = tail_tensor_q.cuda()
        qp_em = torch.cat([query,tail_tensor_q,passage],-1)

        qp_em = self.embedding(qp_em)
        a_embeddings = self.embedding(answer)

        qp = self.analyse(qp_em).transpose(2,1)            ## [32,33=100/3,192] --> [32,192,33]
        a=[]
        for i in range(3):
            x = self.analyse(a_embeddings[:,i,:,:])
            a.append(x.transpose(2,1))    

        # print qp.size(), a[0].size()

        if(not self.onece):
            self.dlinear = DLinear(optimizer)
        def calda(a,optimizer):
            return [self.dlinear(a[i],optimizer) for i in range(3)]
        if(qp.size()[1] >= a[0].size()[1]):
            c1,c2 = calda(a,optimizer),self.dlinear(qp,optimizer)   ## 先输入小size的，保证dlinear不会在一次优化中改变。
        else:
            c2,c1 = self.dlinear(qp,optimizer),calda(a,optimizer)
        ## c1 = [[32,1,192]*3]  c2 = [32,1,192]
       
        a_cos = []
        for i in range(3):
            c1[i] = c1[i].squeeze(1).squeeze(-1)
            c2 = c2.squeeze(1).squeeze(-1)
            # bi = self.CATLAYER2(qp).bmm(a[i].transpose(2,1))   #  M x L
            # print c1[i].size(), c2.size()
            cos = (c1[i]*c2).sum(-1) / ( torch.sqrt((c1[i]**2).sum(-1)) * torch.sqrt((c2**2).sum(-1)) )
            a_cos.append(cos.unsqueeze(-1))

        predict = torch.softmax( torch.cat(a_cos,-1) ,-1)
        if not is_train:
            return predict.argmax(1)
        loss = -torch.log(predict[:,0]).mean()

        return loss


        