# -*- coding: utf-8 -*-
import torch
from torch import nn
import torch.nn.init as init
from torch.autograd import Variable
from torch.nn import functional as F
from termcolor import colored
import logging
import sys,os
import random

from painternerCollection.RLqtest2 import RLqtest
from painternerCollection.utils import printonece1
logger = logging.getLogger(__name__)
logging.basicConfig(level = logging.DEBUG,
                    format = '%(asctime)s[%(levelname)s] ---- %(message)s',
                    )

class umConv1d5d(nn.Module):
    
    def __init__(self,kernel=(3,64),padding=1,stride=1,ochannels=3):
        """ ichannels === 1 
        """
        super(umConv1d5d,self).__init__()
        self.kernel = kernel
        self.stride = stride
        self.padding = padding
        self.ochannels = 3

        self.I = torch.eye(kernel[0])
        self.linear = nn.Linear(kernel[0],kernel[1])

    def initial(self):
        pass

    def forward(self,inputs):
        assert len(inputs.shape) == 3
        self.padding0 = torch.zeros( (inputs.shape[0],self.padding,self.kernel[1]) )
        self.padding1 = torch.zeros( (inputs.shape[0],self.padding,self.kernel[1]) )
        padding_i = torch.cat([self.padding0,inputs,self.padding1],1)
        length = (padding_i.shape[1] - self.kernel[0]) / self.stride + 1

        outputs = []
        for i in range(length):
            output = padding_i[:,i*self.stride:i*self.stride+3,:] * self.linear(self.I)
            outputs.append(output)
        outputs = torch.cat(outputs,1)
        return outputs

# class CRNN(nn.Module):
#     def __init__(self,rnnisize,rnnhsize,rnnbidirect=False,ichannels=1,ochannels=1,k=0,dilation=1,stride=1,padding=0):
#         super(CRNN,self).__init__()
#         self.ochannels = ochannels
#         self.k = k

#         if isinstance(k,int) and k == 0:
#             pass
#         elif isinstance(k,tuple):
#             self.cnn = umConv1d5d(kernel=k,stride=stride,padding=padding)
#         else:
#             raise Exception("k must be 0 or tuple")
        
#         self.rnn = nn.GRU(input_size=rnnisize, hidden_size=rnnhsize, batch_first=True,
#                     bidirectional=rnnbidirect)

#     def forward(self,inputs):
#         if isinstance(self.k,int) and self.k == 0:
#             rnn,ho = self.rnn(inputs)
#         else: 
#             cnn = self.cnn(inputs)
#             rnn,ho = self.rnn(cnn[:,0,:,:])
#         for i in range(1,self.ochannels):  ## concatenate输入?
#             rnn,ho = self.rnn(cnn[:,i,:,:],ho)

#         return rnn
class CRNN(nn.Module):
    def __init__(self,rnnisize,rnnhsize,rnnbidirect=False,ichannels=1,ochannels=1,k=0,dilation=1,stride=1,padding=0):
        super(CRNN,self).__init__()
        self.ochannels = ochannels
        self.k = k

        if isinstance(k,int) and k == 0:
            pass
        elif isinstance(k,int):
            self.cnn = nn.Conv1d(ichannels,ochannels,k,stride=stride,padding=padding,dilation=dilation)
        elif len(k) == 2:
            self.cnn = nn.Conv2d(ichannels,ochannels,k,stride=stride,padding=padding,dilation=dilation)
        
        self.rnn = nn.GRU(input_size=rnnisize, hidden_size=rnnhsize, batch_first=True,
                    bidirectional=rnnbidirect)

    def forward(self,inputs):
        if isinstance(self.k,int) and self.k == 0:
            rnn,ho = self.rnn(inputs)
        else: 
            inputs = inputs.unsqueeze(1)          ## [32,L,C*3/2] -> [32,1,L,C*3/2] to cnn2d with 3x3kernel
            cnn = self.cnn(inputs)
            rnn,ho = self.rnn(cnn[:,0,:,:])
        for i in range(1,self.ochannels):  ## concatenate输入?
            rnn,ho = self.rnn(cnn[:,i,:,:],ho)

        return rnn

class LinearLadder(nn.Module):
    def __init__(self,ladderLength,maxisize,stride=2):
        super(LinearLadder,self).__init__()
        self.ladderLength = ladderLength
        self.ladder = []
        for i in range(ladderLength):
            osize = maxisize-stride-i*stride
            assert osize > 0
            linear = nn.Linear(maxisize-i*stride,osize)
            self.ladder.append(linear)

    def forward(self,inputs):
        assert len(inputs) == 3
        outputs = []
        for i in range(self.ladderLength):
            output = self.ladder[i](inputs[:,:,i])
            outputs.append(output)
        
        return outputs


class DLinear():
    """
    Noting !!!!
    Must Not used in init process,cause we will remove and add parameters to optimizer
    use optimizer.add_param_group()"""
    def __init__(self,optimizer):
        # super(DLinear,self).__init__()
        self.inputsize = 1
        self.outputsize = 1
        self.dlinear = nn.Linear(self.inputsize,self.outputsize)
        self.dlinear.cuda()
        self.useoptimizer = True

        if (self.useoptimizer):
            optimizer.add_param_group({'params':self.dlinear.parameters()})

    ## 有个问题: 如果同一次优化中输入两次不同的形状，会不会导致graph丢失? 有待利用梯度结果来验证一下。
    def __call__(self,inputs,optimizer):
        isize = inputs.size()[-1]
        if(self.inputsize < isize):
            if (self.useoptimizer):
                optimizer.param_groups = optimizer.param_groups[:-1] ## 移除最后一个参数组。
            x = [torch.tensor(x.detach()) for x in list(self.dlinear.parameters())]
            zero0 = torch.zeros((x[0].size()[0],isize-self.inputsize))
            zero0 = zero0.cuda()
            ## 补零时注意weight参数与实际是转置的关系。
            self.dlinear = nn.Linear(isize,self.outputsize)
            self.dlinear.weight=nn.Parameter(torch.cat([x[0],zero0],-1))  ## exchange bias and weight to test
            self.dlinear.bias = nn.Parameter(x[1])
            if(self.useoptimizer):
                optimizer.add_param_group({'params': self.dlinear.parameters()})

            self.inputsize = isize
        elif self.inputsize > isize:
            zero0 = torch.zeros(inputs.size()[0],inputs.size()[1],self.inputsize-isize)
            zero0 = zero0.cuda()
            inputs = torch.cat([inputs,zero0],-1)

        outputs = self.dlinear(inputs)
        return outputs

## 调试:
## 尝试把a的影响因子降低，可以通过将其求和作为其中一项损失。
## cat1 = cat1 + inputs
## crnn中所有的channel都输入一个gru 应该不太好，因为随着时间流逝，gru的效果是减弱的。其记忆一定是有限制的。
class MwAN(nn.Module):
    def __init__(self,vocab_size, embedding_size,encoder_size,drop_out):
        super(MwAN, self).__init__()

        def same1d(k):
            assert k%2 == 1
            padding = k/2
            return k,padding

        self.vpatitialcuda = False
        self.vcuda = False
        self.onece = False

        self.drop_out = drop_out
        self.embedding_size = embedding_size
        self.encoder_size = encoder_size
        self.hidden_size = encoder_size
        self.vocab_size = vocab_size
        self.class_size =  3

        self.usecnn = True

        self.embedding = nn.Embedding(vocab_size + 2, embedding_dim=embedding_size)
        
        self.gru0 = nn.GRU(embedding_size,encoder_size/2)

        self.CATLAYER1 =  nn.Linear(encoder_size/2,encoder_size)
        self.crnn0 = CRNN(embedding_size,encoder_size*3/2)
        if self.usecnn:
            self.crnn1 = CRNN(encoder_size*3/2,encoder_size*3/2,rnnbidirect=True,ochannels=3,k=(3,3),padding=(1,1))
        else:
            self.gru1 = nn.GRU(encoder_size*3/2,encoder_size*3/2,bidirectional=True)
        self.full0 = nn.Linear(encoder_size*3,encoder_size*3/2)

    def analyse(self,inputs):
        gru0,_ = self.gru0( inputs )        ## [32,L,C] -> [32,L,C/2]
        
        cat1 = self.CATLAYER1(gru0)         ## [32,L,C/2] -> [32,L,C]
        cat1 = cat1 + inputs
        crnn0 = self.crnn0(cat1)            ## [32,L,C] -> [32,L,C*3/2] 这里本来打算用输入加gru0联合输入的，但是忘记了。
        if self.usecnn:
            crnn1 = self.crnn1(crnn0)           ## [32,L,C*3/2] -> [32,L1=L,C*3]
        else:
            crnn1,_ = self.gru1(crnn0)

        extract = crnn1.size()[1]/3
        if extract < 1:
            extract == 1
        last = crnn1[:,-extract:,:]
        outputs = self.full0(last)
        return outputs

    def setcuda(self):
        self.vcuda = True
        pass

    def forward(self,inputs,optimizer):
        [query, passage, answer,ids,pos, is_train] = inputs

        tail_tensor_q = torch.tensor([[self.vocab_size+1]]*query.shape[0])
        if(self.vcuda==True):
            tail_tensor_q = tail_tensor_q.cuda()
        qp_em = torch.cat([query,tail_tensor_q,passage],-1)

        qp_em = self.embedding(qp_em)
        a_embeddings = self.embedding(answer)

        qp = self.analyse(qp_em).transpose(2,1)            
        a=[]
        for i in range(3):
            x = self.analyse(a_embeddings[:,i,:,:])
            a.append(x.transpose(2,1))    

        if(not self.onece):
            self.onece = True
            self.dlinear = DLinear(optimizer)
        def calda(a,optimizer):
            return [self.dlinear(a[i],optimizer) for i in range(3)]
        if(qp.size()[1] >= a[0].size()[1]):
            c1,c2 = calda(a,optimizer),self.dlinear(qp,optimizer)  
        else:
            c2,c1 = self.dlinear(qp,optimizer),calda(a,optimizer)

        a_cos = []
        for i in range(3):
            c1[i] = c1[i].squeeze(1).squeeze(-1)
            c2 = c2.squeeze(1).squeeze(-1)
            cos = (c1[i]*c2).sum(-1) / ( torch.sqrt((c1[i]**2).sum(-1)) * torch.sqrt((c2**2).sum(-1)) )
            a_cos.append(cos.unsqueeze(-1))

        predict = torch.softmax( torch.cat(a_cos,-1) ,-1)
        if not is_train:
            return predict.argmax(1)

        gather = predict.gather(-1,pos.unsqueeze(-1))
        loss = -torch.log(gather).mean()

        return loss


## CRNN1 vs CRNN
## 对于CRNN来说，有个失误:
#        if(not self.onece):
#            self.dlinear = DLinear(optimizer)
## 这里忘记了添加self.onece = True, 但是使用30000句64维wordvector tranning data 却可以实现90的acc(picture/record/Screenshot from
# 2018-10-15 01-57-31.png)
# 为了找到Dlinear的影响因素，进行如下实验:
# 1. 单独添加self.onece = True。  图片标记: dlinear_onece_true.png 结果: 与原CRNN的评价结果一致.
# 2. 单独移除Dlinear 中对于optimizer的相关的操作,图片标记:dlinear_remove_optimizer。 结果: 与原CRNN的评价结果一致。结论:
# 是否揭示了在网络中适宜的添加不确定矩阵反而能抑制过拟合现象? --> 混乱矩阵学习。
# 经过以上实验可以看出Dlinear没有影响，或者太过微小。以下实验采用不更新optimizer的方式。(上述1,2同时操作)

# 进行如下实验:
# 1. 网络整体全部换成GRU测试.       --> 只把CRNN1换成GRU,其它保持一致 remove_cnn.png (前几个epoch很低，后面提高到98)))
# 2. 打乱dev的答案顺序.  --> 运用1后精度41 --> 36,网络结构还是与顺序挂钩了,需要修改。

# 其它可能因素:
    # CNN的加入（3X3核)
    # CNN三次输入相同GRU
    # 不同模块维度设置方式。
    # qp_em合并的方式
    # 最后phase3的结尾1/3 extract方式。
    # cosine 评价方式。
# 待测试。
# 1. 测试以前的dmnplus使用cosine评价方式效果.
# 2. 其他参数调整。结果: 性能全开，即Dlinear, embedding 2epoch之后关闭，crnn1输入前联合gru0和inputs，依然没有任何改善。
#     