# -*- coding: utf-8 -*-
import torch
from torch import nn
import torch.nn.init as init
from torch.autograd import Variable
from torch.nn import functional as F
from termcolor import colored
import logging
import sys,os
import random

from painternerCollection.RLqtest2 import RLqtest
from painternerCollection.utils import printonece1
logger = logging.getLogger(__name__)
logging.basicConfig(level = logging.DEBUG,
                    format = '%(asctime)s[%(levelname)s] ---- %(message)s',
                    )

str_str_str = """
        0       1   2   3   4   5       --L
  C  -- 1               
        2
       ...
        127

含义: 每一维度 【【独立线性交叉（有数乘）】】 后再 【【线性交叉（没有数乘）】】 输出一个新的。
"""

## what's the meaning of dilation in conv or pool layer
## 层数变多优化比较困难的一个原因可能是因为cnn每层都是互相交叉的，是否可以有种路径让其不再交叉，最后统一交叉比较好?
## 是否需要在先输入p,然后a,q输入后分别backward(分别屏蔽没有经过的层)),最后再输入一遍p，然后再optimizer.step() ??,不过这样做的前提是backward
## 后会累积梯度。 然后还需要认为a,q输入的值与网络参数无关，或者关系很小。

class MwAN(nn.Module):
    def __init__(self,vocab_size, embedding_size,encoder_size,drop_out):
        super(MwAN, self).__init__()

        def same1d(k):
            assert k%2 == 1
            padding = k/2
            return k,padding

        self.vpatitialcuda = False

        self.drop_out = drop_out
        self.embedding_size = embedding_size
        self.encoder_size = encoder_size
        self.hidden_size = encoder_size
        self.vocab_size = vocab_size
        self.class_size =  3
        self.out_channles = [
                128,
                256,
                512,
                256,
                128,
                64
        ]

        self.a = [None,None,None]
        self.q = None

        self.embedding = nn.Embedding(vocab_size + 1, embedding_dim=embedding_size)

        self.k,self.padding = same1d(3)
        assert self.k == 3
        assert self.padding == 1
        ## cnn是这样的: 输入通道，即feature数/每组; 输出通道，即组数。
        self.cnn1_1 = nn.Conv1d(self.embedding_size,self.embedding_size,self.k,padding=self.padding)
        self.cnn1_2 = nn.Conv1d(self.embedding_size,self.embedding_size,self.k,padding=self.padding)
        self.cnn1_3 = nn.Conv1d(self.embedding_size,self.out_channles[0],self.k,padding=self.padding)
        ## notion: pad should be smaller than half of kernel size ( formula include 2*padding )
        self.max1 = nn.MaxPool1d(self.k,stride=2,padding=self.padding)  ## 为什么不explicit声明padding=1，L=1时就会提示错误

        self.cnn2_1 = nn.Conv1d(self.out_channles[0],self.out_channles[0],self.k,padding=self.padding)
        self.cnn2_2 = nn.Conv1d(self.out_channles[0],self.out_channles[0],self.k,padding=self.padding)
        self.cnn2_3 = nn.Conv1d(self.out_channles[0],self.out_channles[1],self.k,padding=self.padding)
        self.max2 = nn.MaxPool1d(self.k,stride=2,padding=self.padding)

        self.cnn3_1 = nn.Conv1d(self.out_channles[1],self.out_channles[1],self.k,padding=self.padding)
        # self.cnn3_2 = nn.Conv1d(self.out_channles[1],self.out_channles[1],self.k,padding=self.padding)
        self.cnn3_3 = nn.Conv1d(self.out_channles[1],self.out_channles[2],self.k,padding=self.padding)
        self.max3 = nn.MaxPool1d(self.k,stride=2,padding=self.padding)

        self.cnn4_1 = nn.Conv1d(self.out_channles[2],self.out_channles[2],self.k,padding=self.padding)
        # self.cnn4_2 = nn.Conv1d(self.out_channles[2],self.out_channles[2],self.k,padding=self.padding)
        self.cnn4_3 = nn.Conv1d(self.out_channles[2],self.out_channles[3],self.k,padding=self.padding)
        self.max4 = nn.MaxPool1d(self.k,stride=2,padding=self.padding)

        self.cnn5_1 = nn.Conv1d(self.out_channles[3],self.out_channles[3],self.k,padding=self.padding)
        # self.cnn5_2 = nn.Conv1d(self.out_channles[3],self.out_channles[3],self.k,padding=self.padding)
        self.cnn5_3 = nn.Conv1d(self.out_channles[3],self.out_channles[4],self.k,padding=self.padding)
        self.max5 = nn.MaxPool1d(self.k,stride=2,padding=self.padding)

        self.cnn6_1 = nn.Conv1d(self.out_channles[4],self.out_channles[4],self.k,padding=self.padding)
        # self.cnn6_2 = nn.Conv1d(self.out_channles[4],self.out_channles[4],self.k,padding=self.padding)
        self.cnn6_3 = nn.Conv1d(self.out_channles[4],self.out_channles[5],self.k,padding=self.padding)
        self.max6 = nn.MaxPool1d(self.k,stride=2,padding=self.padding)

        self.full1 = nn.Linear(self.out_channles[5],self.out_channles[5])
        # self.full2 = nn.Linear(self.out_channles[5],1)

        self.initiation()

    def setcuda(self):
        pass
        
    def initiation(self):
        initrange = 0.1
        nn.init.uniform_(self.embedding.weight, -initrange, initrange)
        for module in self.modules():
            if isinstance(module, nn.Linear) or isinstance(module,nn.Conv1d):
                nn.init.xavier_uniform_(module.weight, 0.1)

    def phase1(self,inputs,oresult,i=None):
        # print inputs.size()
        __1 = self.cnn1_1(inputs)
        __1 = self.cnn1_2(__1)
        __1 = self.cnn1_3(__1)
        # print __1.size()
        __1 = self.max1(__1)            #[N,64,L]
        if oresult == 'a':
            ## 不用detach(),只有在提取weight的时候才需要detach()
            # self.a[i] = torch.tensor(__1.mean(-1).unsqueeze(1))  ## use mean or max ??  ## [N, 64] --> [N,1,64]
            self.a[i] = self.cnn6_3(__1)  ## use mean or max ?? 
            return

        __2 = self.cnn2_1(__1)
        __2 = self.cnn2_2(__2)
        __2 = self.cnn2_3(__2)
        __2 = self.max2(__2)
        if oresult == 'q':
            ## q 提取概率 1
            self.q = torch.softmax( __2.mean(-1),-1 ).unsqueeze(2)       ## [N, 128] --> ##[N,128,1]
            return

        assert oresult == 'p'
        return __2


    def forward(self,inputs):
        [query, passage, answer,ids, is_train] = inputs
        if(self.embedding_size > 1):
            q_embedding = self.embedding(query).transpose(2,1)     ## N, L, C  --> ## N, C, L
            p_embedding = self.embedding(passage).transpose(2,1)
            a_embeddings = self.embedding(answer)                  ## N, L1，L，C  --> ## N,L1_L,C
        else:    
            q_embedding  = query.unsqueeze(2).float()
            p_embedding  = passage.unsqueeze(2).float()
            a_embeddings = answer.unsqueeze(3).float()

        for i in range(3):
            __i = a_embeddings[:,i,:,:].transpose(2,1)      ##[32, 64, L]
            self.phase1(__i,'a',i)
        self.phase1(q_embedding,'q')
        __2 = self.phase1(p_embedding,'p')

        ## joint q and p, 是使用类似concatenate还是把q压缩成1再与p结合呢?
        q_p = self.q * __2          ##   p --> [N,128,Lm]
        __3 = self.cnn3_1(q_p)
        # __3 = self.cnn3_2(__3)
        __3 = self.cnn3_3(__3)
        __3 = self.max3(__3)

        __4 = self.cnn4_1(__3)
        # __4 = self.cnn4_2(__4)
        __4 = self.cnn4_3(__4)
        __4 = self.max4(__4)   ## [N,512,Lm] --> [N,Lm,512]

        ## 反卷积.先简单理解为features数目反向。

        __5 = self.cnn5_1(__4)
        # __5 = self.cnn5_2(__5)
        __5 = self.cnn5_3(__5)
        __5 = self.max5(__5)    ## [N,123,Lm] --> [N,Lm,128]

        __6 = self.cnn6_1(__5)
        # __6 = self.cnn6_2(__6)
        __6 = self.cnn6_3(__6)                 
        __6 = self.max6(__6).transpose(2,1)                 ##[N,LM,8]

        # ## RoI fullconnect    看完Mask R-cnn再考虑一下。
        # __roi = None
                                                    ## 如何联合 解码输出[N,L,512] 和 候选[N,3,X,64]
                                                    ## 1. 自注意力消除L和X
                                                    ## 2. 推导方程 Y=A*B的反向。
        a_cos = []
        for i in range(3):
            a1 = self.full1(__6).bmm(self.a[i])        
            # print type(a1.max(-1))             ## tensor.max() 是个tuple
            a1_r = __6.transpose(2,1).bmm( torch.softmax(a1.mean(-1),-1).unsqueeze(-1) ).squeeze(-1)   ## q_p length  # [N,8]
            a1_l = self.a[i].bmm( torch.softmax(a1.mean(1),-1).unsqueeze(-1) ).squeeze(-1)    ## a length
            a1_cos =   (a1_r * a1_l).sum(-1)  / ( torch.sqrt( (a1_l**2).sum(-1) ) *torch.sqrt( (a1_r**2).sum(-1)) )
            # a1_cos   [32,]
            a1_cos = a1_cos.unsqueeze(-1)
            a_cos.append(a1_cos)


        predict = torch.softmax( torch.cat(a_cos,-1) ,-1)

        if not is_train:
            return predict.argmax(1)

        loss = -torch.log(predict[:,0]).mean()

        return loss


## 相比alphaCnn6.py
## 1.移除self.a=torch.tensor(...)中多余的torch.tensor函数。
## 2.说明same 对于k为奇数并且padding=k/2的等价性。
## 3.