# -*- coding: utf-8 -*-
import argparse
import cPickle
import torch

from model import MwAN
# from painternerCollection.dmn import MwAN
# from painternerCollection.dmnPlus import MwAN
# from painternerCollection.SeparateDmn3 import MwAN

from preprocess import process_data
from utils import *
from termcolor import colored
from painternerCollection.utils import printonece
from ptColl2.swats import Swats

parser = argparse.ArgumentParser(description='PyTorch implementation for Multiway Attention Networks for Modeling '
                                             'Sentence Pairs of the AI-Challenges')

parser.add_argument('--data', type=str, default='data/',
                    help='location directory of the data corpus')
parser.add_argument('--threshold', type=int, default=5,
                    help='threshold count of the word')
parser.add_argument('--epoch', type=int, default=50,
                    help='training epochs')
parser.add_argument('--emsize', type=int, default=128,
                    help='size of word embeddings')
parser.add_argument('--nhid', type=int, default=128,
                    help='hidden size of the model')
parser.add_argument('--batch_size', type=int, default=32, metavar='N',
                    help='batch size')
parser.add_argument('--log_interval', type=int, default=300,
                    help='# of batches to see the training error')
parser.add_argument('--dropout', type=float, default=0.3,
                    help='dropout applied to layers (0 = no dropout)')
parser.add_argument('--cuda', action='store_true',
                    help='use CUDA')
parser.add_argument('--patitialcuda', action='store_true',
                    help='use patitial CUDA')
parser.add_argument('--save', type=str, default='model.pt',
                    help='path to save the final model')
parser.add_argument('--model', type=str, default='model.pt',
                    help='model path')
parser.add_argument('--picklebuild', action='store_true',
                    help='build picklefile')

parser.add_argument('--restore', action='store_true',
                    help='build picklefile')
args = parser.parse_args()

if args.picklebuild:
    process_data(args.data, args.threshold)

with open("data/word2id.obj",'rb') as f:
    word2id_data = cPickle.load(f)
# vacab_size = 98745
vocab_size = len(word2id_data)
print("vacab_size",vocab_size)

if args.restore:    
    with open(args.model, 'rb') as f:
        print colored("restoring from model.pt","blue")
        model = torch.load(f)
else:              
    model = MwAN(vocab_size=vocab_size, embedding_size=args.emsize, encoder_size=args.nhid, drop_out=args.dropout)
    print('Model total parameters:', get_model_parameters(model))

if args.cuda:
    model.cuda()
    model.setcuda()
elif args.patitialcuda:
    model.setcuda(patitial=True)
optimizer = torch.optim.Adamax(model.parameters())
# optimizer = Swats(model.parameters())
# optimizer = torch.optim.SGD(model.parameters(),lr=0.001,momentum=0.9)
# torch.optim.lr_scheduler.MultiStepLR(optimizer,[1,2,3,4],gamma=0.2)
# scheduler = torch.optim.lr_scheduler.LambdaLR(optimizer,lr_lambda=lambda epoch: 0.2**epoch)
scheduler = torch.optim.lr_scheduler.LambdaLR(optimizer,lr_lambda=lambda epoch: 1)

with open(args.data + 'train.pickle', 'rb') as f:
    train_data = cPickle.load(f)

with open(args.data + 'dev.pickle', 'rb') as f:
    dev_data = cPickle.load(f)
dev_data = sorted(dev_data, key=lambda x: len(x[1]))

print('train data size {:d}, dev data size {:d}'.format(len(train_data), len(dev_data)))


def train(epoch):
    scheduler.step()
    print(optimizer.state_dict()['param_groups'][0]['lr'])  ## 查看一下学习率。
    model.train()
    data = shuffle_data(train_data, 1)
    total_loss = 0.0    
    save_one_flag = False
    for num, i in enumerate(range(0, len(data), args.batch_size)):
        one = data[i:i + args.batch_size]
        # printonece("train_one","red",one[0],Exit=True)
        # output=[]
        # for mm in one[0]:
        #     for mmm in mm:
                
        query, _ = padding([x[0] for x in one], max_len=50)
        passage, _ = padding([x[1] for x in one], max_len=350)
        answer = pad_answer([x[2] for x in one])
        ids = [x[3] for x in one]
 
        query, passage, answer,ids = (torch.LongTensor(query), torch.LongTensor(passage), 
                                                torch.LongTensor(answer),torch.LongTensor(ids))
        if args.cuda:
            query = query.cuda()
            passage = passage.cuda()
            answer = answer.cuda()

        optimizer.zero_grad()  #Clears the gradients of all optimized torch.Tensor s.
        loss = model([query, passage, answer,ids, True])
        loss.backward()        # 告诉优化器需要更新的方式？因为初始化optimizer把model所有parameters都加入，但不一定都想更新。s
        total_loss += loss.item()
        optimizer.step()       # parameter update
        if (num + 1) % args.log_interval == 0:
            print '|------epoch {:d} train error is {:f}  eclipse {:.2f}%------|'.format(epoch,
                                                                                         total_loss / args.log_interval,
                                                                                         i * 100.0 / len(data))
            total_loss = 0
            
        if (i*100.0/len(data) >= 50):
            if(not save_one_flag):
                save_one_flag=True
                with open('half'+args.save, 'wb') as f:
                    print colored('save half model',"green")
                    torch.save(model, f)


def test():
    model.eval()
    r, a = 0.0, 0.0
    with torch.no_grad():
        for i in range(0, len(dev_data), args.batch_size):
            one = dev_data[i:i + args.batch_size]
            query, _ = padding([x[0] for x in one], max_len=50)
            passage, _ = padding([x[1] for x in one], max_len=500)
            answer = pad_answer([x[2] for x in one])
            ids = [x[3] for x in one]
            query, passage, answer,ids = (torch.LongTensor(query), torch.LongTensor(passage), 
                                        torch.LongTensor(answer),torch.LongTensor(ids))
            if args.cuda:
                query = query.cuda()
                passage = passage.cuda()
                answer = answer.cuda()
            output = model([query, passage, answer,ids, False])
            r += torch.eq(output, 0).sum().item()
            a += len(one)
    return r * 100.0 / a


def main():
    best = 0.0
    for epoch in range(args.epoch):
        train(epoch)
        acc = test()
        if acc > best:
            best = acc
            with open(args.save, 'wb') as f:
                torch.save(model, f)
        print 'epcoh {:d} dev acc is {:f}, best dev acc {:f}'.format(epoch, acc, best)


if __name__ == '__main__':
    main()
