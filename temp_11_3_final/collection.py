#!-*-coding:utf-8-*-
import torch
import torch.nn as nn
from painternerlib.HyparaDebug import fgetHypara

# def cossimilary():
#     a_cos = []
#     for i in range(3):
#         a1 = self.full1(__6).bmm(self.a[i])        
#         # print type(a1.max(-1))             ## tensor.max() 是个tuple
#         a1_r = __6.transpose(2,1).bmm( torch.softmax(a1.mean(-1),-1).unsqueeze(-1) ).squeeze(-1)   ## q_p length  # [N,8]
#         a1_l = self.a[i].bmm( torch.softmax(a1.mean(1),-1).unsqueeze(-1) ).squeeze(-1)    ## a length
#         a1_cos =   (a1_r * a1_l).sum(-1)  / ( torch.sqrt( (a1_l**2).sum(-1) ) *torch.sqrt( (a1_r**2).sum(-1)) )
#         # a1_cos   [32,]
#         a1_cos = a1_cos.unsqueeze(-1)
#         a_cos.append(a1_cos)

class QPMultiWay(nn.Module):
    def __init__(self,indim):
        super(QPMultiWay,self).__init__()
        self.Hypara = fgetHypara()
        self.indim = indim

        self.Wphase1 = nn.ModuleDict({
            'mi':  nn.ModuleList(nn.Linear(self.indim,self.indim/2)   for _ in range(1)),
            'bi':  nn.ModuleList(nn.Linear(self.indim,self.indim/2)   for _ in range(1)),
            'dot': nn.ModuleList(nn.Linear(self.indim,self.indim)     for _ in range(1)),
            'cat': nn.ModuleList(nn.Linear(self.indim,self.indim/2)   for _ in range(2)),
        })
        self.Vphase1 = nn.ModuleDict({
            'mi':  nn.ModuleList([nn.Linear(self.indim/2,1)  for _ in range(1)]),
            'dot': nn.ModuleList([nn.Linear(self.indim/2,1)  for _ in range(1)]),
            'cat': nn.ModuleList([nn.Linear(self.indim/2,1)  for _ in range(1)])
        })

        # self.Wagger = nn.Linear(self.indim,self.indim/2)
        # self.Vshare = nn.Linear(self.indim/2,1)
        self.mergeGRU = nn.GRU(input_size=indim*4, hidden_size=indim, batch_first=True,
                                bidirectional=False)
    def fempty(self,X):
        return X
    
    def fMultiWay(self,inputs):
        """
         bi 也要进行torch.tanh激活?
        """
        q,p= inputs
        uq = q.unsqueeze(1)
        up = p.unsqueeze(2)
        def mode(a,v,b,bi=False):
            printf (a.size(),b.size())
            if not bi:
                a = v( torch.tanh( a  )  ).squeeze(-1) 
            else:
                a = v( a  )     
            n = torch.softmax(a,-1).bmm(b)
            return n 

        mi =    mode(self.Wphase1['mi'] [0](uq-up),         self.Vphase1['mi'][0],     q)
        dot =   mode(self.Wphase1['dot'][0](uq*up),         self.Vphase1['dot'][0],     q)
        bi =    mode(self.Wphase1['bi'](p).bmm(q.transpose(2,1)), self.fempty,   q, True)    # (32, 350, 256) (32, 12, 256) (128, 256)
        cat =   mode(self.Wphase1['cat'][0](uq)+self.Wphase1['cat'][1](up), self.Vphase1['cat'][0],   q)
                                                            ## 这里应该直接相加不应该用Linear（取决于是否认为序列已经被提取特征独立可以用线性提取了)

        if self.Hypara['dropout_mi']:
            mi = self.dropoutlayer[2](mi)
        if self.Hypara['dropout_dot']:
            dot = self.dropoutlayer[3](dot)
        if self.Hypara['dropout_bi']:
            bi = self.dropoutlayer[4](bi)
        if self.Hypara['dropout_cat']:
            cat = self.dropoutlayer[5](cat)
        return mi,dot,bi,cat

    # def phase2(self,inputs,inputs2):
    #     mi,dot,bi,cat = inputs
    #     p = inputs2
    #     def mode(a,b,Wg,gru):
    #         printf ( a.size(),b.size() )
    #         cat = torch.cat([a, b],-1)      ## [32,200,128]
    #         g = torch.sigmoid( Wg(cat) )       ## Wg是共享的吗?
    #         printf (g.size(), cat.size())
    #         r,_ = gru( g*cat )
    #         return r
    #     if self.Hypara['phase2_fake_pass']:
    #         gmi,gdot,gbi,gcat = (self.Wg(torch.cat([mi, p],-1)),self.Wg(torch.cat([dot, p],-1)),\
    #                         self.Wg(torch.cat([bi, p],-1)),self.Wg(torch.cat([cat, p],-1)))
    #     else:
    #         gmi = mode(mi,p,  self.Wg,  self.Ggru)        ## Wg是共享的吗? ## Ggru是共享的吗?
    #         gdot= mode(dot,p, self.Wg,  self.Ggru) 
    #         gbi = mode(bi,p,  self.Wg,  self.Ggru)
    #         # print self.shareGgru_order,self.VWshare_all_order
    #         gcat = mode(cat,p,self.Wg,  self.Ggru) 
    #         # print self.shareGgru_order,self.VWshare_all_order
    #     return gmi,gdot,gbi,gcat

    # def fmerge(self,inputs):
    #     """
    #      这里决定share Vshare  Wagger，因为这两个的目的是为了提炼出概率，那么应当用一套feature来提炼出概率。
    #     """
    #     gmi,gdot,gbi,gcat = inputs  #[B,L,M]
    #     def mode(a,V,W):
    #         l = V( torch.tanh(W(a))).transpose(2,1)    #[B,L,M] to [B,1,L] 
    #         return l
    #     aggermi = mode(gmi,     self.Vshare,self.Wagger)  ## 如果Vshare,Wagger是share
    #     aggerdot = mode(gdot,   self.Vshare,self.Wagger)
    #     aggerbi = mode(gbi,     self.Vshare,self.Wagger)
    #     aggercat = mode(gcat,   self.Vshare,self.Wagger)
    #     # print self.shareGgru_order,self.VWshare_all_order
    #     # print self.shareGgru_order,self.VWshare_all_order
    #     # exit()

    #     agger = torch.cat([aggermi,aggerdot,aggerbi,aggercat],1) ## [B,1,L] to [B,4,L]
    #     agger_normal = torch.softmax(agger,1).unsqueeze(-1)  ## [B,4,L] to [B,4,L,1]
    #     gmi,gdot,gbi,gcat = (gmi.unsqueeze(1),gdot.unsqueeze(1),
    #                             gbi.unsqueeze(1),gcat.unsqueeze(1)) ## [B,L,M] to [B,1,L,M]
    #     agger = torch.cat([gmi,gdot,gbi,gcat],1) ## [B,1,L,M] to [B,4,L,M]
    #     agger = agger_normal * agger
    #     agger = agger.mean(1)    ## [B,4,L,M] to [B,L,M]
    #     return agger

    def fmergegru(self,X):
        z = torch.cat(X,-1)
        z = self.mergeGRU(X)
        return z

    def forward(self,q,p):
        z = self.fMultiWay( (q,p) )  ##[B,L,M]   --> [B,L,M]
        z = self.fmergegru(z)        ##[B,L,M*5] --> [B,L,M]
        return z


"""
线性意味着输入的维度都是独立的即 a1X1+a2X2+b (X1与X2的乘积因子独立) ,但是 X1*X2就不是线性了的.
这里在思考线性问题纠结住了，无法确定到底是否使用phase2,phase3
另外双向问题到底该怎么融合始终是个问题，所以也不使用双向的。
这里直接再利用一个GRU来，暂时不用fmerge 
"""