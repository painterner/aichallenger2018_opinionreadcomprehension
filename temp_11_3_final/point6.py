# -*- coding: utf-8 -*-
import torch
from torch import nn
from torch.nn import functional as F
from termcolor import colored
import logging
from painternerCollection.utils import printonece
from painternerlib.HyparaDebug import fputHypara
from painternerCollection.utils4net import preEmbedding
from painternerlib.torch.function import l2_sum
from ptmodel.masknet import MaskInplace
from ptmodel.TwoGruNewEncoder import TwoGru,NewEncoder
from ptmodel.AllShare3 import GRU,Linear, MainW
import time
logger = logging.getLogger(__name__)
logging.basicConfig(level = logging.DEBUG,
                    format = '%(asctime)s[%(levelname)s] ---- %(message)s',
                    )

class Print():
    def __init__(self):
        self.action = True
    def cancel(self):
        self.action = False
    def open(self):
        self.action = True
    def __call__(self,*inputs):
        if not self.action:
            return
        print inputs

printf = Print()
printf.cancel()

## 共享权重分析:
## initfact = 1 -> 0.1
## va vq 用parameter产生的感觉不可靠，先用uq,q代替。
## bias = False ? bi output-> 2*encoder_size ?
## 新建立一个模块的Wrap，每次输入先检测输入大小与预期的是否一致，后续用脚本把外层剥去。 ??
## Wg 扩展到4倍？
## vq --> 不是用bmm,用multiply
## 调试参： a 2*encode_size -> encode_size / 2
## scratch 计算GPU消耗情况。
## 持续学习，每次添加新的都可以持续进行学习。
## 三项测试(百度云一同测试)  
# 1. initfact 0.1 2. 移除phase2 ,3  <-       
#  3,移除phase2 4. 评价改为余弦 5.用单个词来学习。
## 6. 动态添加训练数据.
## 7. 最后一步时已经脱离了注意力而得到一个总结点，所以不能再用question或者answer的任何一个长度，已经与此无关了?
## 或者这只是抽象的长度，所以是合理的。

## paper
## drop_out 机制.参数设置成为0？合适吗?
## batch_normal用于NLP的理论依据存在?
## GRU机制
class MwAN(nn.Module):
    def __init__(self, vocab_size, embedding_size, encoder_size, drop_out=0.5):
        super(MwAN,self).__init__()

        self.embedding_size = embedding_size
        self.encoder_size = encoder_size
        self.hidden_size = encoder_size
        self.vocab_size = vocab_size
        self.class_size =  3
        self.drop_out=drop_out
        self.Hypara = fputHypara({
            "Vr": True,  ## 不能设为false, answer gru 变为 1/2
            "q_only_last": True,
            "Vq_z":True,        ## 与 VWshare_all 的self.Vshare函数冲突。

            "pretrained_embedding":True,
            "pretrained_embedding_fix": True,
            "pretrainedpath": 'data/pretrained_word2id_tencent.obj',

            'analy_bert':False,
            'l2_gru_ih':False,
            'l2_loss_gamma': 0.01,
            
            "dropout_q_encoder":        False,
            "dropout_p_encoder":        False,
            "dropout_mi":               False,
            "dropout_dot":              False,
            "dropout_cat":              False,
            "dropout_bi":               False,
            "dropout_q_W_selfattn":     False,
            "dropout_agger":            False,
            "dropout_agger_W_selfattn": False,
            "dropout_input": False,
            "maskinplace":False,
            "maskinplace_prob": 0.0001,
            "maskinplace_zero": False,

            'MainWa': True,
            'MainWq': True,
            'MainWp': True,
            "AGGER": 'GRUWithShare', ## optional: 'NewEncoder','GRUWithShare','nn.GRU','TwoGru'

            'selfpoolsize':10,
            'selfpool_beta': 100.
        })

        print "Hypara dict",self.Hypara

        self.MainW = MainW(self.embedding_size*self.embedding_size*8) 
        """
            先测试2倍的情况。然后是4，8倍。 然后把MainRnn(agger)也改为这样.
        """

        self.embedding = None

        if self.Hypara['MainWq']:
            self.q_encoder = GRU(embedding_size,encoder_size,self.MainW.transfactor,
                                    bidirectional=False,mode='GRU')
        else:
            self.q_encoder = nn.GRU(input_size=embedding_size, hidden_size=encoder_size, batch_first=True,
                                bidirectional=True)
        if self.Hypara['MainWp']:
            self.p_encoder = GRU(embedding_size,encoder_size,self.MainW.transfactor,
                                bidirectional=False,mode='GRU')
        else:
            self.p_encoder = nn.GRU(input_size=embedding_size, hidden_size=encoder_size, batch_first=True,
                                    bidirectional=True)
        if self.Hypara['MainWa']:
            self.a_encoder = GRU(embedding_size,embedding_size,self.MainW.transfactor,
                                bidirectional=False,mode='GRU')
        else:
            self.a_encoder = nn.GRU(input_size=embedding_size, hidden_size=embedding_size / 2, batch_first=True,
                                bidirectional=True)

        if self.Hypara['AGGER'] == 'NewEncoder':
            self.MainRnn = NewEncoder(self.encoder_size,self.encoder_size)
        elif self.Hypara['AGGER'] == 'GRUWithShare':
            self.MainRnn = GRU(2*self.encoder_size,self.encoder_size,self.MainW.transfactor,
                                    bidirectional=True,mode='GRU')
        elif self.Hypara['AGGER'] == 'nn.GRU':
            self.MainRnn = nn.GRU(input_size=2*self.encoder_size, hidden_size=self.encoder_size, batch_first=True,
                                bidirectional=True)
        elif self.Hypara['AGGER'] == 'TwoGru':
            self.MainRnn = TwoGru(embedding_size,encoder_size,self.Hypara['twogrushare'])
        else:
            raise Exception('must be NewEncoder,GRUWithShare,nn.GRU or TwoGru')
                    
        self.Wq     =   Linear(2*self.encoder_size,self.encoder_size)


        self.Wqp    =  nn.ModuleList([ Linear(2*self.encoder_size,self.encoder_size) for _ in range(self.Hypara['selfpoolsize'])])
        self.Vz     =  nn.ModuleList([ Linear(self.encoder_size,1) for _ in range(self.Hypara['selfpoolsize'])])
        self.Wselfpool = Linear(self.Hypara['selfpoolsize']*self.encoder_size*2,self.encoder_size)

        # self.Vr     =   Linear(2*self.encoder_size,self.encoder_size)
        self.a_attention     =   Linear(self.embedding_size,1)

        if self.Hypara['Vq_z']:
            self.Vq = Linear(self.encoder_size,1)

        self.dropoutlayer = nn.ModuleList(
            [nn.Dropout(p=self.drop_out) for _ in range((20))] ## 重复使用一个应该也没事吧 ？
        )
        if self.Hypara['maskinplace']:
            self.maskinplace = MaskInplace(self.Hypara['maskinplace_prob'],self.Hypara['maskinplace_zero'])

        self.Wtest = nn.ModuleDict({
            "nousephase1": Linear(4*self.encoder_size,2*self.encoder_size)
        })

        self.initiation()
        self.init_embedding()
        self.setMainWflag = False

    def setMainW(self):
        """
         要在forward中一次性使用。
        """
        count = 0
        for module in self.modules():
            if isinstance(module,Linear) or isinstance(module,GRU):
                module.setMainW(self.MainW)
                count += 1
            # else:
            #     print module
            #     raise Exception('test error')
        print "modified module count", count
        

    def init_embedding(self):
        if self.embedding == None:
            if not self.Hypara['pretrained_embedding']:
                self.embedding = nn.Embedding(self.vocab_size + 2,embedding_dim=self.embedding_size)
                initrange = 0.1
                nn.init.uniform_(self.embedding.weight, -initrange, initrange)
            else:
                dy_flag = not self.Hypara['pretrained_embedding_fix']
                self.embedding = preEmbedding(self.vocab_size + 1, self.embedding_size, \
                                   dy_flag,self.Hypara['pretrainedpath'])
    def initiation(self):
        for module in self.modules():
            if isinstance(module, nn.Linear):
                nn.init.xavier_uniform_(module.weight, 0.1)


    def setmaskinplaceflag(self,continue_update_times):
        assert isinstance( continue_update_times,int)
        self.continue_update_times = continue_update_times
    def checkmaskinplaceflag(self):
        if hasattr(self,'continue_update_times'):
            if self.continue_update_times == 0:
                return True
        return False
    def setcuda(self,patitial=False):  ## or use inherited function cuda
        self.vcuda = True
    def checkcuda(self):
        if not hasattr(self,'vcuda'):
            self.vcuda = False
        return self.vcuda


    def forward(self,inputs,optimizer):
        """
            encoder  multiway  q_selfattn  agger  agger_selfattn 
        """
        if not self.setMainWflag:
            self.setMainW()
            self.setMainWflag = True
        # E = 0
        # for p in self.MainW.parameters():
        #     E +=p.sum()
        # print "M",E
        # E = 0
        # for p in self.p_encoder.parameters():
        #     E += p.sum()
        # print "S",E
        if self.Hypara['maskinplace']:
            if self.checkmaskinplaceflag():
                # print "mask inplace start"
                self.maskinplace(self)
        [query, passage, answer,ids,pos,is_train] = inputs
        label = pos
        if self.checkcuda():
            query= query.cuda(); passage= passage.cuda();answer = answer.cuda();label = label.cuda();ids = ids.cuda()
        if self.Hypara['analy_bert']:
            pass
        if self.Hypara['dropout_input']:
            query = self.dropoutlayer[-1](query);passage = self.dropoutlayer[-2](passage)
            answer=self.dropoutlayer[-3](answer)
        q_embedding = self.embedding(query)
        p_embedding = self.embedding(passage)
        a_embeddings = self.embedding(answer)
        
        timeA = time.time()

        q,_ = self.q_encoder(q_embedding)  ## [32,10,128]

        p,_ = self.p_encoder(p_embedding)  ## [32,100,128]

        a_embedding, _ = self.a_encoder(a_embeddings.view(-1, a_embeddings.size(2), a_embeddings.size(3)))
        # q = torch.cat([q[:,:,:self.encoder_size],torch.flip(q[:,:,self.encoder_size:],[1])],-1)
        # p = torch.cat([p[:,:,:self.encoder_size],torch.flip(p[:,:,self.encoder_size:],[1])],-1)
        q = torch.cat([q[:,:,:],q[:,:,:]],-1)
        p = torch.cat([p[:,:,:],p[:,:,:]],-1)
        # a_embedding = torch.cat([a_embedding[:,:,:],a_embedding[:,:,:]],-1)
        timeB = time.time() - timeA
        # print("MainWpqforward",timeB)

        if self.Hypara['dropout_q_encoder']:
            q=self.dropoutlayer[0](q)
        if self.Hypara['dropout_p_encoder']:
            p=self.dropoutlayer[1](p)

        Vq_z = self.Vq
        q_self_W = self.Wq(q)
        if self.Hypara['dropout_q_W_selfattn']:
            q_self_W = self.dropoutlayer[6](q_self_W)
        f = Vq_z(torch.tanh(q_self_W)).transpose(2,1) # [B,Q,1]
        printf (f.size(),q.size())

        assert self.Hypara['q_only_last']
        rq = torch.softmax(f,-1).bmm(q)  # [B,1,M]
        q = rq
        
        assert self.Hypara['q_only_last']
        agger = self.Wtest['nousephase1'] ( torch.cat( [p,q.expand_as(p)],-1) )  

        agger,_ = self.MainRnn(agger)
            
        if self.Hypara['dropout_agger']:
            agger = self.dropoutlayer[7](agger)

        def atten(agger,rq,W,V):
            ## Prediction Layer
            printf (agger.size(), rq.size())
            assert self.Hypara['q_only_last']
            z = W(agger)
            if self.Hypara['dropout_agger_W_selfattn']:
                z = self.dropoutlayer[8](z)
            Vq_z = V
            f = Vq_z(torch.tanh(z)).transpose(2,1)
            f = torch.softmax(f,-1) # [B,1,L]
            r = f.bmm(agger)  # [B,1,M]
            return r,f.squeeze(1)

        attenCollection = []
        resultCollection = []
        for i in range(self.Hypara['selfpoolsize']):   ## 这样真的可以使交叉熵损失最大吗    >> ??
            one_result,one_atten = atten(agger,rq,self.Wqp[i],self.Vz[i])
            one_result = self.dropoutlayer[0](one_result)
            assert one_atten.dim() == 2 and one_result.dim() == 3
            attenCollection.append(one_atten)   ## content: [B,L]
            resultCollection.append(one_result) ## content: [B,M]
        print attenCollection
        crossLossall = 0

        ############MSE############
        assert len(attenCollection) == self.Hypara['selfpoolsize']
        for i in range(len(attenCollection)):
            negloss = -(attenCollection[i]- attenCollection[i].mean(-1,keepdim=True))**2
            # print "negloss",negloss
            crossLossall += negloss.mean(-1) # content: [B,]
        # print "allloss", crossLossall
        # exit()
        if self.Hypara['selfpoolsize'] == 1:
            assert crossLossall == 0.
        else:
            crossLossall = crossLossall.mean() # []  
        print "lastloss", crossLossall      

        ############cross_entropy############
        # assert len(attenCollection) == self.Hypara['selfpoolsize']
        # for i in range(len(attenCollection)):
        #     onece_cross = 0
        #     for ii in range(i+1,len(attenCollection)):
        #         thisloss = ((attenCollection[i]+1e-9)*torch.log(attenCollection[ii]+1e-9)).sum(-1,keepdim=True)
        #         # print thisloss
        #         ##正损失，加大差异。每个都不同，就是每个都相同? 所以还是利用dropoutlayer随机
        #         onece_cross += thisloss  # [B,1]  
        #     if i != len(attenCollection) - 1:  ##last is int  that has no mean method
        #         crossLossall += onece_cross.mean(-1) # content: [B,]
        # # exit()
        # if self.Hypara['selfpoolsize'] == 1:
        #     assert crossLossall == 0.
        # else:
        #     crossLossall = crossLossall.mean() # []

        r =  self.Wselfpool(torch.cat(resultCollection,-1))  ## [B,M*selfpoolsize]
        a_score = F.softmax(self.a_attention(a_embedding), 1)
        a_output = a_score.transpose(2, 1).bmm(a_embedding).squeeze()
        a_embedding = a_output.view(a_embeddings.size(0), 3, -1)

        printf (a_embedding.size(), r.size())

        predict = a_embedding.bmm( F.leaky_relu(r.transpose(2,1),-1) ).squeeze(-1)  ## model.py用的是leaky_relu
        predict = torch.softmax(predict,-1)                                         

        if not self.Hypara['analy_bert']:
            if is_train == 'train' or is_train == 'eval':
                gather = predict.gather(-1,label.unsqueeze(-1))
                loss = -torch.log(gather).mean()  ## 只有一个标签，所以直接mean就行。
            if is_train == 'eval' or is_train == 'infer':
                return predict.argmax(1)
        else:
            raise Exception('error')

        loss1 = crossLossall * self.Hypara['selfpool_beta']
        # print "loss1",loss1
        loss =  loss + loss1
        return loss


"""
11.10:
"AGGER": 'NewEncoder' 'Mainq'
第一次MainW测试(错误的使用了 q_encoder) point5.py 30000 "GRU"
GPU; >1800MiB
|------epoch 17 train error is 0.625185  eclipse 95.97%------| iter: 67349
epcoh 17 dev acc is 64.466667, best dev acc 64.816667

"AGGER": 'nn.GRU' 'Mainq' 'Mainp' point5.py 3000 "GRU"
GPU; 1096MiB 
|------epoch 52 train error is 0.349128  eclipse 95.95%------| iter: 99299
epcoh 52 dev acc is 49.953333, best dev acc 57.416667

"AGGER": 'nn.GRU' 'Mainq' 'Mainp' point5.py 30000 "Simple" 采用Linear测试testW
|------epoch 2 train error is 0.869634  eclipse 95.95%------| iter: 5549
epcoh 2 dev acc is 60.263333, best dev acc 60.263333

"AGGER": 'nn.GRU' 'Mainq' 'Mainp' point5.py 30000 "Simple" 采用Parameter without bias uniform测试testW
|------epoch 4 train error is 0.860992  eclipse 95.95%------| iter: 9299
epcoh 4 dev acc is 57.276667, best dev acc 57.276667
（多次测试，包括randn*0.1(不使用0.1 loss一直是nan) 初始化仍然一样的结果)

"AGGER": 'nn.GRU' 'Mainq' 'Mainp' point5.py 30000 "Simple" 采用Parameter with bias uniform测试testW
|------epoch 2 train error is 0.833446  eclipse 95.95%------| iter: 5549
epcoh 2 dev acc is 60.336667, best dev acc 60.336667
(综合上上面的例子，推测，如果bias randn*0.1应该就好了。  double randn 是nan --> weight randn*0.1 acc不提高 -->
weight uniform_(p,0.1), bias unifrom(-std,std) acc > 65. 所以如果初始值改为0.01呢?)

"AGGER": 'nn.GRU' 'Mainq' 'Mainp' point5.py 30000 "Simple" 
weight uniform_(p,0.01) bias uniform(-std*0.1,std*0.1)
|------epoch 11 train error is 0.689141  eclipse 95.95%------| iter: 22424
epcoh 11 dev acc is 66.166667, best dev acc 66.166667

注意后几个测试都是(        q = torch.cat([q[:,:,:],q[:,:,:]],-1)
                        p = torch.cat([p[:,:,:],p[:,:,:]],-1))

"AGGER": 'nn.GRU' 'Mainq' 'Mainp' point5.py 30000 "Simple"
Wh[0](MainW,inp+hi,reuseflag) + self.mb = (1-self.beta)*self.outb[1] + self.beta*self.mb 
卡住，可能是因为 self.mb会被自动回收，导致内存地址或数据错误。需要想法保存self.mb的值。
修改为如下组合则可以运行
mb = (1-self.beta)*self.outb[1] + self.beta*self.mb
self.mb = (1-self.beta)*self.beta*copy.deepcopy(self.outb[1]).detach() + self.mb
|------epoch 3 train error is 0.882015  eclipse 95.95%------| iter: 7424
epcoh 3 dev acc is 56.970000, best dev acc 57.350000

测试输入
O = X.matmul(self.W) + self.outb[1]*0.1 (也有可能记错了是self.mb*0.1)
|------epoch 7 train error is 0.750376  eclipse 95.95%------| iter: 14924
epcoh 7 dev acc is 65.250000, best dev acc 65.306667

ALLShare2.py
    M x N --> 1 x N --> I x N = I x O
    保持 8-15epoch acc 65% ,就算不能提高acc，这种方法还是能保持acc的不会降低。
    |------epoch 15 train error is 0.465661  eclipse 95.97%------| iter: 59849
    epcoh 15 dev acc is 65.243333, best dev acc 66.240000
    |------epoch 17 train error is 0.427430  eclipse 95.97%------| iter: 67349
    epcoh 17 dev acc is 63.936667, best dev acc 66.240000
这几个测试都具有bidirectional=False

'MainWa': True, 'MainWq': True,'MainWp': True,"AGGER": 'GRUWithShare' 
由于a_encoder 不能outdim降维，所以采用单向，outdim不1/2
    1.
    |------epoch 8 train error is 0.905536  eclipse 95.97%------| iter: 33599
    epcoh 8 dev acc is 57.430000, best dev acc 57.430000
    发现忘记torch.sigmoid(self.model.matmul(submodel.inW))了，所以需要重新测试。
    2.
    |------epoch 20 train error is 0.666005  eclipse 63.79%------| iter: 9674
    epcoh 20 dev acc is 65.626667, best dev acc 65.626667
    |------epoch 68 train error is 0.034041  eclipse 63.79%------| iter: 32174
    epcoh 68 dev acc is 60.310000, best dev acc 65.626667
"""