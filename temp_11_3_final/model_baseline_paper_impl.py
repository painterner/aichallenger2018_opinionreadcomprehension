# -*- coding: utf-8 -*-
import torch
from torch import nn
from torch.nn import functional as F
from termcolor import colored
import logging
from painternerCollection.utils import printonece
from painternerlib.HyparaDebug import fputHypara
from painternerCollection.utils4net import preEmbedding
from painternerlib.torch.function import l2_sum
logger = logging.getLogger(__name__)
logging.basicConfig(level = logging.DEBUG,
                    format = '%(asctime)s[%(levelname)s] ---- %(message)s',
                    )

class Print():
    def __init__(self):
        self.action = True
    def cancel(self):
        self.action = False
    def open(self):
        self.action = True
    def __call__(self,*inputs):
        if not self.action:
            return
        print inputs

printf = Print()
printf.cancel()

## 共享权重分析:
## initfact = 1 -> 0.1
## va vq 用parameter产生的感觉不可靠，先用uq,q代替。
## bias = False ? bi output-> 2*encoder_size ?
## 新建立一个模块的Wrap，每次输入先检测输入大小与预期的是否一致，后续用脚本把外层剥去。 ??
## Wg 扩展到4倍？
## vq --> 不是用bmm,用multiply
## 调试参： a 2*encode_size -> encode_size / 2
## scratch 计算GPU消耗情况。
## 持续学习，每次添加新的都可以持续进行学习。
## 三项测试(百度云一同测试)  
# 1. initfact 0.1 2. 移除phase2 ,3  <-
# 2.1 修改self.Wq(q)+self.Wq2(vq) ,之前改为unsqueeze(),让每个与另一个相联系
# self.Wqp(agger)+self.Wqp2(rq) 移除直接用之前的结果来看能否选择出来。        
#  3,移除phase2 4. 评价改为余弦 5.用单个词来学习。
## 6. 动态添加训练数据.
## 7. 最后一步时已经脱离了注意力而得到一个总结点，所以不能再用question或者answer的任何一个长度，已经与此无关了?
## 或者这只是抽象的长度，所以是合理的。

## paper
## drop_out 机制.参数设置成为0？合适吗?
## batch_normal用于NLP的理论依据存在?
## GRU机制
class MwAN(nn.Module):
    def __init__(self, vocab_size, embedding_size, encoder_size, drop_out=0.2):
        super(MwAN,self).__init__()

        self.drop_out = drop_out
        self.embedding_size = embedding_size
        self.encoder_size = encoder_size
        self.hidden_size = encoder_size
        self.vocab_size = vocab_size
        self.class_size =  3
        self.drop_out=drop_out
        self.Hypara = fputHypara({
            "Vr": True,  ## 不能设为false, answer gru 变为 1/2
            "q_only_last": True,
            "Vq_z":True,        ## 与 VWshare_all 的self.Vshare函数冲突。
            "WgShare": False,
            "shareGgru": False,
            "usephase2": True,
            "phase2_fake_pass": False,
            'mibidot2add':False,
            "WaggerShare":False,
            "Vshare_all": False,   ## 这样做绝对错了。

            "pretrained_embedding":True,
            "pretrained_embedding_fix": True,
            "pretrainedpath": 'data/pretrained_word2id_tencent.obj',

            'analy_bert':False,
            'l2_gru_ih':False,
            'l2_loss_gamma': 0.01
        })

        print "Hypara dict",self.Hypara
        if self.Hypara['phase2_fake_pass']:
            assert self.Hypara['phase2_fake_pass']

        self.embedding = None

        self.q_encoder = nn.GRU(input_size=embedding_size, hidden_size=encoder_size, batch_first=True,
                                bidirectional=True)
        self.p_encoder = nn.GRU(input_size=embedding_size, hidden_size=encoder_size, batch_first=True,
                                bidirectional=True)
        self.a_encoder = nn.GRU(input_size=embedding_size, hidden_size=embedding_size / 2, batch_first=True,
                                bidirectional=True)

        ## Method 2  建立简单，超参数麻烦
        # self.Wphase1  = nn.ModuleList(nn.Linear(2*self.encoder_size,self.encoder_size) for i in range(5))
        # self.Vphase1  = nn.ModuleList(nn.Linear(self.encoder_size,1) for i in range(4))
        ## Method 3  建立麻烦，超参数调节可以利用key来采取不同的动作。(We can 结合 Method2??))
        if self.Hypara['mibidot2add']:
            mibidot_count = 2
        else:
            mibidot_count = 1
        self.Wphase1 = nn.ModuleDict({
            'mi':  nn.ModuleList(nn.Linear(2*self.encoder_size,mibidot_count*self.encoder_size)   for _ in range(mibidot_count)),
            'bi':  nn.ModuleList(nn.Linear(2*self.encoder_size,mibidot_count*self.encoder_size) for _ in range(mibidot_count)) \
                    if self.Hypara['mibidot2add'] else nn.Linear(2*self.encoder_size,2*self.encoder_size),
            'dot': nn.ModuleList(nn.Linear(2*self.encoder_size,mibidot_count*self.encoder_size)   for _ in range(mibidot_count)),
            'cat': nn.ModuleList(nn.Linear(2*self.encoder_size,mibidot_count*self.encoder_size)   for _ in range(2)),
        })
        self.Vphase1 = nn.ModuleDict({
            'mi':  nn.ModuleList([nn.Linear( mibidot_count*self.encoder_size,1) for _ in range(2)]),
            'dot': nn.ModuleList([nn.Linear(mibidot_count*self.encoder_size,1)  for _ in range(2)]),
            'bi':  nn.ModuleList([nn.Linear( mibidot_count*self.encoder_size,1) for _ in range(2)]),
            'cat': nn.ModuleList([nn.Linear(mibidot_count*self.encoder_size,1)  for _ in range(2)])
        })

        self.VWshare_all_order ={
            "Wg":0, 'Vshare':0, 'Wagger':0
        }
        self.shareGgru_order = 0
        # if not self.Hypara['Vshare_all']:
        #     Vshare_count = 4
        # else:
        #     Vshare_count = 1
        # if not self.Hypara['shareGgru']:
        #     shareGgru_count = 4
        # else:
        #     shareGgru_count = 1
        Vshare_count = 4
        shareGgru_count = 4
        
        if self.Hypara['usephase2']:  
            __Wg_factor = 2 if self.Hypara['phase2_fake_pass'] else 4
            self.__Wg     = nn.ModuleList( [ nn.Linear(4*self.encoder_size,__Wg_factor*self.encoder_size) for _ in range(Vshare_count)])
            if not  self.Hypara['phase2_fake_pass']:
                self.__Ggru   = nn.ModuleList( [ nn.GRU(input_size=4*self.encoder_size, hidden_size=self.encoder_size, batch_first=True,
                                    bidirectional=True) for _ in range(shareGgru_count)])
            
        self.__Vshare = nn.ModuleList( [ nn.Linear(self.encoder_size,1) for _ in range(Vshare_count)])
        self.__Wagger = nn.ModuleList( [ nn.Linear(2*self.encoder_size,self.encoder_size) for _ in range(Vshare_count)])
        # self.Wagger1=   nn.Linear(2*self.encoder_size,self.encoder_size)
        # self.Wagger2=   nn.Linear(2*self.encoder_size,self.encoder_size)

        self.AGGER = nn.GRU(input_size=2*self.encoder_size, hidden_size=self.encoder_size, batch_first=True,
                                bidirectional=True)
        self.Wq     =   nn.Linear(2*self.encoder_size,self.encoder_size)
        self.Wq2    =   nn.Linear(2*self.encoder_size,self.encoder_size)
        self.Wqp    =   nn.Linear(2*self.encoder_size,self.encoder_size)
        self.Wqp2   =   nn.Linear(2*self.encoder_size,self.encoder_size)

        self.Vr     =   nn.Linear(2*self.encoder_size,self.encoder_size)
        self.a_attention     =   nn.Linear(self.embedding_size,1)

        if self.Hypara['Vq_z']:
            self.Vq = nn.Linear(self.encoder_size,1)
            self.Vz = nn.Linear(self.encoder_size,1)
        # self.va = nn.Parameter(torch.randn([self.encoder_size,self.encoder_size]))
        # self.vq = nn.Parameter(torch.randn([self.encoder_size,self.encoder_size]))
        ## 单独测试可以动态赋值，这里出现a leaf Variable that requires grad has been used in an in-place operation
        # self.register_parameter('va', None)
        # self.register_parameter('vq', None)
        self.initiation()
        self.init_embedding()

    def init_embedding(self):
        if self.embedding == None:
            if not self.Hypara['pretrained_embedding']:
                self.embedding = nn.Embedding(self.vocab_size + 2,embedding_dim=self.embedding_size)
                initrange = 0.1
                nn.init.uniform_(self.embedding.weight, -initrange, initrange)
            else:
                dy_flag = not self.Hypara['pretrained_embedding_fix']
                self.embedding = preEmbedding(self.vocab_size + 1, self.embedding_size, \
                                   dy_flag,self.Hypara['pretrainedpath'])
    def initiation(self):
        for module in self.modules():
            if isinstance(module, nn.Linear):
                nn.init.xavier_uniform_(module.weight, 0.1)


    def setcuda(self,patitial=False):  ## or use inherited function cuda
        self.vcuda = True
    def checkcuda(self):
        if not hasattr(self,'vcuda'):
            self.vcuda = False
        return self.vcuda

    # def Dvaq(self,inputs):
    #     initfact = 1
    #     if self.va is None:
    #         # self.va = nn.Parameter(inputs.new(inputs.size())).normal_(0.1) ## this is error,normal_ is in-place
    #         self.va = nn.Parameter(initfact*torch.randn(self.encoder_size,self.encoder_size)) ## this is error,normal_ is in-place
    #     if self.vq is None:
    #         self.vq = nn.Parameter(initfact*torch.randn(self.encoder_size,self.encoder_size))
    #     return torch.cat([self.va]*inputs.size()[0]),torch.cat([self.vq]*inputs.size()[0])

    def useParam(self, attr=''):
        attr = getattr(self,attr)
        if attr :
            # attr = nn.Linear()
            pass
        else:
            attr   ## 需要动态使用的都是单位阵。 

    def Wg(self,inputs):
        if not self.Hypara['WgShare']:
            r = self.__Wg[self.VWshare_all_order['Wg']](inputs)
            self.VWshare_all_order['Wg'] += 1
            # print self.Wg.__name__
            if self.VWshare_all_order['Wg'] == 4:
                self.VWshare_all_order['Wg'] = 0
            return r
        return self.__Wg[0](inputs)  ### >> ??
    def Ggru(self,inputs):
        if not self.Hypara['shareGgru']:
            r = self.__Ggru[self.shareGgru_order](inputs)
            self.shareGgru_order += 1
            # print self.Ggru.__name__
            if self.shareGgru_order == 4:
                self.shareGgru_order = 0
            return r
        return self.__Ggru[0](inputs)
    def Vshare(self,inputs):
        if not self.Hypara['Vshare_all']:
            r = self.__Vshare[self.VWshare_all_order['Vshare']](inputs)
            self.VWshare_all_order['Vshare'] += 1
            # print self.Vshare.__name__
            if self.VWshare_all_order['Vshare'] == 4:
                self.VWshare_all_order['Vshare'] = 0
            return r
        return self.__Vshare[0](inputs)
    def Wagger(self,inputs):
        if not self.Hypara['WaggerShare']:     
            r = self.__Wagger[self.VWshare_all_order['Wagger']](inputs)
            self.VWshare_all_order['Wagger'] += 1
            # print self.Wagger.__name__
            if self.VWshare_all_order['Wagger'] == 4:
                self.VWshare_all_order['Wagger'] = 0
            return r
        return self.__Wagger[0](inputs)

    def fempty(self,inputs):
        return inputs

    def phase1(self,inputs):
        q,p,uq,up = inputs
        def mode(a,v,b,bi=False):
            printf (a.size(),b.size())
            if not bi:
                a = v( torch.tanh( a  )  ).squeeze(-1) 
            else:
                a = v( a  )     
            n = torch.softmax(a,-1).bmm(b)
            return n 
        def mode2(a,v):
            a1 = v( torch.tanh( a  )  )  
            n = torch.softmax(a,1) * a
            return n
        if self.Hypara['mibidot2add']: 
            # mi =    mode(self.Wphase1['mi'] [0](p).bmm(q.transpose(2,1)),     self.fempty,  q,True)
            # dot =   mode(self.Wphase1['dot'][0](p).bmm(q.transpose(2,1)),     self.fempty,  q,True)
            # bi =    mode(self.Wphase1['bi'] [0](p).bmm(q.transpose(2,1)),     self.fempty,  q,True)    # (32, 350, 256) (32, 12, 256) (128, 256)
            # cat =   mode(self.Wphase1['cat'][0](p).bmm(q.transpose(2,1)),     self.fempty,  q,True)
            mi =    mode(self.Wphase1['mi'] [0](uq)+(up),     self.Vphase1['mi'] [0],   q)
            dot =   mode(self.Wphase1['dot'][0](uq)+(up),     self.Vphase1['dot'][0],  q)
            bi =    mode(self.Wphase1['bi'] [0](uq)+(up),     self.Vphase1['bi'] [0],   q)    # (32, 350, 256) (32, 12, 256) (128, 256)
            cat =   mode(self.Wphase1['cat'][0](uq)+(up),     self.Vphase1['cat'][0],  q)
        
            mi =    mode2(self.Wphase1['mi'] [1](mi),     self.Vphase1['mi']    [1])
            dot =   mode2(self.Wphase1['dot'][1](dot),     self.Vphase1['dot']  [1])
            bi =    mode2(self.Wphase1['bi'] [1](bi),     self.Vphase1['bi']    [1])    
            cat =   mode2(self.Wphase1['cat'][1](cat),     self.Vphase1['cat']  [1])
        else:
            mi =    mode(self.Wphase1['mi'] [0](uq-up),     self.Vphase1['mi'][0],     q)
            dot =   mode(self.Wphase1['dot'][0](uq*up),    self.Vphase1['dot'][0],     q)
            bi =    mode(self.Wphase1['bi'](p).bmm(q.transpose(2,1)), self.fempty,   q, True)    # (32, 350, 256) (32, 12, 256) (128, 256)
            cat =   mode(self.Wphase1['cat'][0](uq)+self.Wphase1['cat'][1](up), self.Vphase1['cat'][0],   q)
        return mi,dot,bi,cat

    def phase2(self,inputs,inputs2):
        mi,dot,bi,cat = inputs
        p = inputs2
        def mode(a,b,Wg,gru):
            printf ( a.size(),b.size() )
            cat = torch.cat([a, b],-1)      ## [32,200,128]
            g = torch.sigmoid( Wg(cat) )       ## Wg是共享的吗?
            printf (g.size(), cat.size())
            r,_ = gru( g*cat )
            return r
        if self.Hypara['phase2_fake_pass']:
            gmi,gdot,gbi,gcat = (self.Wg(torch.cat([mi, p],-1)),self.Wg(torch.cat([dot, p],-1)),\
                            self.Wg(torch.cat([bi, p],-1)),self.Wg(torch.cat([cat, p],-1)))
        else:
            gmi = mode(mi,p,  self.Wg,  self.Ggru)        ## Wg是共享的吗? ## Ggru是共享的吗?
            gdot= mode(dot,p, self.Wg,  self.Ggru) 
            gbi = mode(bi,p,  self.Wg,  self.Ggru)
            # print self.shareGgru_order,self.VWshare_all_order
            gcat = mode(cat,p,self.Wg,  self.Ggru) 
            # print self.shareGgru_order,self.VWshare_all_order
        return gmi,gdot,gbi,gcat

    def phase3(self,inputs):
        gmi,gdot,gbi,gcat = inputs  #[B,L,M]
        def mode(a,V,W):
            l = V( torch.tanh(W(a))).transpose(2,1)    #[B,L,M] to [B,1,L] 
            return l
        aggermi = mode(gmi,     self.Vshare,self.Wagger)
        aggerdot = mode(gdot,   self.Vshare,self.Wagger)
        aggerbi = mode(gbi,     self.Vshare,self.Wagger)
        # print self.shareGgru_order,self.VWshare_all_order
        aggercat = mode(gcat,   self.Vshare,self.Wagger)
        # print self.shareGgru_order,self.VWshare_all_order
        # exit()

        agger = torch.cat([aggermi,aggerdot,aggerbi,aggercat],1) ## [B,1,L] to [B,4,L]
        agger_normal = torch.softmax(agger,1).unsqueeze(-1)  ## [B,4,L] to [B,4,L,1]
        gmi,gdot,gbi,gcat = (gmi.unsqueeze(1),gdot.unsqueeze(1),
                                gbi.unsqueeze(1),gcat.unsqueeze(1)) ## [B,L,M] to [B,1,L,M]
        agger = torch.cat([gmi,gdot,gbi,gcat],1) ## [B,1,L,M] to [B,4,L,M]
        agger = agger_normal * agger
        agger = agger.mean(1)    ## [B,4,L,M] to [B,L,M]
        return agger


    def forward(self,inputs,optimizer):
        [query, passage, answer,ids,pos,is_train] = inputs
        label = pos
        if self.checkcuda():
            query= query.cuda(); passage= passage.cuda();answer = answer.cuda();label = label.cuda();ids = ids.cuda()
        if self.Hypara['analy_bert']:
            pass

        q_embedding = self.embedding(query)
        p_embedding = self.embedding(passage)
        a_embeddings = self.embedding(answer)
        # if self.checkcuda():
        #     q_embedding= q_embedding.cuda()
        #     p_embedding= p_embedding.cuda()
        #     a_embeddings = a_embeddings.cuda()
        #     label = label.cuda()
        #     ids = ids.cuda()
        
        q,_ = self.q_encoder(q_embedding)  ## [32,10,128]
        p,_ = self.p_encoder(p_embedding)  ## [32,100,128]

        ## 把prediction layer 的一部分提前，因为它被两种超参数共享。 
        if self.Hypara['Vq_z']:
            Vq_z = self.Vq
        else:
            Vq_z = self.Vshare
        f = Vq_z(torch.tanh(self.Wq(q))).transpose(2,1) # [B,Q,1]
        printf (f.size(),q.size())
        ## TEST 1
        rq = torch.softmax(f,-1).bmm(q)  # [B,1,M]
        # rq = torch.softmax(f,-2) * q
        if self.Hypara['q_only_last']:
            q = rq
        
        uq = q.unsqueeze(1)              ## [32,1,10,128]
        up = p.unsqueeze(2)              ## [32,100,1,128]

        phase1 = self.phase1( [q,p,uq,up] )
        if self.Hypara['usephase2']:
            phase2 = self.phase2(phase1,p)
        else:
            phase2 = phase1
        phase3 = self.phase3(phase2)

        # agger = torch.cat(phase3,-1)
        agger = phase3
        agger,_ = self.AGGER(agger)     ## [B,L,2*encoder_size]  >> ??

        ## Prediction Layer
        printf (agger.size(), rq.size())
        if self.Hypara['q_only_last']:
            z = self.Wqp(agger)
        else:
            z = self.Wqp(agger)+self.Wqp2(rq)
        if self.Hypara['Vq_z']:
            Vq_z = self.Vz
        else:
            Vq_z = self.Vshare
        f = Vq_z(torch.tanh(z)).transpose(2,1)
        r = torch.softmax(f,-1).bmm(agger)  # [B,1,M]


        a_embedding, _ = self.a_encoder(a_embeddings.view(-1, a_embeddings.size(2), a_embeddings.size(3)))
        a_score = F.softmax(self.a_attention(a_embedding), 1)
        a_output = a_score.transpose(2, 1).bmm(a_embedding).squeeze()
        a_embedding = a_output.view(a_embeddings.size(0), 3, -1)

        printf (a_embedding.size(), r.size())
        if self.Hypara['Vr'] == True:
            r = self.Vr(r)
        predict = a_embedding.bmm( F.leaky_relu(r.transpose(2,1),-1) ).squeeze(-1)  ## model.py用的是leaky_relu
        predict = torch.softmax(predict,-1)                                                         ## 用softmax比较好?

        if not self.Hypara['analy_bert']:
            if not is_train:
                return predict.argmax(1)

            gather = predict.gather(-1,label.unsqueeze(-1))
            loss = -torch.log(gather).mean()  ## 只有一个标签，所以之际mean就行。
            # print "gather",gather
            # exit()
        else:
            if not is_train:
                if label.dim() == 5:
                    return predict.argmax(1), predict.argmax(1)
                else:
                    return predict.argmax(1)
            if label.dim() == 3:  # [32,3,1]
                label = label.squeeze(-1)
                assert label.dim() == 2
            loss_every = (-torch.log(predict) * label[:,0:3]).sum(-1)  ## batch mean之前别忘了sum
            loss1 = ( loss_every ).mean(); 
            loss = loss1
            if label.size(-1) == 5:  ## incle two relations label
                pass
            else: 
                assert label.size(-1) == 3
            if self.Hypara['l2_gru_ih']:
                l2_loss = l2_sum("weight_ih_l0",[m for m in self.modules() if isinstance(m,nn.GRU)])
                loss = loss + l2_loss*self.Hypara['l2_loss_gamma']

        return loss

"""
11.2
修复一个bug , Bilinear的时候没有 a = v( torch.tanh( a  )  ).squeeze(-1)  中的tanh

以前的model2 Prediction Layer 好像有问题，测试一下，已经截图。

思考一下这几个GRU的维度变化是否正确

Vshare --> 

    baseline是把我理解的反向，即p,q相反，反正都是混合着考虑的，而且也不知道要知道的词是否在文章中，所以不能只当作选择某个固定
的词语，而是选择理解的词语?

11.3
为什么share gru等后cuda反而升高？ 单独开启4个的gru应当要比cuda稍多的（grad空间应该一样大？）才对

('MwAN Model total parameters(explicit):', 14524299) 注意这里的参数不是字节. MxN
识别 --> 翻译 --> 理解
model.py + embedding
model6.py bug
model6.py resnet test
model5_separateskip.py 扩展parameter，丢弃q_conv_p测试.
model6 model5 fusion 测试 (直接融合，先不单独测试没有q的情形。)
多loss fusion 方式思考。 

"""

"""
            "VWshare_all": False, 
            "shareGgru": True,
    ('vacab_size', 96973)
    ('MwAN Model total parameters(explicit):', 2805642)   
    train data size 300, dev data size 300
    0.002
    Wg
    Wg
    Wg
    0 {'Wg': 3, 'Wagger': 0, 'Vshare': 0}
    Wg
    0 {'Wg': 0, 'Wagger': 0, 'Vshare': 0}
    Wagger
    Vshare
    Wagger
    Vshare
    Wagger
    Vshare
    0 {'Wg': 0, 'Wagger': 3, 'Vshare': 3}
    Wagger
    Vshare
    0 {'Wg': 0, 'Wagger': 0, 'Vshare': 0}
    ka@ka-15-ce0xx:/mnt/u16/home/ka/Documents/proj/aichallenge2018/opinion0$ python trainnew.py --cuda --batch_size 8 --datascope 300
                "VWshare_all": False, 
                "shareGgru": False,
    ('vacab_size', 96973)
    ('MwAN Model total parameters(explicit):', 4284810)
    train data size 300, dev data size 300
    0.002
    Wg
    Ggru
    Wg
    Ggru
    Wg
    Ggru
    3 {'Wg': 3, 'Wagger': 0, 'Vshare': 0}
    Wg
    Ggru
    0 {'Wg': 0, 'Wagger': 0, 'Vshare': 0}
    Wagger
    Vshare
    Wagger
    Vshare
    Wagger
    Vshare
    0 {'Wg': 0, 'Wagger': 3, 'Vshare': 3}
    Wagger
    Vshare
    0 {'Wg': 0, 'Wagger': 0, 'Vshare': 0}
    ka@ka-15-ce0xx:/mnt/u16/home/ka/Documents/proj/aichallenge2018/opinion0$ python trainnew.py --cuda --batch_size 8 --datascope 300
    
            "VWshare_all": True, 
            "shareGgru": False,
    ('vacab_size', 96973)
    ('MwAN Model total parameters(explicit):', 3397767)
    train data size 300, dev data size 300
    0.002
    Ggru
    Ggru
    Ggru
    3 {'Wg': 0, 'Wagger': 0, 'Vshare': 0}
    Ggru
    0 {'Wg': 0, 'Wagger': 0, 'Vshare': 0}
    0 {'Wg': 0, 'Wagger': 0, 'Vshare': 0}
    0 {'Wg': 0, 'Wagger': 0, 'Vshare': 0}
    ka@ka-15-ce0xx:/mnt/u16/home/ka/Documents/proj/aichallenge2018/opinion0$ python trainnew.py --cuda --batch_size 8 --datascope 300
    
            "VWshare_all": True, 
            "shareGgru": True,
    ('vacab_size', 96973)
    ('MwAN Model total parameters(explicit):', 1918599)
    train data size 300, dev data size 300
    0.002
    0 {'Wg': 0, 'Wagger': 0, 'Vshare': 0}
    0 {'Wg': 0, 'Wagger': 0, 'Vshare': 0}
    0 {'Wg': 0, 'Wagger': 0, 'Vshare': 0}
    0 {'Wg': 0, 'Wagger': 0, 'Vshare': 0}

self.Hypara = {
    "Vr": True,  ## 不能设为false, answer gru 变为 1/2
    "q_only_last": False,
    "Vq_z":True,        ## 与 VWshare_all 的self.Vshare函数冲突。
    "VWshare_all": False,   ## 这样做绝对错了。
    "shareGgru": False
    # "usephase2": False
}
python trainnew.py --cuda --batch_size 4 --emsize 300 --nhid 300 --datascope 3000
continue_update_period = 8
    |------epoch 32 train error is 0.037725  eclipse 39.87%------|
    save half model
    |------epoch 32 train error is 0.039035  eclipse 79.87%------|
    epcoh 32 dev acc is 51.233333, best dev acc 54.466667
    0.002
    |------epoch 33 train error is 0.039780  eclipse 39.87%------|
    save half model
    |------epoch 33 train error is 0.049942  eclipse 79.87%------|
    epcoh 33 dev acc is 48.466667, best dev acc 54.466667
    0.002
"""

"""
Hypara dict {'VWshare_all': False, 'pretrained_embedding': True, 'q_only_last': True, 'Vq_z': True, 'Vr': True, 'pretrainedpath': 'data/pretrained_word2id_tencent.obj', 'continue_update_period': 8, 'pretrained_embedding_fix': True, 'shareGgru': False}
('MwAN Model total parameters(explicit):', 10446210)
python temp_11_3/trainnew_11_3.py --cuda --emsize 200 --nhid 200 --batch_size 8
('MwAN Model total parameters(explicit):', 5149811)
"""

"""
python temp_11_3/trainnew_11_3.py --cuda --emsize 200 --nhid 200 --batch_size 8
('environment change:', '/mnt/u16/home/ka/Documents/proj/aichallenge2018/opinion1/temp_11_3', 'to', '/mnt/u16/home/ka/Documents/proj/aichallenge2018/opinion1')
('vacab_size', 96973)
Hypara dict {'VWshare_all': False, 'pretrained_embedding': True, 'q_only_last': True, 'Vq_z': True, 'Vr': True, 'pretrainedpath': 'data/pretrained_word2id_tencent.obj', 'continue_update_period': 8, 'pretrained_embedding_fix': True, 'shareGgru': False}
('MwAN Model total parameters(explicit):', 10446210)
train data size 30000, dev data size 30000
0.002
pretrained embedding initing
('word2id length', 96973)
('word_count length', 332265)
('pretrained data length', 92187)
|------epoch 0 train error is 0.863450  eclipse 95.97%------|
epcoh 0 dev acc is 57.110000, best dev acc 57.110000
|------epoch 1 train error is 0.781665  eclipse 95.97%------|
epcoh 1 dev acc is 58.693333, best dev acc 58.693333
|------epoch 2 train error is 0.731420  eclipse 95.97%------|
epcoh 2 dev acc is 61.473333, best dev acc 61.473333
|------epoch 3 train error is 0.789374  eclipse 95.97%------|
epcoh 3 dev acc is 64.013333, best dev acc 64.013333
|------epoch 4 train error is 0.704782  eclipse 95.97%------|
epcoh 4 dev acc is 64.783333, best dev acc 64.783333
|------epoch 5 train error is 0.703930  eclipse 95.97%------|
epcoh 5 dev acc is 66.276667, best dev acc 66.276667
|------epoch 6 train error is 0.703200  eclipse 95.97%------|
epcoh 6 dev acc is 66.423333, best dev acc 66.423333
|------epoch 7 train error is 0.637655  eclipse 95.97%------|
epcoh 7 dev acc is 66.596667, best dev acc 66.596667
|------epoch 8 train error is 0.563379  eclipse 95.97%------|
epcoh 8 dev acc is 65.453333, best dev acc 66.596667

没有phase2
 python temp_11_3/trainnew_11_3.py --cuda --emsize 200 --nhid 200 --batch_size 8('environment change:', '/mnt/u16/home/ka/Documents/proj/aichallenge2018/opinion1/temp_11_3', 'to', '/mnt/u16/home/ka/Documents/proj/aichallenge2018/opinion1')
('vacab_size', 96973)
Hypara dict {'VWshare_all': False, 'usephase2': False, 'pretrained_embedding': True, 'q_only_last': True, 'Vq_z': True, 'Vr': True, 'pretrainedpath': 'data/pretrained_word2id_tencent.obj', 'continue_update_period': 8, 'pretrained_embedding_fix': True, 'shareGgru': False}
('MwAN Model total parameters(explicit):', 3073410)
train data size 30000, dev data size 30000
0.002
pretrained embedding initing
('word2id length', 96973)
('word_count length', 332265)
('pretrained data length', 92187)
|------epoch 0 train error is 0.890844  eclipse 95.97%------|
epcoh 0 dev acc is 57.173333, best dev acc 57.173333
|------epoch 1 train error is 0.886503  eclipse 95.97%------|
epcoh 1 dev acc is 57.210000, best dev acc 57.210000
|------epoch 2 train error is 0.891988  eclipse 95.97%------|
epcoh 2 dev acc is 57.380000, best dev acc 57.380000
|------epoch 3 train error is 0.905358  eclipse 95.97%------|
epcoh 3 dev acc is 57.503333, best dev acc 57.503333
|------epoch 4 train error is 0.855592  eclipse 95.97%------|
epcoh 4 dev acc is 57.206667, best dev acc 57.503333

修改 'phase2_fake_pass': True, 'usephase2': True （phase2不cat p）
    acc 57 左右

phase2 不进行gru,与全phase2最好之差了0.3%左右,可以忽略吧
python temp_11_3/trainnew_11_3.py --cuda --emsize 200 --nhid 200 --batch_size 8('environment change:', '/mnt/u16/home/ka/Documents/proj/aichallenge2018/opinion1/temp_11_3', 'to', '/mnt/u16/home/ka/Documents/proj/aichallenge2018/opinion1')
('vacab_size', 96973)
Hypara dict {'phase2_fake_pass': True, 'usephase2': True, 'pretrained_embedding': True, 'VWshare_all': False, 'q_only_last': True, 'Vq_z': True, 'Vr': True, 'pretrainedpath': 'data/pretrained_word2id_tencent.obj', 'continue_update_period': 8, 'pretrained_embedding_fix': True, 'shareGgru': False}
('MwAN Model total parameters(explicit):', 4355010)
train data size 30000, dev data size 30000
0.002
pretrained embedding initing
('word2id length', 96973)
('word_count length', 332265)
('pretrained data length', 92187)
|------epoch 0 train error is 0.878175  eclipse 95.97%------|
epcoh 0 dev acc is 58.230000, best dev acc 58.230000
|------epoch 1 train error is 0.822812  eclipse 95.97%------|
epcoh 1 dev acc is 60.133333, best dev acc 60.133333
|------epoch 2 train error is 0.786478  eclipse 95.97%------|
epcoh 2 dev acc is 62.270000, best dev acc 62.270000
|------epoch 3 train error is 0.729690  eclipse 95.97%------|
epcoh 3 dev acc is 63.110000, best dev acc 63.110000
|------epoch 4 train error is 0.733806  eclipse 95.97%------|
epcoh 4 dev acc is 64.180000, best dev acc 64.180000
|------epoch 5 train error is 0.740370  eclipse 95.97%------|
epcoh 5 dev acc is 65.900000, best dev acc 65.900000
|------epoch 6 train error is 0.697817  eclipse 95.97%------|
epcoh 6 dev acc is 65.716667, best dev acc 65.900000
|------epoch 7 train error is 0.623709  eclipse 95.97%------|
epcoh 7 dev acc is 66.260000, best dev acc 66.260000
|------epoch 8 train error is 0.598894  eclipse 95.97%------|
epcoh 8 dev acc is 65.096667, best dev acc 66.260000
|------epoch 9 train error is 0.596414  eclipse 95.97%------|
epcoh 9 dev acc is 65.313333, best dev acc 66.260000

四个都是cat类型的multiway, 结果有1% 。 注意参数: 'VWshare_all': False, 'usephase2': True,'phase2_fake_pass': True
python temp_11_3/trainnew_11_3.py --cuda --emsize 200 --nhid 200 --batch_size 8('environment change:', '/mnt/u16/home/ka/Documents/proj/aichallenge2018/opinion1/temp_11_3', 'to', '/mnt/u16/home/ka/Documents/proj/aichallenge2018/opinion1')
('vacab_size', 96973)
Hypara dict {'VWshare_all': False, 'usephase2': True, 'pretrained_embedding': True, 'phase2_fake_pass': True, 'analy_bert': False, 'q_only_last': True,'Vq_z': True, 'mibidot2add': True, 'l2_gru_ih': False, 'Vr': True, 'l2_loss_gamma': 0.01, 'pretrainedpath': 'data/pretrained_word2id_tencent.obj', 'continue_update_period': 8, 'pretrained_embedding_fix': True, 'shareGgru': False}
pretrained embedding initing
('word2id length', 96973)
('word_count length', 332265)
('pretrained data length', 92187)
('MwAN Model total parameters(explicit):', 4515611)
train data size 30000, dev data size 30000
|------epoch 0 train error is 0.865948  eclipse 95.97%------|
epcoh 0 dev acc is 57.276667, best dev acc 57.276667
|------epoch 1 train error is 0.885745  eclipse 95.97%------|
epcoh 1 dev acc is 60.180000, best dev acc 60.180000
|------epoch 2 train error is 0.849899  eclipse 95.97%------|
epcoh 2 dev acc is 63.200000, best dev acc 63.200000
|------epoch 3 train error is 0.737773  eclipse 95.97%------|
epcoh 3 dev acc is 65.296667, best dev acc 65.296667
|------epoch 4 train error is 0.685150  eclipse 95.97%------|
epcoh 4 dev acc is 65.136667, best dev acc 65.296667
|------epoch 5 train error is 0.563134  eclipse 95.97%------|
epcoh 5 dev acc is 64.986667, best dev acc 65.296667

+号直接换成* 号,结果:
    最大 65.363333

(W1Q）*P + W1P, 不过此时由于以前设置的经过W维度减半导致需要先扩展维度测试，那么以前的有可能需要重新测试。
    最大 64.950000
然后重测+号换成*号:
    65.006667
全部双线性：
    65.400000
共享W *
    64.26333
如下的操作 acc 65.143333
    mi =    mode(self.Wphase1['mi']  [0](uq)+(up), self.Vphase1['mi'] [0],   q)
    mi =    mode2(self.Wphase1['mi'] [1](mi),      self.Vphase1['mi'] [1])

WaggerShare True Vshare False,即phase3采用四个共享的Weight(为了拔出同样的信息测试)，acc
    59.296667 ->... --> 65.410000
Vshare_all --> 
    65.243333
"""