# -*- coding: utf-8 -*-
import argparse
import cPickle
import torch
import json

import sys,os
def ChangePath2Cwd():
    A = sys.path[0]
    sys.path.remove(sys.path[0])
    sys.path.append(os.getcwd())   ## then you can import preprocess in fanterfolder
    B = sys.path[-1]
    print ('environment change:',A,'to',B)
ChangePath2Cwd()

# from model import MwAN
# from model_modifier import MwAN
# from painternerCollection.dmn import MwAN
# from painternerCollection.dmnPlus import MwAN
# from painternerCollection.SeparateDmn3 import MwAN
# from painternerCollection.alphaCnn6 import MwAN
# from painternerCollection.alphaCnn7 import MwAN
# from painternerCollection2.attentionPool2 import MwAN
# from painternerCollection2.CRNN import MwAN
# from painternerCollection2.CRNN2_1d5 import MwAN
# from painternerCollection2.fullrnn3 import MwAN, NoCudaEmbedding
# from painternerCollection2.beta0 import MwAN
# from ptmodel.model1 import MwAN
# from ptmodel.model6 import MwAN
# from temp_11_3.model_11_3 import MwAN
# from temp_11_3.model6_11_3 import MwAN
# from temp_11_3.model6_11_6 import MwAN
# from temp_11_3.test_model6_11_6 import MwAN
# from temp_11_3.point5 import MwAN
# from temp_11_3.point6 import MwAN
from temp_11_3.graphmerge3 import MwAN

from preprocess import process_data
from utils import *
from termcolor import colored
from ptColl2.swats import Swats
from painternerlib.HyparaDebug import fputHDebug,fputHypara
from temp_11_3.parameter_debug import ParameterDebug
# from ptColl2.graphic import GraphicUpdate
import random
import time

parser = argparse.ArgumentParser(description='PyTorch implementation for Multiway Attention Networks for Modeling '
                                             'Sentence Pairs of the AI-Challenges')

parser.add_argument('--data', type=str, default='data/',
                    help='location directory of the data corpus')
parser.add_argument('--threshold', type=int, default=5,
                    help='threshold count of the word')
parser.add_argument('--epoch', type=int, default=5000,
                    help='training epochs')
parser.add_argument('--emsize', type=int, default=128,
                    help='size of word embeddings')
parser.add_argument('--nhid', type=int, default=128,
                    help='hidden size of the model')
parser.add_argument('--batch_size', type=int, default=32, metavar='N',
                    help='batch size')
parser.add_argument('--log_interval', type=int, default=300,
                    help='# of batches to see the training error')
parser.add_argument('--dropout', type=float, default=0.5,
                    help='dropout applied to layers (0 = no dropout)')
parser.add_argument('--cuda', action='store_true',
                    help='use CUDA')
parser.add_argument('--patitialcuda', action='store_true',
                    help='use patitial CUDA')
parser.add_argument('--save', type=str, default='model.pt',
                    help='path to save the final model')
parser.add_argument('--model', type=str, default='model.pt',
                    help='model path')
parser.add_argument('--picklebuild', action='store_true',
                    help='build picklefile')

parser.add_argument('--retain_graph', action='store_true',
                    help='build picklefile')

parser.add_argument('--restore', action='store_true',
                    help='build picklefile')
parser.add_argument('--datascope', type=int, default=0,
                    help='# train and dev data ratio')                   

args = parser.parse_args()

global_Hypara = fputHypara({
    "continue_update_period": 1,
    "train_embedding": False,
    "optim":'Adamax', ## 'Adamax', 'Adam', 'SGD'
    'l2': 0.0
})
global_HDebug = fputHDebug({
    "weight_record": 'epoch' ## optional: 'offen', 'epoch', 'no'
})
paramdg = ParameterDebug()
retain_graph = [False]

if args.picklebuild:
    process_data(args.data, args.threshold)

with open("data/word2id.obj",'rb') as f:
    word2id_data = cPickle.load(f)
# vacab_size = 98745
vocab_size = len(word2id_data)
print("vacab_size",vocab_size)

if args.restore:    
    with open(args.model, 'rb') as f:
        print colored("restoring from model.pt","blue")
        model = torch.load(f)
else:              
    model = MwAN(vocab_size=vocab_size, embedding_size=args.emsize, encoder_size=args.nhid, drop_out=args.dropout)
    print('MwAN Model total parameters(explicit):', get_model_parameters(model))
    # embedmodel = NoCudaEmbedding(vocab_size=vocab_size,embedding_size=args.emsize)
    # print('embedmodel Model total parameters:', get_model_parameters(embedmodel))

if args.cuda:
    model.cuda()  ## will take several seconds
    if hasattr(model,'setcuda'):
        model.setcuda()
elif args.patitialcuda:
    model.setcuda(patitial=True)
if global_Hypara['optim'] == 'Adamax':
    optimizer = torch.optim.Adamax(model.parameters(),weight_decay=global_Hypara['l2'])
elif global_Hypara['optim'] == 'Adam':
# optimizer = Swats(model.parameters(),weight_decay=global_Hypara['l2'])
    optimizer = torch.optim.Adam(model.parameters(),weight_decay=global_Hypara['l2'])
elif global_Hypara['optim'] == 'SGD':
    optimizer = torch.optim.SGD(model.parameters(),lr=0.001,momentum=0.9,weight_decay=global_Hypara['l2'])
# optimizer = Swats(model.parameters())
# torch.optim.lr_scheduler.MultiStepLR(optimizer,[1,2,3,4],gamma=0.2)
# scheduler = torch.optim.lr_scheduler.LambdaLR(optimizer,lr_lambda=lambda epoch: 0.2**epoch)
scheduler = torch.optim.lr_scheduler.LambdaLR(optimizer,lr_lambda=lambda epoch: 1)

with open(args.data + 'train.pickle', 'rb') as f:
    train_data = cPickle.load(f)

    ## 从training中剥夺的30000 line数据，copy代码后只需运行一次。
    # from preprocess import seg_data, transform_data_to_id
    # with open('data/word2id.obj', 'rb') as f__:
    #     word2id = cPickle.load(f__)

    # raw_data = seg_data('data/develop/training_30000line.json',False)
    # transformed_data = transform_data_to_id(raw_data, word2id)
    # train_data = [x + [y[2]] for x, y in zip(transformed_data, raw_data)]
    # with open('data/develop/training_30000line.pickle','wb') as f___:
    #     cPickle.dump(train_data,f___)

    # with open('data/develop/training_30000line.pickle','rb') as f___:
    #     train_data = cPickle.load(f___)

with open(args.data + 'dev.pickle', 'rb') as f:
    dev_data = cPickle.load(f)

if args.datascope != 0:
    dev_data = dev_data[:args.datascope]
    train_data = train_data[:args.datascope]

dev_data = sorted(dev_data, key=lambda x: len(x[1]))

print('train data size {:d}, dev data size {:d}'.format(len(train_data), len(dev_data)))


def train(epoch):
    shuffle_answer = True
          
    for p in model.embedding.parameters():
        p.requires_grad = global_Hypara['train_embedding']

    scheduler.step()
    print(optimizer.state_dict()['param_groups'][0]['lr'])  ## 查看一下学习率。
    model.train()
    data = shuffle_data(train_data, 1)
    total_loss = 0.0    
    save_one_flag = False
    continue_update_times = 0
    continue_update_period = global_Hypara['continue_update_period']
    for num, i in enumerate(range(0, len(data), args.batch_size)):
        one = data[i:i + args.batch_size]
        query, _ = padding([x[0] for x in one], max_len=50)
        passage, _ = padding([x[1] for x in one], max_len=500)
        answer = pad_answer([x[2] for x in one])
        ids = [x[3] for x in one]

        if shuffle_answer:
            an_label = [[[0],[1],[2]] for _ in range(len(answer))]  ## do not use batch_size,cause tails' may be less than it
            try:
                answer = np.concatenate([answer,an_label],-1)
            except:
                print i*100.0/len(dev_data)
                exit()
            for ii in range(len(answer)):
                np.random.shuffle(answer[ii,:,:])  ## shuffle only answer
            label = answer[:,:,-1]
            pos = np.argmin(label,-1)
            assert len(pos.shape) == 1
            answer = answer[:,:,:-1]
 
        query, passage, answer,ids,pos = (torch.LongTensor(query), torch.LongTensor(passage), 
                                                torch.LongTensor(answer),torch.LongTensor(ids),
                                                torch.LongTensor(pos))

        continue_update_times += 1
        if continue_update_times > continue_update_period:
            continue_update_times = 0
        if num == 0:
            optimizer.zero_grad() ## 有可能上一次epoch没有optimizer.step()而残留

        if continue_update_times == 0:
            optimizer.zero_grad()  #Clears the gradients of all optimized torch.Tensor s.
        model.setmaskinplaceflag(continue_update_times)
        loss = model([query, passage, answer,ids,pos,'train'],optimizer)
        if args.retain_graph: retain_gra = not retain_graph[0] 
        else: retain_gra = retain_graph[0]
        timeA = time.time()
        loss.backward(retain_graph=retain_gra)        # 告诉优化器需要更新的方式？因为初始化optimizer把model所有parameters都加入，但不一定都想更新。s
        timeB = time.time() - timeA
        # print("backward",timeB)
        total_loss += loss.item()

        if continue_update_times == continue_update_period:
            optimizer.step()       # parameter update

        if (num + 1) % args.log_interval == 0:
            print '|------epoch {:d} train error is {:f}  eclipse {:.2f}%------| iter: {:d}'.format(epoch,
                                                                                         total_loss / args.log_interval,
                                                                                         i * 100.0 / len(data),num+epoch*len(data)/args.batch_size)
            total_loss = 0

            if hasattr(model,'getloss'):
                print "detail loss", model.getloss()

            if global_HDebug['weight_record'] == 'offen':
                paramdg.analyse(model,num+epoch*len(data)/args.batch_size) 
            
        if (i*100.0/len(data) >= 50):
            if(not save_one_flag):
                save_one_flag=True
                with open('half'+args.save, 'wb') as f:
                    print colored('save half model',"green")
                    torch.save(model, f)

    if global_HDebug['weight_record'] == 'epoch':
        paramdg.analyse(model,epoch)
    

def test():
    shuffle_answer = True

    model.eval()
    r, a = 0.0, 0.0
    with torch.no_grad():
        for i in range(0, len(dev_data), args.batch_size):
            one = dev_data[i:i + args.batch_size]
            query, _ = padding([x[0] for x in one], max_len=50)
            passage, _ = padding([x[1] for x in one], max_len=500)
            answer = pad_answer([x[2] for x in one]) ## [32,3,4]

            if shuffle_answer:
                an_label = [[[0],[1],[2]] for _ in range(len(answer))] ## do not use batch_size,cause tails' may be less than it
                try:
                    answer = np.concatenate([answer,an_label],-1)
                except:
                    print i*100.0/len(dev_data)
                    exit()
                for i in range(len(answer)):
                    np.random.shuffle(answer[i,:,:])  ## shuffle only answer
                label = answer[:,:,-1]
                pos = np.argmin(label,-1)
                assert len(pos.shape) == 1
                answer = answer[:,:,:-1]

            ids = [x[3] for x in one]
            query, passage, answer,ids,pos = (torch.LongTensor(query), torch.LongTensor(passage), 
                                        torch.LongTensor(answer),torch.LongTensor(ids),
                                        torch.LongTensor(pos))
            output = model([query, passage, answer,ids,pos, 'eval'],optimizer)
            if shuffle_answer:
                assert len(output.size()) == 1
                pos =torch.tensor(pos)
                if args.cuda:
                    pos= pos.cuda()
                r += torch.eq(output,pos).sum().item()
            else:
                r += torch.eq(output, 0).sum().item()
            a += len(one)
    return r * 100.0 / a


def main():
    best = 0.0
    for epoch in range(args.epoch):
        train(epoch)
        acc = test()
        # GraphicUpdate(acc)
        with open(args.save+'always','wb') as f:
            torch.save(model,f)
        if acc > best:
            best = acc
            with open(args.save, 'wb') as f:
                torch.save(model, f)
        print 'epcoh {:d} dev acc is {:f}, best dev acc {:f}'.format(epoch, acc, best)


if __name__ == '__main__':
    main()
