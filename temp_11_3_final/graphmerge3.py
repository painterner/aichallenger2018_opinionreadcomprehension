# -*- coding: utf-8 -*-
import torch
from torch import nn
from torch.nn import functional as F
from termcolor import colored
import logging
from painternerCollection.utils import printonece
from painternerlib.HyparaDebug import fputHypara
from painternerCollection.utils4net import preEmbedding
from painternerlib.torch.function import l2_sum
from ptmodel.masknet import MaskInplace
from ptmodel.TwoGruNewEncoder import TwoGru,NewEncoder
from ptmodel.AllShare2 import SubWfactor, GRUWithShare, MainW
import time
logger = logging.getLogger(__name__)
logging.basicConfig(level = logging.DEBUG,
                    format = '%(asctime)s[%(levelname)s] ---- %(message)s',
                    )

class Print():
    def __init__(self):
        self.action = True
    def cancel(self):
        self.action = False
    def open(self):
        self.action = True
    def __call__(self,*inputs):
        if not self.action:
            return
        print inputs

printf = Print()
printf.cancel()

## 共享权重分析:
## initfact = 1 -> 0.1
## va vq 用parameter产生的感觉不可靠，先用uq,q代替。
## bias = False ? bi output-> 2*encoder_size ?
## 新建立一个模块的Wrap，每次输入先检测输入大小与预期的是否一致，后续用脚本把外层剥去。 ??
## Wg 扩展到4倍？
## vq --> 不是用bmm,用multiply
## 调试参： a 2*encode_size -> encode_size / 2
## scratch 计算GPU消耗情况。
## 持续学习，每次添加新的都可以持续进行学习。
## 三项测试(百度云一同测试)  
# 1. initfact 0.1 2. 移除phase2 ,3  <-
# 2.1 修改self.Wq(q)+self.Wq2(vq) ,之前改为unsqueeze(),让每个与另一个相联系
# self.Wqp(agger)+self.Wqp2(rq) 移除直接用之前的结果来看能否选择出来。        
#  3,移除phase2 4. 评价改为余弦 5.用单个词来学习。
## 6. 动态添加训练数据.
## 7. 最后一步时已经脱离了注意力而得到一个总结点，所以不能再用question或者answer的任何一个长度，已经与此无关了?
## 或者这只是抽象的长度，所以是合理的。

## paper
## drop_out 机制.参数设置成为0？合适吗?
## batch_normal用于NLP的理论依据存在?
## GRU机制
class MwAN(nn.Module):
    def __init__(self, vocab_size, embedding_size, encoder_size, drop_out=0.5):
        super(MwAN,self).__init__()

        self.embedding_size = embedding_size
        self.encoder_size = encoder_size
        self.hidden_size = encoder_size
        self.vocab_size = vocab_size
        self.class_size =  3
        self.drop_out=drop_out
        self.Hypara = fputHypara({
            "q_only_last": True,
            "Vq_z":True,        ## 与 VWshare_all 的self.Vshare函数冲突。

            "pretrained_embedding":True,
            "pretrained_embedding_fix": True,
            "pretrainedpath": 'data/pretrained_word2id_tencent.obj',

            'analy_bert':False,
            'l2_gru_ih':False,
            'l2_loss_gamma': 0.01,
            
            "dropout_q_encoder":        False,
            "dropout_p_encoder":        False,
            "dropout_mi":               False,
            "dropout_dot":              False,
            "dropout_cat":              False,
            "dropout_bi":               False,
            "dropout_q_W_selfattn":     False,
            "dropout_agger":            False,
            "dropout_agger_W_selfattn": False,
            "dropout_input": False,
            "maskinplace":False,
            "maskinplace_prob": 0.0001,
            "maskinplace_zero": False,

            "AGGER": 'nn.GRU' ## optional: 'NewEncoder','GRUWithShare','nn.GRU','TwoGru'
        })

        print "Hypara dict",self.Hypara

        self.embedding = None
        self.q_encoder = nn.GRU(input_size=embedding_size, hidden_size=encoder_size, batch_first=True,
                            bidirectional=True)
        # self.p_encoder = nn.GRU(input_size=embedding_size, hidden_size=encoder_size, batch_first=True,
        #                         bidirectional=True)
        self.p_encoder = self.q_encoder
        # self.a_encoder = nn.GRU(input_size=embedding_size, hidden_size=embedding_size / 2, batch_first=True,
        #                     bidirectional=True)
        self.a_encoder = self.q_encoder

        self.MainRnn = nn.GRU(input_size=4*self.encoder_size, hidden_size=4*self.encoder_size, batch_first=True,
                                bidirectional=False)
                    
        self.Wq     =   nn.Linear(2*self.encoder_size,self.encoder_size)
        self.Wqp    =   nn.Linear(4*self.encoder_size,2*self.encoder_size)

        self.Vr     =   nn.Linear(4*self.encoder_size,2*self.encoder_size)
        self.a_attention     =   nn.Linear(2*self.embedding_size,1)

        if self.Hypara['Vq_z']:
            self.Vq = nn.Linear(self.encoder_size,1)
            self.Vz = nn.Linear(2*self.encoder_size,1)

        self.dropoutlayer = nn.ModuleList(
            [nn.Dropout(p=self.drop_out) for _ in range((20))] ## 重复使用一个应该也没事吧 ？
        )
        if self.Hypara['maskinplace']:
            self.maskinplace = MaskInplace(self.Hypara['maskinplace_prob'],self.Hypara['maskinplace_zero'])

        self.Wgraph = nn.Linear(4*self.encoder_size,2*encoder_size)

        self.reconstr = nn.GRU(input_size=2*self.encoder_size, hidden_size=encoder_size, batch_first=True,
                               bidirectional=False)
        self.initiation()
        self.init_embedding()

    def init_embedding(self):
        if self.embedding == None:
            if not self.Hypara['pretrained_embedding']:
                self.embedding = nn.Embedding(self.vocab_size + 2,embedding_dim=self.embedding_size)
                initrange = 0.1
                nn.init.uniform_(self.embedding.weight, -initrange, initrange)
            else:
                dy_flag = not self.Hypara['pretrained_embedding_fix']
                self.embedding = preEmbedding(self.vocab_size + 1, self.embedding_size, \
                                   dy_flag,self.Hypara['pretrainedpath'])
    def initiation(self):
        for module in self.modules():
            if isinstance(module, nn.Linear):
                nn.init.xavier_uniform_(module.weight, 0.1)


    def setmaskinplaceflag(self,continue_update_times):
        assert isinstance( continue_update_times,int)
        self.continue_update_times = continue_update_times
    def checkmaskinplaceflag(self):
        if hasattr(self,'continue_update_times'):
            if self.continue_update_times == 0:
                return True
        return False
    def setcuda(self,patitial=False):  ## or use inherited function cuda
        self.vcuda = True
    def checkcuda(self):
        if not hasattr(self,'vcuda'):
            self.vcuda = False
        return self.vcuda


    def forward(self,inputs,optimizer):
        """
            encoder  multiway  q_selfattn  agger  agger_selfattn 
        """
        if self.Hypara['maskinplace']:
            if self.checkmaskinplaceflag():
                # print "mask inplace start"
                self.maskinplace(self)
        [query, passage, answer,ids,pos,is_train] = inputs
        label = pos
        if self.checkcuda():
            query= query.cuda(); passage= passage.cuda();answer = answer.cuda();label = label.cuda();ids = ids.cuda()
        if self.Hypara['analy_bert']:
            pass
        if self.Hypara['dropout_input']:
            query = self.dropoutlayer[-1](query);passage = self.dropoutlayer[-2](passage)
            answer=self.dropoutlayer[-3](answer)
        q_embedding = self.embedding(query)
        p_embedding = self.embedding(passage)
        a_embeddings = self.embedding(answer)
        
        timeA = time.time()
        q,_ = self.q_encoder(q_embedding)  ## [32,10,128]
        p,_ = self.p_encoder(p_embedding)  ## [32,100,128]
        # a_embedding, _ = self.a_encoder(a_embeddings.view(-1, a_embeddings.size(2), a_embeddings.size(3)))
        q = torch.cat([q[:,:,:self.encoder_size],torch.flip(q[:,:,self.encoder_size:],[1])],-1)
        p = torch.cat([p[:,:,:self.encoder_size],torch.flip(p[:,:,self.encoder_size:],[1])],-1)
        # q = torch.cat([q[:,:,:],q[:,:,:]],-1)
        # p = torch.cat([p[:,:,:],p[:,:,:]],-1)
        timeB = time.time() - timeA
        # print("MainWpqforward",timeB)

        if self.Hypara['dropout_q_encoder']:
            q=self.dropoutlayer[0](q)
        if self.Hypara['dropout_p_encoder']:
            p=self.dropoutlayer[1](p)

        Vq_z = self.Vq
        q_self_W = self.Wq(q)
        if self.Hypara['dropout_q_W_selfattn']:
            q_self_W = self.dropoutlayer[6](q_self_W)
        f = Vq_z(torch.tanh(q_self_W)).transpose(2,1) # [B,Q,1]
        printf (f.size(),q.size())

        assert self.Hypara['q_only_last']
        rq = torch.softmax(f,-1).bmm(q)  # [B,1,M]
        q = rq
        
        agger = torch.cat( [p,q.expand_as(p)],-1)
        agger,_ = self.MainRnn(agger)
        loss1,predict1 = self.softmaxloss(agger,a_embeddings,label)

        if self.Hypara['dropout_agger']:
            agger = self.dropoutlayer[7](agger)

        loss2,predict2 = self.GraphLoss(agger,a_embeddings,label)

        if is_train == 'eval' or is_train == 'infer':
            return predict1

        # print("loss1",loss1,"loss2",loss2)
        loss = loss1 + loss2
        self.loss = {
            "loss1": loss1,
            "loss2": loss2
        }
        return loss

    def getloss(self):
        return self.loss

    def softmaxloss(self,agger,a_embeddings,label):
        def atten(agger):
            z = self.Wqp(agger)
            if self.Hypara['dropout_agger_W_selfattn']:
                z = self.dropoutlayer[8](z)        
            f = self.Vz(torch.tanh(z)).transpose(2,1)
            r = torch.softmax(f,-1).bmm(agger)  # [B,1,M]
            return r
        r = atten(agger)

        a_embedding, _ = self.a_encoder(a_embeddings.view(-1, a_embeddings.size(2), a_embeddings.size(3)))
        a_embedding = torch.cat([a_embedding[:,:,:self.encoder_size],torch.flip(a_embedding[:,:,self.encoder_size:],[1])],-1)
        a_score = F.softmax(self.a_attention(a_embedding), 1)
        a_output = a_score.transpose(2, 1).bmm(a_embedding).squeeze()
        a_embedding = a_output.view(a_embeddings.size(0), 3, -1)

        printf (a_embedding.size(), r.size())
        r = self.Vr(r)
        predict = a_embedding.bmm( F.leaky_relu(r.transpose(2,1),-1) ).squeeze(-1)  ## model.py用的是leaky_relu
        predict = torch.softmax(predict,-1)                                                        

        gather = predict.gather(-1,label.unsqueeze(-1))
        loss = -torch.log(gather).mean()  ## 只有一个标签，所以之际mean就行。

        predict = predict.argmax(1)
        return loss, predict

    def RNNGraphMerge(self,agger,iter_count=3):
        def atten(agger):
            z = self.Wqp(agger)
            if self.Hypara['dropout_agger_W_selfattn']:
                z = self.dropoutlayer[8](z)        
            f = self.Vz(torch.tanh(z)).transpose(2,1)
            r = torch.softmax(f,-1).bmm(agger)  # [B,1,M]
            return r
        hi=None
        for _ in range(iter_count):
            agger,hi = self.MainRnn(agger,hi)
            if self.Hypara['dropout_agger']:
                agger = self.dropoutlayer[7](agger)
        
        r = atten(agger)
        return r

    def calotherlabel(self,label):
        assert label.dim() == 1
        toreturn = []
        for i in range(label.size(0)):
            if label[i] == 0:
                toreturn.append(torch.tensor([[1],[2]],dtype=label.dtype))
            elif label[i] == 1:
                toreturn.append(torch.tensor([[0],[2]],dtype=label.dtype))
            elif label[i] == 2:
                toreturn.append(torch.tensor([[0],[1]],dtype=label.dtype))
        toreturn = torch.cat(toreturn,0)
        if 'cuda' in label.type():
            toreturn = toreturn.cuda()
        return toreturn

    def GraphLoss(self,agger,a_embeddings,label):
        if self.Hypara['GraphMerge']:
            graph = self.GraphMerge(agger,betas=(0.9,0.5),iter_count=2,activ_fun=self.squash)
        else:
            graph = self.RNNGraphMerge(agger,iter_count=3)
        printf(["grap",graph.size()])
        graph = self.Wgraph(graph)
        assert graph.dim() == 3
        assert a_embeddings.dim() == 4
        hi = None; reconstrlist = []
        # print graph
        for _ in range(a_embeddings.size(2)):
           reconstr,hi = self.reconstr(graph,hi)
           reconstrlist.append(reconstr)
        cosa = a_embeddings.view(a_embeddings.size(0),3,-1) ## 语句合并成一个
        reconstr = torch.cat(reconstrlist,-1).expand_as(cosa)
        printf(["reconstr",reconstr.size()])
        # print reconstr.contiguous()
        # print cosa
        cos = F.cosine_similarity(reconstr,cosa,dim=-1)
        assert cos.dim() ==2 and label.dim() == 1
        # loss = -torch.log(cos.gather(-1,label.unsqueeze(-1))).mean()
        pred = cos.argmax(-1)
        loss = -(cos.gather(-1,label.unsqueeze(-1))).mean() ## 可以计算前先收集，节约微小的计算消耗。
        # print loss
        # exit()
        return loss,pred

    def squash(self, tensor):
            squared_norm = (tensor ** 2).sum(dim=-1, keepdim=True)
            scale = squared_norm / (1 + squared_norm)
            r = scale * tensor / torch.sqrt(squared_norm)
            return r

    def GraphMerge(self,X,betas=(0.9,0.5),iter_count=3,activ_fun=torch.sigmoid):
        """
            先利用rnn提供的注意力结果来测试收敛性。（还是无法解决激活错误的问题?)
            还是不用rnn提供的注意力了吧，GraphMerge的作用就是代替注意力啊.
        """
        assert X.dim() == 3
        func = activ_fun
        iter = iter_count
        init_graph = torch.zeros_like(X[:,0:1,:])
        
        graph = init_graph
        last_graph = graph
        for i in range(iter):
            for ii in range(X.size(1)):
                if ii == 0 and i == 0:
                    graph =  graph+func(X[:,ii:ii+1,:])
                else:
                    graph =  betas[0]*func(graph)+func(X[:,ii:ii+1,:])  ## 利用GRU后处理的有个好处，可能X在此刻不想大量激活，GRU可能会将其值减弱，所以不用学习X的激活系数
            if i != iter-1:
                # graph = graph*betas[1]**i + graph
                graph = last_graph*betas[1]**i + graph
                last_graph = graph
        return graph

        """
            注意 X补零的时候随之时间流逝会指数递减，可能需要处理一下。
            think RNN每一步都紧密联系，而且使用的时候并不是直接使用最后一步，所以是不是太过复杂了？
            回忆一下胶囊网络是否与此类似?
        """






"""
在multiway match之后进行多轮非线性累加合并操作。 output1 = (sigmoid(W)+sigmoid(X1)) output2 = (0.9sigmoid(output1)+sigmoid(X2))
                                           损失直接用余弦相似来计算（注意角度和长度, 长度不重要，不需要计算?) --> 强约束. softmax 不太好，
                                           因为softmax 在输入两个数据交换正负的情况下是无法区分的 ??。（暂时不考虑数据无限情况，数据多准确提高
                                           是一定的吧)

flow:
    embeddings --> share bi-encoder --> q selfattention --> q p cat -->
    aggerGRU   --> GraphLoss (reconstruct answer)


last order:
    |------epoch 17 train error is -0.841886  eclipse 53.75%------| iter: 137011
    detail loss {'loss2': tensor(-0.8512, device='cuda:0', grad_fn=<NegBackward>), 'loss1': tensor(0.0266, device='cuda:0', grad_fn=<NegBackward>)}
    |------epoch 17 train error is -0.829608  eclipse 57.59%------| iter: 137311
    detail loss {'loss2': tensor(-0.8884, device='cuda:0', grad_fn=<NegBackward>), 'loss1': tensor(0.0823, device='cuda:0', grad_fn=<NegBackward>)}
    |------epoch 17 train error is -0.840171  eclipse 61.43%------| iter: 137611
    detail loss {'loss2': tensor(-0.8429, device='cuda:0', grad_fn=<NegBackward>), 'loss1': tensor(0.1201, device='cuda:0', grad_fn=<NegBackward>)}


"""