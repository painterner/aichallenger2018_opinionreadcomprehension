# -*- coding: utf-8 -*-
import argparse
import cPickle
import codecs

import torch
from utils import *
from termcolor import colored

from preprocess import seg_data, transform_data_to_id
from painternerCollection.utils import printonece

parser = argparse.ArgumentParser(description='inference procedure, note you should train the data at first')

parser.add_argument('--data', type=str,
                    default='data/ai_challenger_oqmrc_testa_20180816/ai_challenger_oqmrc_testa.json',
                    help='location of the test data')

parser.add_argument('--word_path', type=str, default='data/word2id.obj',
                    help='location of the word2id.obj')

parser.add_argument('--output', type=str, default='data/prediction.a.txt',
                    help='prediction path')
parser.add_argument('--model', type=str, default='model.pt',
                    help='model path')
parser.add_argument('--batch_size', type=int, default=32, metavar='N',
                    help='batch size')
parser.add_argument('--cuda', action='store_true',
                    help='use CUDA')
parser.add_argument('--patitialcuda', action='store_true',
                    help='use patitial CUDA')

args = parser.parse_args()

with open(args.model, 'rb') as f:
    model = torch.load(f)
if args.cuda:
    model.cuda()
    if hasattr(model,'setcuda'):
        model.setcuda()
# elif args.patitialcuda:
#     model.setcuda()

with open(args.word_path, 'rb') as f:
    word2id = cPickle.load(f)

raw_data = seg_data(args.data,False)
transformed_data = transform_data_to_id(raw_data, word2id)
data = [x + [y[2]] for x, y in zip(transformed_data, raw_data)]
data = sorted(data, key=lambda x: len(x[1]))

data_dic = {}
for x in data:
    candidate_l = len(x[2])
    if str(candidate_l) not in data_dic:
        data_dic[str(candidate_l)] = [] 
    data_dic[str(candidate_l)].append(x)

print 'data_dic size {:d}'.format(len(data_dic))
print 'test data size {:d}'.format(len(data))

for x in data_dic.keys():
    if(x != str(3)):
        print data_dic[x]
        raise Exception("candidate must have 3 length")

data = data_dic[str(3)]

def inference():
    model.eval()
    predictions = []
    for i in range(0, len(data), args.batch_size):
        one = data[i:i + args.batch_size]

        query, _ = padding([x[0] for x in one], max_len=50)
        passage, _ = padding([x[1] for x in one], max_len=300)
        answer = pad_answer([x[2] for x in one])
        # printonece("one","green",one)
        str_words = [x[-1] for x in one]
        ids = [x[3] for x in one]

        # lll = len(query[0])
        # for I in query:
        #     if( len(I) != lll):
        #         print I, len(I), lll
        #         raise Exception(" ")    

        query       =   torch.LongTensor(query)
        passage     =   torch.LongTensor(passage)
        ids__         =   torch.LongTensor(ids)
        try:
            answer      =   torch.LongTensor(answer) 
        except:
            printonece("infer_query","blue",query)
            printonece("infer_errorpoint","blue",answer,onece=False)
            printonece("infer_query_id","blue",ids,onece=False)


        pos = torch.zeros(1)## 和训练时参数保持一致。 faker 参数
        if args.cuda:
            query = query.cuda()
            passage = passage.cuda()
            answer = answer.cuda()
            ids__ = ids__.cuda()
            pos = pos.cuda() 
        optimizer = None  ## 和训练时参数保持一致。
        
        output = model([query, passage, answer,ids__,pos, False],optimizer)
        for q_id, prediction, candidates in zip(ids, output, str_words):
            prediction_answer = u''.join(candidates[prediction])
            predictions.append(str(q_id) + '\t' + prediction_answer)
    outputs = u'\n'.join(predictions)
    with codecs.open(args.output, 'w',encoding='utf-8') as f:
        f.write(outputs)
    print 'done!'


if __name__ == '__main__':
    inference()
