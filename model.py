# -*- coding: utf-8 -*-
import torch
from torch import nn
from torch.nn import functional as F
from termcolor import colored
import logging
from painternerCollection.utils import printonece
logger = logging.getLogger(__name__)
logging.basicConfig(level = logging.DEBUG,
                    format = '%(asctime)s[%(levelname)s] ---- %(message)s',
                    )


class MwAN(nn.Module):
    def __init__(self, vocab_size, embedding_size, encoder_size, drop_out=0.2):
        super(MwAN, self).__init__()

        self.attentionWithAdd = True
        self.qpRNN = True
        self.answertoalternatives = False  ## if True , need to change preprocess.py (seg_data process_data function)
        printonece("customered patameters","red","")
        logging.info(self.attentionWithAdd)
        logging.info(self.qpRNN)
        logging.info(self.answertoalternatives)


        self.drop_out=drop_out
        self.embedding = nn.Embedding(vocab_size + 1, embedding_dim=embedding_size)
        if(self.qpRNN):
            self.qp_encoder = nn.GRU(input_size=embedding_size, hidden_size=encoder_size, batch_first=True,
                                bidirectional=True)
        self.q_encoder = nn.GRU(input_size=embedding_size, hidden_size=encoder_size, batch_first=True,
                                bidirectional=True)
        self.p_encoder = nn.GRU(input_size=embedding_size, hidden_size=encoder_size, batch_first=True,
                                bidirectional=True)
        self.a_encoder = nn.GRU(input_size=embedding_size, hidden_size=embedding_size / 2, batch_first=True,
                                bidirectional=True)
        self.a_attention = nn.Linear(embedding_size, 1, bias=False)
        # Concat Attention
        self.Wc1 = nn.Linear(2 * encoder_size, encoder_size, bias=False)
        self.Wc2 = nn.Linear(2 * encoder_size, encoder_size, bias=False)
        self.vc = nn.Linear(encoder_size, 1, bias=False)
        # Bilinear Attention
        self.Wb = nn.Linear(2 * encoder_size, 2 * encoder_size, bias=False)
        # Dot Attention :
        self.Wd = nn.Linear(2 * encoder_size, encoder_size, bias=False)
        self.vd = nn.Linear(encoder_size, 1, bias=False)
        # Minus Attention :
        self.Wm = nn.Linear(2 * encoder_size, encoder_size, bias=False)
        self.vm = nn.Linear(encoder_size, 1, bias=False)
        # Add Attention :
        if(self.attentionWithAdd):
            self.Wadd = nn.Linear(2 * encoder_size, encoder_size, bias=False)
            self.vadd = nn.Linear(encoder_size, 1, bias=False)
        if(self.qpRNN):
            self.Wqp = nn.Linear(2 * encoder_size,encoder_size,bias=False)
            self.vqp = nn.Linear(encoder_size,1,bias=False)
            self.predt_qp       = nn.Linear(2*encoder_size,2*encoder_size,bias=False)
            self.predt_aggre    = nn.Linear(2*encoder_size,2*encoder_size,bias=False)

        self.Ws = nn.Linear(2 * encoder_size, encoder_size, bias=False)
        self.vs = nn.Linear(encoder_size, 1, bias=False)

        if(self.attentionWithAdd):
            factor = 14  
        else:
            factor = 12

        self.gru_agg = nn.GRU(factor * encoder_size, encoder_size, batch_first=True, bidirectional=True)

        """
        prediction layer
        """
        self.Wq = nn.Linear(2 * encoder_size, encoder_size, bias=False)
        self.vq = nn.Linear(encoder_size, 1, bias=False)
        self.Wp1 = nn.Linear(2 * encoder_size, encoder_size, bias=False)
        self.Wp2 = nn.Linear(2 * encoder_size, encoder_size, bias=False)
        self.vp = nn.Linear(encoder_size, 1, bias=False)
        self.prediction = nn.Linear(2 * encoder_size, embedding_size, bias=False)


        self.Wtest1  = nn.Linear(10,1)
        self.initiation()

    def initiation(self):
        initrange = 0.1
        nn.init.uniform_(self.embedding.weight, -initrange, initrange)
        for module in self.modules():
            if isinstance(module, nn.Linear):
                nn.init.xavier_uniform_(module.weight, 0.1)
                
    def forward(self, inputs,optimizer):
        [query, passage, answer,ids,pos is_train] = inputs
        printonece("query","green",query.size())                ## inference will print it,because of having been include into model.pt ?
        printonece("passage","green",passage.size())
        printonece("answer","green",answer.size())
        printonece("ids","green",ids.size())
        q_embedding = self.embedding(query)
        p_embedding = self.embedding(passage)
        a_embeddings = self.embedding(answer)
        printonece("q_ems size","red",q_embedding.size());printonece("p_em size","red",p_embedding.size());printonece("a_em size","red",a_embeddings.size());

        a_embedding, _ = self.a_encoder(a_embeddings.view(-1, a_embeddings.size(2), a_embeddings.size(3)))
        printonece("a_en","green",a_embedding.size())
        a_score = F.softmax(self.a_attention(a_embedding), 1)
        printonece("a_score","green",a_score.size())
        a_output = a_score.transpose(2, 1).bmm(a_embedding).squeeze()
        printonece("a_output","green",a_output.size())
        if(is_train and self.answertoalternatives):
            a_embedding = a_output.view(a_embeddings.size(0), 4, -1)
        else:
            a_embedding = a_output.view(a_embeddings.size(0), 3, -1)
        printonece("a_em","green",a_embedding.size())

        hq, _ = self.q_encoder(p_embedding)    ## p,q 交叉，但是如果是代码写错了，那么下面的部分操作就有可能颠倒。
        hq=F.dropout(hq,self.drop_out)
        hp, _ = self.p_encoder(q_embedding)
        hp=F.dropout(hp,self.drop_out)
        printonece("hq","blue",hq.size()); printonece("hp","blue",hp.size()); 
        if(self.qpRNN):
            hqp,_ = self.qp_encoder(torch.cat((q_embedding,p_embedding),-2))
            hqp = F.dropout(hqp,self.drop_out)


        # hq, _ = self.q_encoder(q_embedding)    ## p,q 交叉，但是如果是代码写错了，那么下面的部分操作就有可能颠倒。
        # hq=F.dropout(hq,self.drop_out)
        # hp, _ = self.p_encoder(p_embedding)
        # hp=F.dropout(hp,self.drop_out)
        
        _s1 = self.Wc1(hq).unsqueeze(1)
        _s2 = self.Wc2(hp).unsqueeze(2)
        printonece("v1_s1","red",_s1.size()); printonece("v1_s2","red",_s2.size());
        sjt = self.vc(torch.tanh(_s1 + _s2))
        sjt = sjt.squeeze()
        ait = F.softmax(sjt, 2)                 
        qtc = ait.bmm(hq)
        printonece("qtc","red",qtc.size())

        _s1 = self.Wb(hq).transpose(2, 1)
        sjt = hp.bmm(_s1)
        printonece("sjt","green",sjt.size())
        ait = F.softmax(sjt, 2)
        qtb = ait.bmm(hq)
        printonece("qtb","green",qtb.size())

        _s1 = hq.unsqueeze(1)
        _s2 = hp.unsqueeze(2)
        printonece("v2_s1","blue",_s1.size()); printonece("v2_s2","blue",_s2.size());
        sjt = self.vd(torch.tanh(self.Wd(_s1 * _s2))).squeeze()   ## encoder_size *2 -->  encoder_size
        printonece("_s1 * _s2","blue",(_s1*_s2).size())
        ait = F.softmax(sjt, 2)
        qtd = ait.bmm(hq)
        printonece("qtd","blue",qtd.size())

        sjt = self.vm(torch.tanh(self.Wm(_s1 - _s2))).squeeze() ## encoder_size *2 -->  encoder_size
        printonece("_s1 - _s2","red",(_s1 - _s2).size())
        ait = F.softmax(sjt, 2)
        qtm = ait.bmm(hq)
        printonece("qtm","red",qtm.size())

        if(self.attentionWithAdd):
            sjt = self.vadd(torch.tanh(self.Wm(_s1 + _s2))).squeeze()
            printonece("_s1 + _s2","blue",(_s1 + _s2).size())
            ait = F.softmax(sjt,2)
            qtadd = ait.bmm(hq)
            printonece("qtadd","blue",qtadd.size())
        
        if(self.qpRNN):
            sjt = self.vqp(torch.tanh(self.Wqp(hqp)))
            ait = F.softmax(sjt,1).transpose(2,1)
            qtqp = ait.bmm(hqp)
            printonece("qtap","red",qtqp.size())

        ## 如果交换q,p这里会卡住，hp*hp太大。若passage 300长度，300*300*128*32*4 > 1.47G. 
        ##                               若query  10长度,  1.47/30 <50M还可以接受.
        ##                 如果维数增多虽然与Linear相乘(如self.W(s))的运算相当，但只Linear是不需要这么多内存的。
        _s1 = hp.unsqueeze(1)
        _s2 = hp.unsqueeze(2)
        sjt = self.vs(torch.tanh(self.Ws(_s1 * _s2))).squeeze() ## encoder_size *2 -->  encoder_size
        ait = F.softmax(sjt, 2)
        qts = ait.bmm(hp)
        printonece("qts","green",qts.size())


        ## 如果cat 5个 128*128*5*4*2 =》 640K左右 应该问题不大。
        if(self.attentionWithAdd):
            toConcatenate = [hp, qts, qtc, qtd, qtb, qtm,qtadd]
        else:
            toConcatenate = [hp, qts, qtc, qtd, qtb, qtm]

        aggregation = torch.cat(toConcatenate, 2)

        printonece("aggregation","blue",aggregation.size())
        aggregation_representation, _ = self.gru_agg(aggregation)
        printonece("aggregation_representation","blue",aggregation_representation.size())

        sj = self.vq(torch.tanh(self.Wq(hq))).transpose(2, 1)
        rq = F.softmax(sj, 2).bmm(hq)
        printonece("rq","red",rq.size())
        sj = F.softmax(self.vp(self.Wp1(aggregation_representation) + self.Wp2(rq)).transpose(2, 1), 2) #encoder_size --> 1
        printonece("sj_rq","red",sj.size())
        rp = sj.bmm(aggregation_representation)
        printonece("rp","red",rp.size())
        
        ## rp shape [32, n, 128] --> n 为question 的长度比较合理，因为要回答的内容是与提问的长度相当的。
        if(self.qpRNN):
            rp = self.predt_qp(qtqp) + self.predt_aggre(rp)    

        encoder_output = F.dropout(F.leaky_relu(self.prediction(rp)),self.drop_out)
        printonece("model_encoder_output","green",encoder_output.size())
        score = F.softmax(a_embedding.bmm(encoder_output.transpose(2, 1)).squeeze(), 1)
        printonece("model_score","green",score.size())

        if not is_train:
            return score.argmax(1)
        loss = -torch.log(score[:,0]).mean()
        printonece("loss","blue",loss.size())
        exit()
        return 


