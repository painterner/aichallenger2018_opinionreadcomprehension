
```python
""" 
code:
  tokenization.py:

    for token in self.basic_tokenizer.tokenize(text):
      for sub_token in self.wordpiece_tokenizer.tokenize(token):
        split_tokens.append(sub_token)

    这里的Fulltokenize 的tokenize分成两步：
        1. basic_tokenizer
            阅读后大概理解: 非chinese的字符连接到一块，chinese字符用空格分开，而且对英语进行lower case转换
        2. wordpice_tokenizer
            策略：
                当未知词（非chinese)的长度超过一定限制的时候标记[UNK]
                当搜索算法为发现时标记[UNK],搜索案例:
                    unable --> ['un','##unable"] , 只从前向后搜索可能是因为英文词语的特点为前缀+词语式的构词法?
                    从这里可以推测，chinese vocabulary中可能存在很多英文字符(假设每一100个chinese中有一个English-->1%,也够多了)
            是否传入的text除了非chinese字符都是单个子？ 验证一下

  run_opinion.py
    def get_train_examples(self, data_dir):
        """Gets a collection of `InputExample`s for the train set."""
        raise NotImplementedError()
    提供这个类的目的之一:
        后面代码一定调用了这个类的此方法，即要求不同子类要实例化相同的函数，实现代码的统一(不再需要调用不同的函数），和功能的分离(只需在类中修改即可)。

"""

"""
Note:
    bert 是字符级别的。 
--> from scratch get pre-trained data
    how to load bert chinese model, 此时不仅仅是嵌入层，所以要准备好同等的模型来加载。
    because of the TensorFlow checkpoint, so we will use tensorflow
"""

"""
To do:
    https://github.com/google-research/bert/blob/master/multilingual.md
    The Multilingual model does include Chinese (and English) --> 
        正确性比单一Chinese要低,不过它的原理值得学习，可能会得到某些意外的收获。

    11.27
        测试chinese model上载以及上载后的数据内容。
        1. 修改run_classifier input_examples 函数.
        2. 修改model_fn 中的损失函数。
        12.7:
            1. random.shuffle(alternatives)   测试验证 (与以前训练中每次都shuffle不同；现在只在准备阶段统一shuffle，训练中不改变)
            2. handle  get_labels() and label_map
"""


"""
docker run -it --runtime=nvidia -v /mnt/u16/home/ka/Documents:/mnt/u16/home/ka/Documents --user ka -w $PWD -p 8889:8888 painterner/tensorflow:01-gpu-py2 bash  ## To do: prepare gpu py2 version 

export BERT_BASE_DIR=/mnt/u16/home/ka/Documents/proj/bert/data/chinese_L-12_H-768_A-12
export OPINION_DIR=../data

python run_opinion.py \
  --task_name=OPINION \
  --do_train=true \
  --do_eval=true \
  --data_dir=$OPINION_DIR \
  --vocab_file=$BERT_BASE_DIR/vocab.txt \
  --bert_config_file=$BERT_BASE_DIR/bert_config.json \
  --init_checkpoint=$BERT_BASE_DIR/bert_model.ckpt \
  --train_batch_size=32 \
  --learning_rate=2e-5 \
  --num_train_epochs=3.0 \
  --partial_dataset=1.0 \
  --partial_train=head \
  --output_dir=/tmp/opinion_output/
"""

"""
noting:
    Haven't handled the do_predict
    num_train_epochs has been jointed to all steps --> num_train_steps (it is convenient to write code)
    there are only once eval, i.e. after the training (what's the function of the iterations_per_loop)
test this two cases:
    1. token_c joint to token_a and token_b
    2. token_c separate from token_a and token_b, this case should works more efficient ?
problem:
    how to joint two 128 token to one label ? run model twice (use first's output as second's inputs)??

Todo:
    how to train a deep net and have no gradient explosion or disappear, there are some hypthesis
            1. (LayerNormal will do this ??)
            2.  clip_by_global_norm (in optimization.py)
            3. num_warmup_steps = int(num_train_steps * FLAGS.warmup_proportion)

    If fixed the pre_model, we can train it in my PC ? (CPU is ok.)
    How to not train the premodel ?
    attention is multiattention ? (because I can't see [768,1]weights)
    bidi dictionary ?
    analyse net structure
    code test:
        position_embeddings = tf.slice(full_position_embeddings, [0, 0],
                                     [seq_length, -1])
"""
"""
In [1]: import tensorflow as tf                                                 
In [2]: a=tf.Variable(tf.random_normal([2,2]))                                  
In [3]: b=tf.Variable(tf.random_normal([2,2]))                                  
In [4]: c=tf.constant([[1,2.],[2,3]])                                           
In [5]: d=c@a                                                                   
In [6]: e=d@b                                                                   
In [7]: f=tf.reduce_sum(e)                                                      
In [8]: optimier = tf.train.AdamOptimizer(0.1).minimizer(f)                     
In [9]: optimier = tf.train.AdamOptimizer(0.1).minimize(f)                      
In [10]: tf.global_variables()                                                  
Out[10]: 
[<tf.Variable 'Variable:0' shape=(2, 2) dtype=float32_ref>,
 <tf.Variable 'Variable_1:0' shape=(2, 2) dtype=float32_ref>,
 <tf.Variable 'beta1_power:0' shape=() dtype=float32_ref>,
 <tf.Variable 'beta2_power:0' shape=() dtype=float32_ref>,
 <tf.Variable 'Variable/Adam:0' shape=(2, 2) dtype=float32_ref>,
 <tf.Variable 'Variable/Adam_1:0' shape=(2, 2) dtype=float32_ref>,
 <tf.Variable 'Variable_1/Adam:0' shape=(2, 2) dtype=float32_ref>,
 <tf.Variable 'Variable_1/Adam_1:0' shape=(2, 2) dtype=float32_ref>]
## we can't see the jocobo matrix ,but it did exist ??

                              
INFO:tensorflow:  name = bert/encoder/layer_0/attention/self/query/kernel:0, shape = (768, 768), *INIT_FROM_CKPT*
INFO:tensorflow:  name = bert/encoder/layer_0/attention/self/query/bias:0, shape = (768,), *INIT_FROM_CKPT*
INFO:tensorflow:  name = bert/encoder/layer_0/attention/self/key/kernel:0, shape = (768, 768), *INIT_FROM_CKPT*
INFO:tensorflow:  name = bert/encoder/layer_0/attention/self/key/bias:0, shape = (768,), *INIT_FROM_CKPT*
INFO:tensorflow:  name = bert/encoder/layer_0/attention/self/value/kernel:0, shape = (768, 768), *INIT_FROM_CKPT*
INFO:tensorflow:  name = bert/encoder/layer_0/attention/self/value/bias:0, shape = (768,), *INIT_FROM_CKPT*
INFO:tensorflow:  name = bert/encoder/layer_0/attention/output/dense/kernel:0, shape = (768, 768), *INIT_FROM_CKPT*
INFO:tensorflow:  name = bert/encoder/layer_0/attention/output/dense/bias:0, shape = (768,), *INIT_FROM_CKPT*
INFO:tensorflow:  name = bert/encoder/layer_0/attention/output/LayerNorm/beta:0, shape = (768,), *INIT_FROM_CKPT*
INFO:tensorflow:  name = bert/encoder/layer_0/attention/output/LayerNorm/gamma:0, shape = (768,), *INIT_FROM_CKPT*
INFO:tensorflow:  name = bert/encoder/layer_0/intermediate/dense/kernel:0, shape = (768, 3072), *INIT_FROM_CKPT*
INFO:tensorflow:  name = bert/encoder/layer_0/intermediate/dense/bias:0, shape = (3072,), *INIT_FROM_CKPT*
INFO:tensorflow:  name = bert/encoder/layer_0/output/dense/kernel:0, shape = (3072, 768), *INIT_FROM_CKPT*
INFO:tensorflow:  name = bert/encoder/layer_0/output/dense/bias:0, shape = (768,), *INIT_FROM_CKPT*
INFO:tensorflow:  name = bert/encoder/layer_0/output/LayerNorm/beta:0, shape = (768,), *INIT_FROM_CKPT*
INFO:tensorflow:  name = bert/encoder/layer_0/output/LayerNorm/gamma:0, shape = (768,), *INIT_FROM_CKPT*
analyse:
                                            non jacob out       jacob out max?          adam (variable will x2)
Input: I = 32*384
layer_n/xxx       768 x 768 x4 +          m: I*768*14+          1. 768*3072* I*3072
                  768 x 3072 x1 +            I*3072*2? + 7M         = I*6.72G
                  3072 x 768 x1 +             =205M             2. I*384*768  *768         
                  768 x 9 +                                         = 7G
                  3072 x1 = 7M                                 
layer x 12 ->  7M x12                     m: x12                1. = I*6.72G
                                             = 2.5G             2. = 7G
               
"""
