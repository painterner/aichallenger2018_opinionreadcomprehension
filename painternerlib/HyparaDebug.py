class Hypara():
    def __init__(self):
        self.dict = {}
    def __getattribute__(self,x):
        raise Exception("Can't be implemented")

    def __getitem__(self,x):
        raise Exception("Can't be implemented")


__hypara = Hypara()
def fgetHypara():
    return __hypara.dict

def fputHypara(dict_,check=True):
    """
        put Hypara dictionary
        you may need to check When your code is not in single file
    """
    for v in dict_:
        if check:
            assert v not in __hypara.dict
        __hypara.dict[v] = dict_[v]
    return __hypara.dict


__hdebug = Hypara()
def fgetHDebug():
    return __hdebug.dict

def fputHDebug(dict_,check=True):
    """
        put Debug dictionary
        you may need to check When your code is not in single file
    """
    for v in dict_:
        if check:
            assert v not in __hdebug.dict
        __hdebug.dict[v] = dict_[v]
    return __hdebug.dict