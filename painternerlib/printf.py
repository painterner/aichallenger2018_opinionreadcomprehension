# -*- coding: utf-8 -*-
from termcolor import colored
# error SyntaxError: from __future__ imports must occur at the beginning of the file
# from __future__ import print_function ## so we can multiprint use print,such as print(colored(xx),colored(xx))
import json

__all__ = ["Print","printonece","printf"
    ]

class Print():
    def __init__(self):
        self.action = True
        self.test = True
        self.levels = {}
    @property
    def testflag(self):
        return self.test
    def cancel(self):
        self.action = False
    def open(self):
        self.action = True
    def empty(self):
        pass
    def get_current_levels(self):
        return self.levels
    def set_levels(self,v):
        """
        example -> levels_dict: {0:True,1:False,2:False}
        """
        assert isinstance(v,(tuple,list)) and len(v) == 3
        for i, vv in enumerate(v):
            assert isinstance(vv,bool)
            self.levels[i] = vv
    def exit(self):
        if self.action:
            exit()
    def __call__(self,*inputs,**kargs):
        if 'color' in kargs:
            assert type(kargs['color']) is str
            print(colored(inputs[0],kargs['color']))
        else:
            print(inputs[0])

__printf = Print()
__printf.set_levels([True,False,False])
def printf(*v,**kargs):
    """use: printf(str)
            printf(str,color='red')
            printf(str,onece=True,name='id1') ## color is blue
    """
    if not __printf.action:
        return
    if 'level' in kargs:
        if not __printf.get_current_levels()[kargs['level']]:
            return
    else:
        ## default level is 0
        if not __printf.levels[0]:
            return

    if 'onece' in kargs and  kargs['onece'] == True and 'name' in kargs and kargs['name'] != None:
        printonece(kargs['name'],'blue',json.dumps(v))
        return
    if 'color' in kargs:
        color = kargs['color']
        for x in v:
            __printf(x,color=color) ## x is a tuple
    else:
        for x in v:
            __printf(x) ## x is a tuple
def getPrintf():
    return __printf



__onecedict={}  
__onecesavefile={}
__onecesavefile1={}
__vonece1freq = 50
global __vonece1count
__vonece1count = -1

def printonece(string,color,target,target2=None,onece=True,Exit=False,**kargs):
    if(onece):
        if(string not in __onecedict):
            __onecedict[string] = 0
        else:
            return
    content = target
    print(colored(string,color), content)

    if "savepath" in kargs:
        if (kargs['savepath'] != True):
            assert type(kargs['savepath']) is str
            if ("savepath" not in __onecesavefile):
                __onecesavefile['savepath'] = kargs['savepath']
                flag = 'w'
            else:
                flag = 'a'
        else:
            flag = 'a'

        assert len(__onecesavefile) is 1

        if(target2 is None):   ## error if use target2 == None (判断某些可用==,但是某些需要用is，可能与自定义操作符有关))
            target2 = content

        with open(__onecesavefile['savepath'],flag) as f: ## you can pass savepath=path or savepath=True
            f.writelines(
                string+'\t'+
                str(target2.size())+'\n' +
                str(target2)+'\n'
                )
            

    if(Exit == True):
        exit()

def clearsome():
    global __vonece1count
    __vonece1count = -1

def printonece1(string,color,target,target2=None,onece=True,Exit=False,**kargs):
    if "savepath" in kargs:
        if (kargs['savepath'] != True):
            assert type(kargs['savepath']) is str
            __onecesavefile1['savepath'] = kargs['savepath']

            global __vonece1count
            __vonece1count += 1
            flag = 'w'
        else:
            flag = 'a'

        if(target2 is None):   ## error if use target2 == None (判断某些可用==,但是某些需要用is，可能与自定义操作符有关))
            target2 = target

        if(__vonece1count % __vonece1freq == 0):
            with open(__onecesavefile1['savepath'],flag) as f: ## you can pass savepath=path or savepath=True
                f.writelines(
                    string+'\t'+
                    str(target2.size())+'\n' +
                    str(target2)+'\n'
                    )

    
    if(onece):
        if(string not in __onecedict):
            __onecedict[string] = 0
        else:
            return
    print(colored(string,color), target)

