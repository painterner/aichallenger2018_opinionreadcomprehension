#!-*-coding:utf-8-*-
import time
def l2_sum(names,models):  ## 比较以前的word判断方向，看看是否有pitfall
    E = 0.
    # print(names,models)
    if not isinstance(models,tuple) and not isinstance(models,list):
        models = [models]
    if type(names) is str:
        names = [names]
    for m in models:
        for v,p in m.named_parameters():
            for n in names:
                if v == n:
                    E = E + (p**2).sum()

    return E

def invoke_and_time(function,*args):
    timeA =time.time()
    function(*args)
    print time.time() - timeA