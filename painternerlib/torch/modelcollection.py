#!-*-coding-utf-8-*-
import torch
import torch.nn as nn

class selfattention(nn.Module):
    def __init__(self,indim,middim):
        super(selfattention,self).__init__()
        self.indim = indim
        self.middim = middim
        self.model =  nn.Sequential(
            nn.Linear(self.indim,self.middim),
            nn.Tanh(),
            nn.Linear(self.middim,1),
            nn.Softmax(1)
        )
    def forward(self,inputs):
        return self.model(inputs)