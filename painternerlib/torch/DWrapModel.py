#!-*-coding:utf-8-*-
import torch
import torch.nn as nn
class DWrapModel():
    def __init__(self,vcuda=True,autocuda=True):
        self.firstrun = True
        self.vcuda = vcuda
        self.autocuda = autocuda
    def setcuda(self):
        self.model.cuda()
    def clrcuda(self):
        self.model.cpu()
    def __call__(self,model,inputs,*arg,**kargs):
        """ inputs must be explicit
        """
        if self.firstrun:
            self.firstrun = False
            if arg == [] and kargs != {}:    
                self.model = model(**kargs)
            elif arg != [] and kargs =={}:
                self.model = model(*arg)
            elif arg != [] and kargs != {}:
                self.model = model(*arg,**kargs)
            else:
                self.model = model()

            for param in self.model.parameters():
                param.data.normal_()
            if self.autocuda:         
                if torch.cuda.is_available():
                    self.setcuda()
            else:
                if self.vcuda:
                    self.setcuda()

        if inputs is None:
            return self.model()
        else:
            return self.model(inputs)

class A():
    def __init__(self):
        self.b=2  

# __ext_string = 'DySp278_M_00'
# def fWrapModel(context,model,*args,**kargs):
#     print hasattr(context,__ext_string)
#     context.__ext_string = 1
#     print(context.__ext_string)
#     pass

__ext_name={}
def fWrapModel(name,model,inputs,*args,**kargs):
    """
        需要外部保证name是唯一的。
    """
    if name not in __ext_name:
        __ext_name[name] = DWrapModel()
        return __ext_name[name](model,inputs,*args,**kargs)
    else:
        return __ext_name[name](model,inputs,*args,**kargs)
    

class __test():
    def __init__(self):
        self.Linear1 = DWrapModel(False,True)

        self.inputs = torch.Tensor(2,3)
        self.inputs.data.normal_()
        self.inputs = self.inputs.cuda()

    def run(self):
        for i in range(2):
            inputs = self.inputs
            linear1 = self.Linear1(nn.Linear,inputs,3,4)
            print (linear1)

if __name__ == "__main__":
    __test().run()
