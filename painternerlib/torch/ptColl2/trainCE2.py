#-*-coding:utf-8-*-
import torch
from torch import nn
import sys,os
print sys.path[0]
sys.path.remove(sys.path[0])
sys.path.append(os.getcwd())   ## then you can import preprocess in fanterfolder


from ptColl2.curvenet import Net
from ptColl2.curve2m import fload_with_evall
from ptColl2.curve2m import getplot
import numpy as np
import random
import argparse
from matplotlib import pyplot as plt
from termcolor import colored
import shutil
from ptColl2.swats import Swats

parser=argparse.ArgumentParser(description='option')
parser.add_argument('--cuda', action='store_true',
                    help='use CUDA')
parser.add_argument('--infer', action='store_true',
                    help='use CUDA')
parser.add_argument('--restore',action='store_true',
                    help='restore to train')
parser.add_argument('--optimizer',action='store_true',
                    help='restore to train')

args = parser.parse_args()

modelpath='model/curve/model.pt'
optimizerpath='model/curve/optimizer.opt'

sdata = fload_with_evall()
data = np.concatenate(sdata[0:3],0)
evall = np.array(sdata[3])
print "evall length",len(evall)
print "data length",len(data)
__ceprobA,__ceprobB,__ceprobC = len(sdata[0])*1.0/len(data), len(sdata[1])*1.0/len(data),len(sdata[2])*1.0/len(data)
vg_ceprob = (__ceprobA,__ceprobB,__ceprobC)

if args.infer or args.restore:
    print colored('restore model from point\t\t'+modelpath,'red')
    with open(modelpath,'rb') as f:
        model = torch.load(f)
else:
    model = Net(2,3)

if args.cuda:
    model.cuda()

if args.optimizer:
    print colored('restore optimizer from point\t\t'+optimizerpath,'red')
    with open(optimizerpath,'rb') as f:
        optimizer__ = torch.load(f)
        # print optimizer__.state_dict()['param_groups']
        optimizer = torch.optim.Adamax(model.parameters(),lr=2e-3)
        optimizer.load_state_dict(optimizer__.state_dict())
        print optimizer.state_dict()['param_groups']       ## add_param_group 无法成功，改用load_state_dict
                                                           ## 只恢复state_dict，会把一些mt-1,vt-1等状态都恢复吗。
        # exit()
else:
    # optimizer = torch.optim.Adamax(model.parameters(),lr=2e-3)
    # optimizer = torch.optim.SGD(model.parameters(),lr=0.0002)
    optimizer = Swats(model.parameters(),lr=0.0002)

def fLrPolicy(epoch):
    if epoch==0:   ## value of epoch will change after scheduler was invoked,after first step() is 0
        return 1
    else:
        return 0.01
    
lambda1 = lambda x: fLrPolicy(x)
scheduler = torch.optim.lr_scheduler.LambdaLR(optimizer,lr_lambda=[lambda1])


class CeParameter():
    def __init__(self,model,optimizer):
        self.f = open('data/cetemp.tmp','rb+') 
        self.f.seek(0)
        # self.model = model
        # self.optimizer = optimizer
        self.modelstate_dict =None
        self.optimizerstate_dict = None
    def fsake_save(self,buffer):
        torch.save(buffer,self.f)
        self.f.seek(0)

    def fsake_load(self):
        buffer = torch.load(self.f)
        self.f.seek(0)
        return buffer

    def save(self,model,optimizer):
        self.modelstate_dict =  model.state_dict()
        self.optimizerstate_dict = optimizer.state_dict()  ## mt,vt的状态也包含在里面了 ？
        buffer = [self.modelstate_dict,self.optimizerstate_dict]
        self.fsake_save(buffer)
        ## or we can store as method：
        ## for i in model.state_dict().keys():  torch.tensor( model.state_dict(i) ) --> saveedList

    def restore(self,model,optimizer):
        buffer = self.fsake_load()
        model.load_state_dict(buffer[0])
        # optimizer.load_state_dict(buffer[1])

cePara = CeParameter(model,optimizer)

class Main():
    def __init__(self):
        self.cerestorecount = -1
        self.cerestorecount__ = -1
    def train(self,epoch,ce=False):
        batch_size=10
        interval=300

        __acc,_,_ = self.test(0,evall,True)
        total_loss = 0
        all_loss = 0
        after_loss = 0
        trainacc = 0
        np.random.shuffle(data)
        for num, i in enumerate(range(0, len(data), batch_size)):
            inputs = np.array(data[i:i+batch_size,:2],dtype=np.float32)
            label =np.array( np.expand_dims(data[i:i+batch_size,2],1),dtype=np.int64)
            # getplot(inputs,label)

            inputs,label = torch.Tensor(inputs), torch.LongTensor(label)
            if args.cuda:
                inputs = inputs.cuda()
                label = label.cuda()
            
            ceprob = None
            if(ce):
                cePara.save(model,optimizer)

                ceEvaldata = []
                for i in range(20):
                    randint = np.random.randint(0,len(data)-2*batch_size)
                    ceEvaldata.append(data[randint:randint+batch_size])
                ceEvaldata = np.concatenate(ceEvaldata)
                # acc1,_,_ = self.test(0,ceEvaldata,True)

                __ceprobA,__ceprobB,__ceprobC = vg_ceprob
                ceprob=torch.tensor([[__ceprobA,__ceprobB,__ceprobC]]*label.size()[0])
            optimizer.zero_grad()
            loss,after_loss,predict=model([inputs,label,True],ceprob)
            if ce:
                after_loss.backward()
            else:
                loss.backward()
            total_loss += loss.item()
            all_loss += loss.item()
            after_loss += after_loss.item()
            if 1:
                acccount = torch.eq(predict.long(),label.squeeze(1)).sum().item()
                trainacc += acccount*1.0/batch_size
            optimizer.step()

            if(ce):
                # 随机取10*batch_szie的进行验证。(不用考虑自身的排除，因为概率可以忽略)
                acc2,_,_=self.test(0,ceEvaldata,True)
                # cePara.restore(model,optimizer)
                alpha=100
                beta = 0
                if ((acc2 < alpha) and (acc2 < __acc+beta)):  ## 为什么改成<就会出现爆炸或消失情况？
                    cePara.restore(model,optimizer)
                    self.cerestorecount += 1
                    self.cerestorecount__ += 1
                    # acc3,_,_ = self.test(0,ceEvaldata,True)
                    # print acc1,acc2
                    pass

            # ce 的一些打印是有误差的，因为会有ceresotre的情况。
            if (num+1)%interval == 0:
                print "epoch: {},loss: {}, allloss:{},train_acc,{},afterloss:{} eclipse: {}".format(epoch,
                        total_loss/interval,all_loss/(num+1),trainacc*100.0/interval,after_loss/interval, i*100.0/len(data))
                total_loss = 0
                after_loss = 0
                trainacc=0
        print colored('cecountsingle','green'),self.cerestorecount__
        self.cerestorecount__ = -1
        # plt.show() #查看数据是否正常.
            
    def test(self,epoch,data,noprint=False):
        batch_size=100
        interval=300

        a=0
        devacc = 0
        with torch.no_grad():
            m_inputs = []
            m_predict = []
            for num, i in enumerate(range(0, len(data), batch_size)):
                inputs_ = np.array(data[i:i+batch_size,:2],dtype=np.float32)
                label =np.array( np.expand_dims(data[i:i+batch_size,2],1),dtype=np.int64)

                inputs,label = torch.Tensor(inputs_), torch.LongTensor(label)
                if args.cuda:
                    inputs = inputs.cuda()
                    label = label.cuda()

                predict=model([inputs,label,False])
                acccount = torch.eq(predict.long(),label.squeeze(1)).sum().item()
                devacc += acccount
                a += batch_size

                m_inputs.append(inputs)
                m_predict.append(predict.unsqueeze(1))
        
            acc = devacc*100.0/a
            if not noprint:
                print "epoch {},acc {}".format(epoch,acc)
        m_inputs = torch.cat(m_inputs)
        m_predict = torch.cat(m_predict)
        return acc,np.array(m_inputs), np.array(m_predict)

    def main(self):
        epoch  = 200
        best = 0

        vpreparePicture = True
        sce_normal='normal'
        scheduler.step() 
        print "scheduler lr", optimizer.state_dict()['param_groups'][0]['lr']
        if args.infer:
            acc,datas,label = self.test(0,evall)
            getplot(datas,label)
            plt.show()
            return
        if args.restore:
            startTrainpoint = 10
        else:
            startTrainpoint = 50
        cetrainepoch = 0

        for i in range(epoch):
            # self.train(i,True)
            if i >= startTrainpoint:
                # exit()
                if i>=startTrainpoint+cetrainepoch:
                    return
                scheduler.step()
                print "scheduler lr", optimizer.state_dict()['param_groups'][0]['lr']
                self.train(i,ce=True)
                print colored('cerstorecount','blue'),self.cerestorecount ## 由于随机输入10个batch_size，所以
                                                                ## 如果就算移除optimizer.step,每次结果也是不确定的。
                sce_normal='ce'
            else:
                self.train(i)

            acc,datas,label = self.test(0,data)
            getplot(datas,label)

            if vpreparePicture == True:
                vpreparePicture=False
                shutil.rmtree('picture/backup')
                shutil.move('picture/save','picture/backup')
                os.mkdir('picture/save')
            S=''
            for s in __file__.split('/'):
                S += s+'_'
            plt.savefig('picture/save/'+sce_normal+'_'+S+str(i)+'.png')

            acc,_,_=self.test(i,evall)
            # if best < acc:
            #     best = acc
            with open(modelpath,'wb') as f:
                torch.save(model,f)
            with open(optimizerpath,'wb') as f:
                torch.save(optimizer,f)

main = Main()
main.main()