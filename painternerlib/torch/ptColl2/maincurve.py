#-*-coding:utf-8-*-
import torch
from torch import nn
import sys,os
print sys.path[0]
sys.path.remove(sys.path[0])
sys.path.append(os.getcwd())   ## then you can import preprocess in fanterfolder


from ptColl2.curvenet import Net
from ptColl2.curve2m import fload_with_evall
from ptColl2.curve2m import getplot
import numpy as np
import random
import argparse
from matplotlib import pyplot as plt
parser=argparse.ArgumentParser(description='option')
parser.add_argument('--cuda', action='store_true',
                    help='use CUDA')
parser.add_argument('--infer', action='store_true',
                    help='use CUDA')
args = parser.parse_args()

modelpath='model/curve/model.pt'

sdata = fload_with_evall()
data = np.concatenate(sdata[0:3],0)
evall = np.array(sdata[3])
print "evall length",len(evall)
print "data length",len(data)


if args.infer:
    with open(modelpath,'rb') as f:
        model = torch.load(f)
else:
    model = Net(2,3)
if args.cuda:
    model.cuda()

optimizer = torch.optim.Adamax(model.parameters(),lr=2e-3)

def train(epoch):
    batch_size=10
    interval=300

    total_loss = 0
    all_loss = 0
    np.random.shuffle(data)
    for num, i in enumerate(range(0, len(data), batch_size)):
        inputs = np.array(data[i:i+batch_size,:2],dtype=np.float32)
        label =np.array( np.expand_dims(data[i:i+batch_size,2],1),dtype=np.int64)
        # getplot(inputs,label)

        inputs,label = torch.Tensor(inputs), torch.LongTensor(label)
        if args.cuda:
            inputs = inputs.cuda()
            label = label.cuda()

        optimizer.zero_grad()
        loss=model([inputs,label,True])
        loss.backward()
        total_loss += loss.item()
        all_loss += loss.item()
        optimizer.step()

        if (num+1)%interval == 0:
            print "epoch: {},loss: {}, allloss:{}, eclipse: {}".format(epoch,total_loss/interval,
                                                    all_loss/(num+1),i*100.0/len(data))
            total_loss = 0
    # plt.show() #查看数据是否正常.
        
def test(epoch):
    batch_size=100
    interval=300

    a=0
    devacc = 0
    data = evall
    with torch.no_grad():
        m_inputs = []
        m_predict = []
        for num, i in enumerate(range(0, len(data), batch_size)):
            inputs_ = np.array(data[i:i+batch_size,:2],dtype=np.float32)
            label =np.array( np.expand_dims(data[i:i+batch_size,2],1),dtype=np.int64)

            inputs,label = torch.Tensor(inputs_), torch.LongTensor(label)
            if args.cuda:
                inputs = inputs.cuda()
                label = label.cuda()

            predict=model([inputs,label,False])
            acccount = torch.eq(predict.long(),label.squeeze(1)).sum().item()
            devacc += acccount
            a += batch_size

            m_inputs.append(inputs)
            m_predict.append(predict.unsqueeze(1))
    
        acc = devacc*100.0/a
        print "epoch {},acc {}".format(epoch,acc)
    m_inputs = torch.cat(m_inputs)
    m_predict = torch.cat(m_predict)
    return acc,np.array(m_inputs), np.array(m_predict)

def main():
    epoch  = 200
    best = 0
    if args.infer:
        acc,data,label = test(0)
        getplot(data,label)
        plt.show()
        return

    for i in range(epoch):
        train(i)
        acc,_,_=test(i)
        # if best < acc:
        #     best = acc
        with open(modelpath,'wb') as f:
            torch.save(model,f)

main()