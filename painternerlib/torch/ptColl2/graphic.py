import matplotlib.pyplot as plt
import numpy as np
import time

fig = plt.figure()
ax = fig.add_subplot(111)

# some X and Y data
x = np.arange(100)
y = np.zeros(100)
y[-1] = 100

li, = ax.plot(x, y)

# draw and show it
ax.relim() 
ax.autoscale_view(True,True,True)
fig.canvas.draw()
plt.show(block=False)


def GraphicUpdate(value):
    y[:-1] = y[1:]
    y[-1:] = np.array(value)
    # set the new data
    li.set_ydata(y)
    fig.canvas.draw() 

def GraphicTest():
    count = -1
    while True:
        count += 1
        try:
            GraphicUpdate(count)
            time.sleep(0.1)
        except KeyboardInterrupt:
            break












# import numpy as np
# import matplotlib.pyplot as plt
# import matplotlib.animation as animation
# from threading import Thread
# import time


# def data_gen(t=0):
#     cnt = 0
#     while cnt < 1000:
#         cnt += 1
#         t += 0.1
#         yield t, np.sin(2*np.pi*t) * np.exp(-t/10.)


# def init():
#     ax.set_ylim(-1.1, 1.1)
#     ax.set_xlim(0, 10)
#     del xdata[:]
#     del ydata[:]
#     line.set_data(xdata, ydata)
#     return line,

# fig, ax = plt.subplots()
# line, = ax.plot([], [], lw=2)
# ax.grid()
# xdata, ydata = [], []


# def run(data):
#     # update the data
#     t, y = data
#     xdata.append(t)
#     ydata.append(y)
#     xmin, xmax = ax.get_xlim()

#     if t >= xmax:
#         ax.set_xlim(xmin, 2*xmax)
#         ax.figure.canvas.draw()
#     line.set_data(xdata, ydata)

#     return line,

# ani = animation.FuncAnimation(fig, run, data_gen, blit=False, interval=10,
#                               repeat=False, init_func=init)

# class thread1(Thread):
#     def __init__(self):
#         super(thread1,self).__init__()
#     def run(self):
#         while(1):
#             time.sleep(1)
#             print "over"

# th = thread1()
# th.start()
# plt.show()
# print "over"

# import matplotlib.pyplot as plt
# plt.ion()
# class DynamicUpdate():
#     #Suppose we know the x range
#     min_x = 0
#     max_x = 10

#     def on_launch(self):
#         #Set up plot
#         self.figure, self.ax = plt.subplots()
#         self.lines, = self.ax.plot([],[], 'o')
#         #Autoscale on unknown axis and known lims on the other
#         self.ax.set_autoscaley_on(True)
#         self.ax.set_xlim(self.min_x, self.max_x)
#         #Other stuff
#         self.ax.grid()

#     def on_running(self, xdata, ydata):
#         #Update data (with the new _and_ the old points)
#         self.lines.set_xdata(xdata)
#         self.lines.set_ydata(ydata)
#         #Need both of these in order to rescale
#         self.ax.relim()
#         self.ax.autoscale_view()
#         #We need to draw *and* flush
#         self.figure.canvas.draw()
#         self.figure.canvas.flush_events()

#     #Example
#     def __call__(self):
#         import numpy as np
#         import time
#         self.on_launch()
#         xdata = []
#         ydata = []
#         for x in np.arange(0,10,0.5):
#             xdata.append(x)
#             ydata.append(np.exp(-x**2)+10*np.exp(-(x-7)**2))
#             self.on_running(xdata, ydata)
#             time.sleep(1)
#         return xdata, ydata

# d = DynamicUpdate()
# d()
# print "over"

# import joystick as jk
# import numpy as np
# import time

# class test(jk.Joystick):
#     # initialize the infinite loop decorator
#     _infinite_loop = jk.deco_infinite_loop()

#     def _init(self, *args, **kwargs):
#         """
#         Function called at initialization, see the doc
#         """
#         print "hello"
#         self._t0 = time.time()  # initialize time
#         self.xdata = np.array([self._t0])  # time x-axis
#         self.ydata = np.array([0.0])  # fake data y-axis
#         # create a graph frame
#         self.mygraph = self.add_frame(jk.Graph(name="test", size=(500, 500), pos=(50, 50), fmt="go-", xnpts=10000, xnptsmax=10000, xylim=(None, None, 0, 1)))

#     @_infinite_loop(wait_time=0.2)
#     def _generate_data(self):  # function looped every 0.2 second to read or produce data
#         """
#         Loop starting with the simulation start, getting data and
#     pushing it to the graph every 0.2 seconds
#         """
#         # concatenate data on the time x-axis
#         self.xdata = jk.core.add_datapoint(self.xdata, time.time(), xnptsmax=self.mygraph.xnptsmax)
#         # concatenate data on the fake data y-axis
#         self.ydata = jk.core.add_datapoint(self.ydata, np.random.random(), xnptsmax=self.mygraph.xnptsmax)
#         self.mygraph.set_xydata(t, self.ydata)

# t = test()
# t.start()
# time.sleep(3)
# t.stop()

# import matplotlib.pyplot as plt
# import numpy as np
# import time

# fig = plt.figure()
# ax = fig.add_subplot(111)

# # some X and Y data
# x = np.arange(10000)
# y = np.random.randn(10000)

# li, = ax.plot(x, y)

# # draw and show it
# ax.relim() 
# ax.autoscale_view(True,True,True)
# fig.canvas.draw()
# plt.show(block=False)

# # loop to update the data
# while True:
#     try:
#         y[:-10] = y[10:]
#         y[-10:] = np.random.randn(10)

#         # set the new data
#         li.set_ydata(y)

#         fig.canvas.draw()

#         time.sleep(0.01)
#     except KeyboardInterrupt:
#         break