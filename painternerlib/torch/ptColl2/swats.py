#-*-coding:utf-8-*-
import math
import torch
from torch.optim.optimizer import Optimizer

class Swats(Optimizer):
    """Implements Adam algorithm.

    It has been proposed in `Adam: A Method for Stochastic Optimization`_.

    Arguments:
        params (iterable): iterable of parameters to optimize or dicts defining
            parameter groups
        lr (float, optional): learning rate (default: 1e-3)
        betas (Tuple[float, float], optional): coefficients used for computing
            running averages of gradient and its square (default: (0.9, 0.999))
        eps (float, optional): term added to the denominator to improve
            numerical stability (default: 1e-8)
        weight_decay (float, optional): weight decay (L2 penalty) (default: 0)
        amsgrad (boolean, optional): whether to use the AMSGrad variant of this
            algorithm from the paper `On the Convergence of Adam and Beyond`_

    .. _Adam\: A Method for Stochastic Optimization:
        https://arxiv.org/abs/1412.6980
    .. _On the Convergence of Adam and Beyond:
        https://openreview.net/forum?id=ryQu7f-RZ
    """

    def __init__(self, params, lr=1e-3, betas=(0.9, 0.999), eps=1e-8,
                 weight_decay=0, amsgrad=False,momentum=0, dampening=0,nesterov=False):
        """ from momentum(included) is for SGD"""
        self.phase = 'ADAM'
        self.sgdlr = 0
        self.swflag = False

        if not 0.0 <= lr:
            raise ValueError("Invalid learning rate: {}".format(lr))
        if not 0.0 <= eps:
            raise ValueError("Invalid epsilon value: {}".format(eps))
        if not 0.0 <= betas[0] < 1.0:
            raise ValueError("Invalid beta parameter at index 0: {}".format(betas[0]))
        if not 0.0 <= betas[1] < 1.0:
            raise ValueError("Invalid beta parameter at index 1: {}".format(betas[1]))

        if momentum < 0.0:
            raise ValueError("Invalid momentum value: {}".format(momentum))
        if nesterov and (momentum <= 0 or dampening != 0):
            raise ValueError("Nesterov momentum requires a momentum and zero dampening")
        defaults = dict(lr=lr, betas=betas, eps=eps,
                        weight_decay=weight_decay, amsgrad=amsgrad,
                        momentum=momentum, dampening=dampening,nesterov=nesterov)

        super(Swats, self).__init__(params, defaults)

    def __setstate__(self, state):
        super(Swats, self).__setstate__(state)
        for group in self.param_groups:
            group.setdefault('amsgrad', False)
            group.setdefault('nesterov',False)

    def step(self, closure=None):
        """Performs a single optimization step.

        Arguments:
            closure (callable, optional): A closure that reevaluates the model
                and returns the loss.
        """
        loss = None
        if closure is not None:
            loss = closure()

        if(self.phase == 'SGD'):
            for group in self.param_groups:
                weight_decay = group['weight_decay']
                momentum = group['momentum']
                dampening = group['dampening']
                nesterov = group['nesterov']

                beta1, beta2 = group['betas']

                for p in group['params']:
                    if p.grad is None:
                        continue
                    d_p = p.grad.data

                    state = self.state[p]

                    if weight_decay != 0:
                        d_p.add_(weight_decay, p.data)

                    if len(state) == 0:
                        ## 如果从adam过渡来后没有进行add_param_group，那么应该用不到此判断。
                        state['step'] = 0
                        state['swlamda'] =  0
                        state['sgdvk'] = torch.zeros_like(p.data)
                    state['step'] += 1
                    sgdvk = state['sgdvk']
                    # if momentum != 0:
                    #     param_state = self.state[p]
                    #     if 'momentum_buffer' not in param_state:
                    #         buf = param_state['momentum_buffer'] = torch.zeros_like(p.data)
                    #         buf.mul_(momentum).add_(d_p)
                    #     else:
                    #         buf = param_state['momentum_buffer']
                    #         buf.mul_(momentum).add_(1 - dampening, d_p)
                    #     if nesterov:
                    #         d_p = d_p.add(momentum, buf)
                    #     else:
                    #         d_p = buf

                    assert momentum == 0
                    assert dampening == 0
                    sgdvk.mul_(beta1).add_(d_p)
                    sgdvk.mul_(1-beta1)

                    p.data.add_(-self.sgdlr, sgdvk)
        
        elif self.phase == 'ADAM':
            for group in self.param_groups:
                for p in group['params']:    ### 每一个模块是一个矩阵，所以可以用计算机矩阵法则批量操作。
                    if p.grad is None:
                        continue
                    grad = p.grad.data
                    if grad.is_sparse:
                        raise RuntimeError('Adam does not support sparse gradients, please consider SparseAdam instead')
                    amsgrad = group['amsgrad']

                    state = self.state[p]

                    # State initialization
                    if len(state) == 0:
                        state['step'] = 0
                        # Exponential moving average of gradient values
                        state['exp_avg'] = torch.zeros_like(p.data)
                        # Exponential moving average of squared gradient values
                        state['exp_avg_sq'] = torch.zeros_like(p.data)
                        
                        # state['swlamda'] =  torch.zeros_like(torch.zeros(1)) 
                        ## zeros_like may included cuda information,so can't use torch.zeros(1)
                        state['swlamda'] =  0
                        state['sgdvk'] = torch.zeros_like(p.data)
                        if amsgrad:
                            # Maintains max of all exp. moving avg. of sq. grad. values
                            state['max_exp_avg_sq'] = torch.zeros_like(p.data)

                    exp_avg, exp_avg_sq = state['exp_avg'], state['exp_avg_sq']
                    swlamda = state['swlamda']
                    sup_p = torch.zeros_like(p.data)

                    if amsgrad:
                        max_exp_avg_sq = state['max_exp_avg_sq']
                    beta1, beta2 = group['betas']

                    state['step'] += 1

                    if group['weight_decay'] != 0:
                        grad = grad.add(group['weight_decay'], p.data)
                        """add()表示添加函数(某种)，而add_则表示加法 ??
                        """

                    # Decay the first and second moment running average coefficient
                    exp_avg.mul_(beta1).add_(1 - beta1, grad)
                    exp_avg_sq.mul_(beta2).addcmul_(1 - beta2, grad, grad)
                    if amsgrad:
                        # Maintains the maximum of all 2nd moment running avg. till now
                        torch.max(max_exp_avg_sq, exp_avg_sq, out=max_exp_avg_sq)
                        # Use the max. for normalizing running avg. of gradient
                        denom = max_exp_avg_sq.sqrt().add_(group['eps'])
                    else:
                        denom = exp_avg_sq.sqrt().add_(group['eps'])
    
                    # assert len(denom.size()) == 2 or len(denom.size()) == 1
                    # 如果是3会怎么样？

                    bias_correction1 = 1 - beta1 ** state['step']
                    bias_correction2 = 1 - beta2 ** state['step']
                    step_size = group['lr'] * math.sqrt(bias_correction2) / bias_correction1

                    pk = sup_p.addcdiv_(-step_size, exp_avg, denom)
                    pk_1 = pk.view(-1,1)
                    
                    # if(len(denom.size()) == 1):
                    #     gamma = pk_1.matmul(pk) / (-pk.matmul(p.data))
                    # else:
                    ## reshape to plained matrix (vector)
                    gamma = pk_1.transpose(1,0).matmul(pk_1) / (-pk_1.transpose(1,0).matmul(p.data.view(-1,1)))

                    # print pk
                    swlamda = beta2*swlamda + (1-beta2)*gamma
                    sgdlr = swlamda/bias_correction2
 
                    # print state['step']
                    # # print sgdlr
                    # print pk.size()
                    # print gamma.size()

                    if state['step']> 1: 
                        
                        # a=abs( sgdlr - gamma ).squeeze().item()
                        # if a < 1e-5:
                        #     print a
                        if abs( sgdlr - gamma ) < group['eps']:
                            self.swflag = True
                            self.phase = 'SGD'
                            self.sgdlr = sgdlr.squeeze()
                        else:
                            self.swflag = False   ## only all has True that we can switch it

                    p.data.add_(pk)
                if(self.swflag):
                    print "\nSwitch to SGD\n", "lr", self.sgdlr
                    # p.data.addcdiv_(-step_size, exp_avg, denom)

        return loss