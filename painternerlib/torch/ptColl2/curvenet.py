# -*- coding: utf-8 -*-
import torch
from torch import nn
from torch.nn import functional as F
from torch import tensor

class Net(nn.Module):
    def __init__(self, input_size, class_size, drop_out=0.2):
        super(Net, self).__init__()

        self.input_size = input_size
        self.class_size = class_size
        self.drop_out = drop_out

        self.count = 0

        self.full1 = nn.Linear(self.input_size,128)
        self.full2 = nn.Linear(128,128)
        self.full3 = nn.Linear(128,128)
        self.fullend = nn.Linear(128,3)
        
        # self.initiation()

    def initiation(self):
        for module in self.modules():
            if isinstance(module, nn.Linear):
                nn.init.xavier_uniform_(module.weight, 0.1)
                # nn.init.xavier_uniform_(module.bias, 0.1)
                module.bias.data.uniform_(-0.1,0.1)

    def forward(self,inputs,ceprob= None):
        # fA = torch.relu
        def fA(X):
            # return torch.softmax(X,-1)
            # return torch.sigmoid(X)
            # return F.leaky_relu(X)
            return torch.tanh(X)

        inputs,label,is_train = inputs
        f1 = fA(self.full1(inputs))
        f2 = fA(self.full2(f1))
        fend = fA(self.full3(f2))
        o = self.fullend(fend)
        
        o = torch.softmax(o,-1)
        predict = o.argmax(1)
        if(not is_train):
            return predict

        if ceprob is None:
            ori_o = o.gather(1,label).squeeze(1)
            ori_loss = -torch.log(ori_o)
            ori_loss = ori_loss.mean()
            return ori_loss,torch.tensor(0),predict
        else:
            ori_o = o.gather(1,label).squeeze(1)
            ori_loss = -torch.log(ori_o)
            ori_loss = ori_loss.mean()

            mask = torch.ones((label.size()[0],self.class_size),dtype=torch.float32)
            for i,item in enumerate(mask):
                item[label[i][0]] = 0.001
            loss = (ceprob*1.5)*mask 
            loss = ( -loss*torch.log(o) ).sum(1)
            # self.count += 1
            # print torch.log(o)
            # if self.count > 300:
            #     exit()
            loss = loss.mean()
        return ori_loss, loss, predict