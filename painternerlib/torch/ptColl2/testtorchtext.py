#-*-coding:utf-8-*-
import sys,os
print sys.path[0]
sys.path.remove(sys.path[0])
sys.path.append(os.getcwd())   ## then you can import preprocess in fanterfolder

import torchtext
from torchtext import data
from torchtext import datasets
# from torchtext.vocab import GloVe

import torch
import spacy
import janome
from janome.tokenizer import Tokenizer
# j_t = Tokenizer()
# def tokenizer(text): 
#     return [tok for tok in j_t.tokenize(text, wakati=True)]
# print(tokenizer(u'あなたを愛しています。'))

spacy_en = spacy.load('en')
def tokenizer(text):
    return [tok.text for tok in spacy_en.tokenizer(text)]
print(tokenizer(u'I love you'))

TEXT = data.Field(sequential=True, tokenize=tokenizer, lower=True)
# TEXT = data.Field(sequential=True, tokenize=tokenizer, lower=True,include_lengths=True)
LABEL = data.Field(sequential=False, use_vocab=False)
# # # noting: TabularDataset may use tsv csv or json
# train, val, test = data.TabularDataset.splits(
#                     path='./data/develop', train='train_ja.tsv',
#                     validation='val_ja.tsv',test='test_ja.tsv', format='tsv',
#                     fields=[('Text',TEXT),('Label',LABEL)])
# # noting: TabularDataset may use tsv csv or json
train, val, test = data.TabularDataset.splits(
                    path='./data/develop', train='train_en.tsv',
                    validation='val_en.tsv',test='test_en.tsv', format='tsv',
                    fields=[('Text',TEXT),('Label',LABEL)])

# for d in train:
#     print vars(d)
# for d in val:
#     print vars(d)
# for d in test:
#     print vars(d)

TEXT.build_vocab(train, min_freq=2)
print TEXT.vocab.freqs
print TEXT.vocab.stoi  ## string: index
print TEXT.vocab.itos  ## index: string

train_iter, val_iter, test_iter = data.Iterator.splits(
        (train, val, test), batch_sizes=(2, 2, 2), device=-1)

batch = next(iter(train_iter))
print(batch.Text)
print(batch.Label)
batch = next(iter(train_iter))
print(batch.Text)
print(batch.Label)

from torchtext.vocab import FastText
TEXT.build_vocab(train, vectors=FastText(language="zh"), min_freq=2) 
## .vector_cache/wiki.zh.vec: 0.00B [00:00, ?B/s]
## github上有同样的名字,所以先从网页下载看看，不过网站有bin+text 和单独text版本。（预警：下载极其不稳定。）

