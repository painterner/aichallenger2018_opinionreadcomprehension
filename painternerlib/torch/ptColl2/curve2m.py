#-*-coding:utf-8-*-
import numpy as np
from matplotlib import pyplot as plt
import cPickle

shape2m = """
        
                \              |             /            
                 \             |            / 
                  \            | C         /  
                   \      \    |    /  B  /    
                     \     \   |   /    /
                        \    \ |  /   /
                           \   |   /           A
                _______________|_______________________
        """

def square(alpha):
    X=np.array([v for v in range(1000)],dtype=np.float32)
    X=X/100    ## 0-1000   -->     0-10
    Y=alpha*(X**2)
    X=np.expand_dims(X,1)
    Y=np.expand_dims(Y,1)
    plt.plot(X,Y)
    return (X,Y)

def frandom_rand(count,a=0,b=1):
    rand = np.random.rand(count)
    return rand*(b-a) + a

class RandSquare():
    def __init__(self,windows=[[0,0]]):
        self.windows = windows

    def randsquare(self,alpha1,alpha2,types='A'):
        rtop = 10 ## X axis取值范围.
        self.rflag = False

        def start(x):
            for window in self.windows:
                if (x>window[0]) and (x<window[1]):
                    self.rflag = True
                    break ## must break to hold window variable temp
            if self.rflag:
                self.rflag = False
                ## 以(window[0],y1), (window[0],y1) 距离作为直径，中点为圆心画圆 
                a1,b1 = ( (window[0]+window[1])*1.0/2, alpha1*(window[0]**2+window[1]**2)*1.0/2 )
                c1_square = (window[0]-window[1])**2*1.0 + (alpha1*(window[0]**2-window[1]**2))**2*1.0
                ## move 圆
                a1 = np.sqrt(c1_square) + window[0] 
                b1 = alpha1*(window[1]**2)
                y1 = np.sqrt(c1_square-(x-a1)**2) + b1
                a2,b2 = ( (window[0]+window[1])*1.0/2, alpha2*(window[0]**2+window[1]**2)*1.0/2 )
                c2_square = (window[0]-window[1])**2*1.0 + (alpha2*(window[0]**2-window[1]**2))**2*1.0
                ## move 圆
                a2 = np.sqrt(c2_square) + window[0] 
                b2 = alpha2*(window[1]**2)
                y2 = np.sqrt(c2_square-(x-a2)**2) + b2
                if types=='A':
                    # print x,a1,b1,c1_square,y1
                    return 0,y1,0.0  
                if types=='B':
                    return y1,y2,1.0
                if types=='C':
                    return y2,alpha2*(rtop**2)+10,2.0
            else:
                if types=='A':
                    return 0, alpha1*(x**2),0.0
                elif types=='B':
                    return alpha1*(x**2),alpha2*(x**2),1.0
                elif types=='C':
                    return alpha2*(x**2),alpha2*(rtop**2)+10,2.0
        X=np.random.rand(100)
        ## 升序排序。
        X=np.sort(X)
        X=X*rtop
        SY = []
        for i,v in enumerate(X):
            if i == 0:
                SY.append(np.array([[v,v**2,0.0]]))  ### 0,0 poing 标记为A
            else:
                if(types =='C'):
                    count = len(X)-i
                else:
                    # count = i**2   ## this will lead to large data
                    count = i
                ## 采样点数与index的平方成正比。
                p1,p2,index = start(v)
                Y = frandom_rand(count,p1,p2)
                Y = np.sort(Y)
                V = np.array([[v]]*count)
                Y = np.expand_dims(Y,1)
                index = [[index]]*count
                index = np.array(index)
                # print V.shape,Y.shape,index.shape,count
                SY.append(np.concatenate([V,Y,index],1))

        return SY
def mixUp(alpha1,alpha2,window=[None,None]):
    window[0]

def plotABC(SY):
    X=[]
    Y=[]
    for item in SY:
        X.append(item[0])
        Y.append(item[1])
    X=np.array(X)
    Y=np.array(Y)
    plt.plot(X,Y,'.')
    return X,Y

def getevall(alpha1 = 2,alpha2 = 4.0):
    xup = 10
    yup = int(alpha2*(xup**2)) + 11

    evall=[]
    for i in range(1,xup*10+1): ## Sample 100 points
        vi = i*1.0/10
        v1 = alpha1*(vi**2)
        v2 = alpha2*(vi**2)
        for ii in range(1,301): ## Sample 300 points
            vii = ii/300.0*yup
            if vii < v1:
                annotation = 0
            elif vii >=v1 and vii < v2:
                annotation = 1
            elif vii >=v2:
                annotation = 2
            evall.append([vi,vii,annotation])
    return evall

def fsampleABC(flatten=True,alpha1 = 2,alpha2 = 4.0,window = [[6.0,6.5],[3,3.5],[4,5],[1,1.5],[8,8.5]]):
    def allABC(SY):
        vall=[]
        for item in SY:
            for point in item:
                vall.append([point[0],point[1],point[2]])
        return np.array(vall)

    randsquare = RandSquare(window).randsquare
    SYA = randsquare(alpha1,alpha2,'A')
    SYB = randsquare(alpha1,alpha2,'B')
    SYC = randsquare(alpha1,alpha2,'C')
    if flatten:
        SYA = allABC(SYA)
        SYB = allABC(SYB)
        SYC = allABC(SYC)

    evall = getevall(alpha1,alpha2)
    return SYA,SYB,SYC,evall

def getplot(inputs,label):
    XA=[];YA=[];XB=[];YB=[];XC=[];YC=[]
    for idx,lb in enumerate(label):
        if lb[0] == 0:
            XA.append(inputs[idx][0]);YA.append(inputs[idx][1])
        elif lb[0] == 1:
            XB.append(inputs[idx][0]);YB.append(inputs[idx][1])
        elif lb[0] == 2:
            XC.append(inputs[idx][0]);YC.append(inputs[idx][1])
    plt.plot(XA,YA,'or')
    plt.plot(XB,YB,'ob')
    plt.plot(XC,YC,'og')

def fsave(SYA,SYB,SYC):
    with open('data/curve_train.obj','wb') as f:
        cPickle.dump([SYA,SYB,SYC],f)
def fload():
    with open('data/curve_train.obj','rb') as f:
        SYA,SYB,SYC =  cPickle.load(f)
    return SYA,SYB,SYC
def fload_with_evall(alpha1 = 2,alpha2 = 4.0):
    SYA,SYB,SYC=fload()
    evall = getevall(alpha1,alpha2)
    return SYA,SYB,SYC,evall

def main():
    ## 观察图像上A，B，C都与少量交叉，可能是因为其上面曲线的四舍时导致的(参考frandom_rand函数)。
    SYA,SYB,SYC,evall = fsampleABC()
    # fsave(SYA,SYB,SYC)
    # SYA,SYB,SYC,evall = fload_with_evall()
    A=plotABC(SYA)
    B=plotABC(SYB)
    C=plotABC(SYC)

    plt.show()

    evall = np.array(evall)
    label = np.expand_dims(evall[:,2],1)
    getplot(evall,label)
    plt.show()

if __name__ == "__main__":
    main()
