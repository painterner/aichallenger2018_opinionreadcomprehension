#!-*-coding:utf-8-*-
import torch
import torch.nn as nn
import numpy as np
import torchvision.utils as vutils
from tensorboardX import SummaryWriter

writer = SummaryWriter()

class ParameterDebug():
    """
        1.每种参数的散点图（不同位置的数值,颜色分辨不同类型参数)
        2.统计图,方差，均值 (是否是正态分布)
        3.梯度曲线，方向图
        4.对应输入数据的分布图。
    """
    def __init__(self,record_time=1):
        self.params={}
        self.putflag = False
        self.record_time = record_time
        self.defaultconfig = {
            "image_time": 1
        }

    def config(self,dict_):
        for v in self.defaultconfig:
            if v not in dict_:
                raise Exception('dict_ item must equal to defaultconfig')
        self.defaultconfig = dict_

    def load(self,path):
        pass
    def save(self,path):
        pass
    # def put(self,model):
    #     for m in model.modules():
    #         for n,p in model.named_parameters():
    #             self.params[n] = np.array(p)
    #     self.putflag = True
    def analyse(self,model,n_iter,name_list=None,tensor_image=False):
        # self.put(model)
        # assert self.putflag
        # for m in model.modules():
        m= model
        if name_list is not None:
            if not isinstance(name_list,tuple) and not isinstance(name_list,list):
                if isinstance(name_list,str):
                    name_list = [name_list]
        for name, param in m.named_parameters():
            if 'bn' not in name:
                if name_list is not None:
                    if name not in name_list:
                        continue
                # print name
                if n_iter % self.record_time == 0:
                    try:
                        writer.add_histogram(name, param, n_iter)
                    except:
                        print "add_histogram error"
                        print "name",name, "param",param,"n_iter",n_iter
                if tensor_image:
                    if n_iter % self.defaultconfig['image_time'] == 0:
                        tensor = self.prepare_image(param)
                        # print tensor.size()
                        if tensor is None:
                            continue
                        image = vutils.make_grid(tensor, normalize=True, scale_each=True)                   
                        writer.add_image('Image_'+name+"_iter"+str(n_iter), image, n_iter)  # 4D mini-batch Tensor of shape (B x C x H x W)
                    
            else:
                print "name",name,"param",param
                raise Exception("bn error")

    def prepare_image(self,tensor,checkdim=None):
        def make(tensor):
            if tensor.dim() == 0:
                tensor = tensor.unsqueeze(0)[None,None,None,:]
            elif tensor.dim() == 1:
                tensor = tensor[None,None,None,:] 
            elif tensor.dim() == 2:
                tensor = tensor[None,None,:,:]
            else:
                tensor = tensor.view(-1)[None,None,None,:]
            return tensor
        if checkdim is not None:
            if checkdim != tensor.dim():
                return None
        return make(tensor)

    def putcurve(self,name,n_iter):
        # self.putcurve('xoxo',n_iter)
        writer.add_pr_curve(name, np.random.randint(2, size=100), np.random.rand(100), n_iter)
        # writer.add_scalars('data/scalar_group', {"xsinx": n_iter * np.sin(n_iter),
        #                                     "xcosx": n_iter * np.cos(n_iter),
        #                                     "arctanx": np.arctan(n_iter)}, n_iter)
    def puttext(self,text):
        pass

class Testmodel(nn.Module):
    def __init__(self):
        super(Testmodel,self).__init__()
        self.model = nn.ModuleList( [ nn.Linear(128,128) for _ in range(2)] )
        self.model[0].weight.data = torch.zeros(128,128)

if __name__ == "__main__":
    paradebug = ParameterDebug()
    m = Testmodel()
    for i in range(10):
        for mm in m.modules():
            if isinstance(mm,nn.Linear):
                # mm.weight.data = torch.tensor([[i,i+1,i+2],[i+2,i+1,i]])
                mm.weight.data.normal_()
                # print mm.weight
        paradebug.analyse(m,i,tensor_image=True)


    writer.close()
        



"""
tensorboardx:
    tensorboard --logdir runs
    tensorboard projector hangs on 'computing PCA' bug:
        pip install --upgrade tensorboard   (更新后为1.12.0可用，之前版本未记录)
    tensorboard 无video标签:
        ka@ka-15-ce0xx:/mnt/u16/home/ka/Documents/proj/tensorboard_pytorch/tensorboardX$ python examples/demo.py
        add_video needs package moviepy
        add_video needs package moviepy
        源码的写法:
            try:
                import moviepy
            except ImportError:
                print('add_video needs package moviepy')
                return
            (有个疑问：这样写就不会出现前面import moviepy与此重复的错误.)
    writer.add_embedding(features, metadata=label, label_img=images.unsqueeze(1)) 理解:
        features应该是告诉projector的PCA等分析的元数据的。剩下的项目用来可视化输出，所以即使这些东西没有，
        也是可以PCA分析的，只不过是一堆一样的点，只能看出数据特性的整体聚集度。
        可以用来展示网络分析结果的聚集度?


    调试方法，不正确的数据，手动更改某些参数观察是否能够改变。
"""