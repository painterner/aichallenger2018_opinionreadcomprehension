#!-*-coding:utf-8-*-

def trap0():
    # Wrong, will return None
    a=[1]
    b = a.append(2) if True else 0
    
    # Right
    a=[1]
    a.append(2)
    b = a if True else 0

def trap1():
    m = [0]
    # Wrong
    def f():
        a=m
        m=2  # 要么重写，要么赋值，只能选择一样
    # Right
    def f1():
        a=m

def trick():
    import binascii
    binascii.b2a_hex(u"123".encode("utf8"))

