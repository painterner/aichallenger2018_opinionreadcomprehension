#!-*-coding:utf-8-*-
import tensorflow as tf
import os,sys
import numpy as np
import datetime

def __test():
    sess = tf.InteractiveSession()

    ## image
    with tf.name_scope('input'):
        x=tf.placeholder(tf.float32,shape=[784*20],name='inputx')
    xxx = tf.placeholder(tf.float32,shape=[2],name='inputxxx')

    with tf.name_scope('input_reshape'):
        image_shaped_input = tf.reshape(x, [-1, 28, 28, 1])
        tf.summary.image('input', image_shaped_input, 10)
        tf.summary.scalar('image_slice',image_shaped_input[0,0,0,0])
        tf.summary.scalar('listxxx',tf.reduce_mean(xxx))

    ## scalar
    mean = tf.reduce_mean(x)
    tf.summary.scalar('mean', mean)
    nd = np.array([1,2,3.,100,]).mean()
    for i in range(10):
        nd = nd + 1
        tf.summary.scalar('numpy',nd)  ## will auto generate 10 unidentity

    ## histogram
    histogram = tf.summary.histogram('histogram', x)

    merged = tf.summary.merge_all()
    single_merged = tf.summary.merge([histogram])
    time_ = datetime.datetime.now().strftime('%Y_%m_%d_%H_%M_%S')
    train_writer = tf.summary.FileWriter('/tmp/parameter_debug/'+time_, sess.graph)

    X = np.random.randn(784*20)
    for i in range(10,5,-1):
        X = X + 1
        summary,r0 = sess.run([merged,mean],feed_dict={x:X,xxx:[2.0,3.0]})
        print(r0)
        run_metadata = tf.RunMetadata()
        train_writer.add_run_metadata(run_metadata, 'step%03d' % i)
        train_writer.add_summary(summary,i)
    for i in range(6):
        X = X + 1
        summary,r0 = sess.run([merged,mean],feed_dict={x:X,xxx:[2.0,3.0]})
        print(r0)
        run_metadata = tf.RunMetadata()
        train_writer.add_run_metadata(run_metadata, 'step%03d' % i)
        train_writer.add_summary(summary,i)
    
    train_writer.close()


if __name__ == "__main__":
    __test()

"""
if scalar look like mess:
    slide the "Smoothing" slider will change

如果多次运行使用一个文件夹存放events, 在可视化的时候以scalar为例， 这些events数据会互相耦合。所以在不需要耦合的时候，可以
利用current time来命名一个文件夹使用。

## summary, = sess.run([merged],feed_dict={xxx})
    这里必须要有',' ， 否则返回会判断成一个列表（add_summary传入的是str)

note:
    test: tf.name_scope will merge sub- to one tags
"""