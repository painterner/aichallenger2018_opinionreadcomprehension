# -*- coding: utf-8 -*-
import torch
from torch import nn
import torch.nn.init as init
from torch.autograd import Variable
from torch.nn import functional as F
from termcolor import colored
import logging
import sys,os
from painternerCollection.utils import printonece1

class AttnGRUCell(nn.Module):
    def __init__(self, input_size, hidden_size):
        super(AttnGRUCell, self).__init__()
        self.input_size = input_size
        self.hidden_size = hidden_size
        self.Wr = nn.Linear(input_size, hidden_size)
        self.Ur = nn.Linear(hidden_size, hidden_size)
        self.W = nn.Linear(input_size, hidden_size)
        self.U = nn.Linear(hidden_size, hidden_size)

        self.vcuda=False

        init.xavier_normal_(self.Wr.state_dict()['weight'])
        init.xavier_normal_(self.Ur.state_dict()['weight'])
        init.xavier_normal_(self.W.state_dict()['weight'])
        init.xavier_normal_(self.U.state_dict()['weight'])


    def setcuda(self,v=True):
        self.vcuda=v

    def forward(self, fact, hi_1, g):
        # fact is the final output of InputModule for each sentence and fact.size() = (batch_size, embedding_length)
        # hi_1.size() = (batch_size, embedding_length=hidden_size)
        # g.size() = (batch_size, )

        r_i = torch.sigmoid(self.Wr(fact) + self.Ur(hi_1))
        h_tilda = torch.tanh(self.W(fact) +r_i*self.U(hi_1))
        # print("g size",g.size(),"hi_1 size",hi_1.size(),"h_tilada size",h_tilda.size())
        g=g.unsqueeze(1)   ## we should unsqueeze(1) ? or use batch correlattion
        hi = g*h_tilda + (1 - g)*hi_1

        return hi # Returning the next hidden state considering the first fact and so on.

class AttnGRU(nn.Module):
    def __init__(self, input_size, hidden_size):
        super(AttnGRU, self).__init__()
        self.input_size = input_size
        self.hidden_size = hidden_size
        self.AttnGRUCell = AttnGRUCell(input_size, hidden_size)

        self.vcuda=False

    def setcuda(self,v=True):
        self.vcuda=v

    def forward(self, facts, G):
        # facts.size() = (batch_size, num_sentences, embedding_length)
        # fact.size() = (batch_size, embedding_length=hidden_size)
        # G.size() = (batch_size, num_sentences)
        # g.size() = (batch_size, )
        
        h_0 = Variable(torch.zeros(self.hidden_size))
        if(self.vcuda):
            h_0 = h_0.cuda()

        for sen in range(facts.size()[1]):
            fact = facts[:, sen, :]
            g = G[:, sen]
            if sen == 0: # Initialization for first sentence only
                hi_1 = h_0.unsqueeze(0).expand_as(fact)
            hi_1 = self.AttnGRUCell(fact, hi_1, g)
        C = hi_1 # Final hidden vector as the contextual vector used for updating memory

        return C

class MemoryModule(nn.Module): # Takes facts, question and prev_mem as its and output next_mem
    def __init__(self, hidden_size):
        super(MemoryModule, self).__init__()
        self.hidden_size = hidden_size
        self.AttnGRU = AttnGRU(hidden_size, hidden_size)
        self.W1 = nn.Linear(4*hidden_size, hidden_size)
        self.W2 = nn.Linear(hidden_size, 1)
        self.W_mem = nn.Linear(3*hidden_size, hidden_size)

        self.vcuda = False

        init.xavier_normal_(self.W1.state_dict()['weight'])
        init.xavier_normal_(self.W2.state_dict()['weight'])
        init.xavier_normal_(self.W_mem.state_dict()['weight'])

    def setcuda(self,v=True):
        self.vcuda=v
        self.AttnGRU.setcuda(self.vcuda)

    def gateMatrix(self, facts, questions, prev_mem):
        # facts.size() = (batch_size, num_sentences, embedding_length=hidden_size)
        # questions.size() = (batch_size, 1, embedding_length)
        # prev_mem.size() = (batch_size, 1, embedding_length)
        # z.size() = (batch_size, num_sentences, 4*embedding_length)
        # G.size() = (batch_size, num_sentences)

        # passage  ## [32,1,10,128]
        # query  ## [32,100,1,128] 

        # passage  ## [32,100,128]
        # query  ##    [32,1,128] (<-- [32,10,128] )

        ## 与model.py的不同点之一，这里的长度是以passage，model.py是以query.
        questions = questions.expand_as(facts)
        prev_mem = prev_mem.expand_as(facts)

        embedding_length = self.hidden_size
        batch_size = facts.size()[0]
        z = torch.cat([facts*questions, facts*prev_mem, torch.abs(facts - questions), torch.abs(facts - prev_mem)], dim=2)
        # z.size() = (batch_size, num_sentences, 4*embedding_length)
        z = z.view(-1, 4*embedding_length)
        Z = self.W2(torch.tanh(self.W1(z)))
        Z = Z.view(batch_size, -1)
        G = F.softmax(Z,1)

        return G

    def forward(self, facts, questions, prev_mem):
        # questions = questions.unsqueeze(1)
        # prev_mem = prev_mem.unsqueeze(1)
        G = self.gateMatrix(facts, questions, prev_mem)
        C = self.AttnGRU(facts, G)
        # Now considering prev_mem, C and question, we will update the memory state as follows
        concat = torch.cat([prev_mem.squeeze(1), C, questions.squeeze(1)], dim=1)
        next_mem = F.relu(self.W_mem(concat))
        next_mem = next_mem.unsqueeze(1)

        return next_mem
