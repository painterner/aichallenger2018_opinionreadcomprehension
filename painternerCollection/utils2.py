#!-*-coding:utf-8-*-
import numpy as np
import time
def shuffle_answer__(data,data_ex=None):
    answer = data
    one_ex = data_ex
    # print "answer",answer
    # print "one_ex",one_ex
    if one_ex is not None: # [label,state]
        ## 不能直接concatenate,因为这样会把原来int 变成float(不过之后再转换也应该不会损失的。)
        lists = []
        for i in range(len(answer)):
            sublists = []
            for ii in range(3): 
                sublists.append(answer[i][ii] + one_ex[i][ii])
            lists.append(sublists)
        answer = lists

    else:
        # an_label = [[[0],[1],[2]]]*len(answer)  
        # 如果使用上面的，可能随后如果用random.shuffle会出问题是因为这里是浅拷贝。
        an_label = [[[1],[0],[0]] for _ in range(len(answer))] ## do not use batch_size,cause tails' may be less than it
        answer = np.concatenate([answer,an_label],-1)

    # print 'cat', answer

    for i in range(len(answer)):
        np.random.shuffle(answer[i])  ## shuffle only answer
    
    # print 'shuffle',answer

    if one_ex is not None:
        lists = []
        answerlists = []
        for i in range(len(answer)):
            sublists = []
            answersublists = []
            for ii in range(3): 
                sublists.append(answer[i][ii][-1:]) ## 每个选择标签只有一个概率标记(不计算其他loss,如相关性-->在上层函数处理)
                answersublists.append(answer[i][ii][0:-1])
            lists.append(sublists)
            answerlists.append(answersublists)
        # print answer
        answer = np.array(answerlists)
        label = np.array(lists)
    else:
        label = answer[:,:,-1:]  ## 本来是 [:,:,-1]（最后一维会自动squeeze，所以改成-1:,就不会自动squeeze了
        answer = answer[:,:,:-1]
    # print 'label',label    
    # print 'answer',answer

    return label,answer

if __name__ == "__main__":
    answer = [[[47052,213,24], [890,11,26], [60241,34,28]],
                [[52,23,25], [80,13,27], [241,325,29]]]

    one_ex = [[[1],[0],[0]], [[1./3],[1./3],[1./3]]]

    timeA = time.time()
    for i in range(2):
        shuffle_answer__(answer,one_ex)
    print time.time() - timeA
    # shuffle_answer__(answer)

"""
cat [[[47052, 213, 24, 1], [890, 11, 26, 0], [60241, 34, 28, 0]], [[52, 23, 25, 0.3333333333333333], [80, 13, 27, 0.3333333333333333], [241, 325, 29, 0.3333333333333333]]]
shuffle [[[47052, 213, 24, 1], [890, 11, 26, 0], [60241, 34, 28, 0]], [[80, 13, 27, 0.3333333333333333], [52, 23, 25, 0.3333333333333333], [241, 325, 29, 0.3333333333333333]]]
label [[[1.        ]
  [0.        ]
  [0.        ]]

 [[0.33333333]
  [0.33333333]
  [0.33333333]]]
answer [[[47052   213    24]
  [  890    11    26]
  [60241    34    28]]

 [[   80    13    27]
  [   52    23    25]
  [  241   325    29]]]
"""
