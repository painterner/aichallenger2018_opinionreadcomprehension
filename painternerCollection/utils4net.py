# -*- coding: utf-8 -*-
import torch
from torch import nn
from torch.nn import functional as F
import numpy as np
from painternerCollection.utils import fGet_pretrainedWord,fGet_word

class preEmbedding(nn.Module):
    def __init__(self, vocabsize, embedding_size, dynamic_embedding=True,path='data/pretrained_word2id.obj'):
        """
        Args:
            vocabsize: 训练集测试出的vocabulary大小
            embedding_size: As name shows
            dynamic_embedding: 是否使用动态embedding替换训练集含有但是预训练中没有的.
        """
        super(preEmbedding, self).__init__()
        print("pretrained embedding initing")
        self.vcuda = False
        self.dynamic_embedding = dynamic_embedding
        self.maskid = []
        self.path = path

        data_id,_ = fGet_word()
        pre_data = fGet_pretrainedWord(path=self.path)
        
        wordl = len(data_id)
        
        # weight = np.((vocabsize,embedding_size),dtype='float32')
        # np.random.randn默认是float64. 预训练的嵌入层值都比较低，尝试乘以一个0.1因子。
        weight = np.array(np.random.randn(vocabsize,embedding_size)*0.1,dtype='float32')
        for w in data_id:
            i = data_id[w]
            # if isinstance(pre_data[w],int) and pre_data[w]== 1: ## 如果pre中没有保存undef(比较合理), 可以修改成为判断 i is not in 
            if w not in pre_data:
                if self.dynamic_embedding:
                    self.maskid.append(i)
            else:
                weight[i] = pre_data[w]
                
                
        # print weight[2],weight[3]
        weight = torch.FloatTensor(weight)
        # print "preEmclass,Get weight shape", weight.shape
        # print "preEmclass,Mask id length", len(self.maskid)

        ## fixed Embedding
        self.em = nn.Embedding.from_pretrained(weight)

        if self.dynamic_embedding:
            self.dem = nn.Embedding(vocabsize,embedding_size)

    @property
    def weight(self):
        return self.dem.weight
    @property
    def bias(self):
        return self.dem.bias

    def setcuda(self):
        self.vcuda = True

    def getMask(self,input):
        rs = input.contiguous().view(-1)
        mask = torch.zeros(rs.size())
        for pos, i in enumerate(rs): 
            if int(i.item()) in self.maskid:
                mask[pos] = 1.0
        mask = mask.view(input.size()).unsqueeze(-1)
        if self.vcuda:
            mask = mask.cuda()
        return mask

    def getEmbedding(self,inputs):
        A = self.dem(inputs)*self.getMask(inputs) + self.em(inputs)
        return A

    def forward(self,inputs):
        if self.dynamic_embedding:
            r = self.getEmbedding(inputs)
        else:
            r = self.em(inputs)
        return r


if __name__ == "__main__":
    input = torch.LongTensor([1,2,3])
    A = preEmbedding(300)
    print A(input)