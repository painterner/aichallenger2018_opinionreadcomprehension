# -*- coding: utf-8 -*-
from termcolor import colored
import torch
import cPickle
import argparse
import jieba
import json
import numpy as np
import sys,os
from utils2 import shuffle_answer__

onecedict={}
onecesavefile={}
def printonece(string,color,target,target2=None,onece=True,Exit=False,**kargs):
	if(onece):
		if(string not in onecedict):
			onecedict[string] = 0
		else:
			return
	content = target
	print colored(string,color), content

	if "savepath" in kargs:
		if (kargs['savepath'] != True):
			assert type(kargs['savepath']) is str
			if ("savepath" not in onecesavefile):
				onecesavefile['savepath'] = kargs['savepath']
				flag = 'w'
			else:
				flag = 'a'
		else:
			flag = 'a'

		assert len(onecesavefile) is 1

		if(target2 is None):   ## error if use target2 == None (判断某些可用==,但是某些需要用is，可能与自定义操作符有关))
			target2 = content

		with open(onecesavefile['savepath'],flag) as f: ## you can pass savepath=path or savepath=True
			f.writelines(
				string+'\t'+
				str(target2.size())+'\n' +
				str(target2)+'\n'
				)
			

	if(Exit == True):
		exit()

onecesavefile1={}
vonece1freq = 50
global vonece1count
vonece1count = -1

def clearsome():
	global vonece1count
	vonece1count = -1

def printonece1(string,color,target,target2=None,onece=True,Exit=False,**kargs):
	if "savepath" in kargs:
		if (kargs['savepath'] != True):
			assert type(kargs['savepath']) is str
			onecesavefile1['savepath'] = kargs['savepath']

			global vonece1count
			vonece1count += 1
			flag = 'w'
		else:
			flag = 'a'

		if(target2 is None):   ## error if use target2 == None (判断某些可用==,但是某些需要用is，可能与自定义操作符有关))
			target2 = target

		if(vonece1count % vonece1freq == 0):
			with open(onecesavefile1['savepath'],flag) as f: ## you can pass savepath=path or savepath=True
				f.writelines(
					string+'\t'+
					str(target2.size())+'\n' +
					str(target2)+'\n'
					)

	
	if(onece):
		if(string not in onecedict):
			onecedict[string] = 0
		else:
			return
	print colored(string,color), target



vpath_word2id = "data/word2id.obj"
vpath_word_count = "data/word-count.obj"
vpath_word_output = "painterner/1.txt"
vpath_pretrained = "data/pretrained_word2id.obj"
vpath_pretrained_tencent = "data/pretrained_word2id_tencent.obj"
def printWord(id=True,count=True):
	flag = 0
	if(id):
		with open(vpath_word2id,'rb') as f:
			data = cPickle.load(f)
			with open(vpath_word_output,'wb') as f1:
				f.writelines(data)
				f.writelines("\n\n")
			flag = 1
	if(count):
		with open(vpath_word_count,'rb') as f:
			data = cPickle.load(f)
			if(flag):
				flag_o = 'ab'
			else:
				flag_o = 'wb'
			with open(vpath_word_output,flag_o) as f1:
				f.writelines(data)


def seg_data(path,answertoalternatives):
	print 'start process ', path
	data = {}
	with open(path, 'r') as f:
		for line in f:
			dic = json.loads(line, encoding='utf-8')
			data[str(dic['query_id'])] = dic
	return data

class PreData():
	"""
	"""
	def __init__(self):
		# word2id: 待训练的word2id (可以用jieba等提取)
		pass

	def fGet_word(self,path):
		with open(path,'rb') as f:
			data_count =cPickle.load(f) 
		return data_count

	def fload_id_embedding(self,path,word2id):
		"""
		Args:
			path: the return of fread_embedding
		Return:
			{id:1, id:embedding,xxx}
		"""
		worddict = self.fGet_word(path)
		print "pre-load file lines count", len(worddict)
		undef = 0
		weight = {}
		for word in word2id:
			i = word2id[word]
			if word not in worddict:
				undef += 1
				weight[i] = 1          ## if the work unknow
			else:
				weight[i] = worddict[word]
		print "counts of word in word2id but not worddict",undef
		return weight


	def fread_embedding(self,path,des=None):
		"""
		Args:
			path: 预训练的embedding路径
			des: 输出提取的dict路径,i.e.	{"word": embedding,...}

		Return:
		"""
		worddict = {}
		with open(path,'r') as f:
			for line in f.readlines():
				if(line > 0):
					## 以空白符分割，第一个是词语， 第二个是权重值
					sp = line.split()
					word = sp[0].decode('utf-8')  ## str to unicode   https://blog.csdn.net/vbaspdelphi/article/details/60332613
					vectors = [float(v) for v in sp[1:]]
					vector = np.array(vectors,dtype=np.float32)
					# assert word not in worddict
					worddict[word] = vector
		if des is not None:	
			print "writting exsting file to ", des
			with open(des,'wb') as f:
				cPickle.dump(worddict,f)		
		return worddict

class Load_embedding():
	"""
		修改成类，这样如果要添加返回值，只需要获取类的属性即可。（如果不想建立后变量保留，只需利用时在函数内局部初始化局部变量?)
	"""
	def __init__(self):
		self.worddict = None
		self.nowrite = False

	def fload_embedding(self,path,word2id,des=None,hold_undef_to_1=False):
		"""
		Args:
			path: 预训练的embedding路径
			word2id: 待训练的word2id (可以用jieba等提取)
			des: 输出提取的embedding
			hold_undef_to_1: 未定义标记为1. （不过可以不使用，在需要时添加即可）

		Return:
		"""
		worddict = {}
		pre_word2id = {}
		with open(path,'r') as f:
			for line in f.readlines():
				if(line > 0):
					## 以空白符分割，第一个是词语， 第二个是权重值
					sp = line.split()
					word = sp[0].decode('utf-8')  ## str to unicode   https://blog.csdn.net/vbaspdelphi/article/details/60332613
					vectors = [float(v) for v in sp[1:]]
					vector = np.array(vectors,dtype=np.float32)
					# assert word not in worddict
					worddict[word] = vector

		self.worddict = worddict
		print "pre-load file lines count", len(worddict)
		undef = 0
		for word in word2id:
			if word not in worddict:
				undef += 1
				if hold_undef_to_1:
					pre_word2id[word] = 1  ## <UNK>
			else:
				pre_word2id[word] = worddict[word]
		print "counts of word in word2id but not worddict",undef

		toreturn = pre_word2id,undef
		if self.nowrite:
			return toreturn

		if des is None:
			des = 'data/pretrained_word2id.obj'
			print("Warning:do you want to rewrite file",des,"Y/N")
			inputs = input()
			if inputs != 'Y' or inputs != 'y':
				return toreturn

		print "writting exsting file to ", des
		with open(des,'wb') as f:
			cPickle.dump(pre_word2id,f)
		return toreturn

def fSearch_id2word(id):
	with open(vpath_word2id,'rb') as f:
		data = cPickle.load(f)
	for d in data:
		if data[d] == id:
			return d
	return None

def fSearch_word2id(word):
	with open(vpath_word2id,'rb') as f:
		data = cPickle.load(f)
	return data[word]

def fPrint_wordstatus():
	with open(vpath_word2id,'rb') as f:  
		data =cPickle.load(f)            
		print("word2id length",len(data))
## 有可能在分解低于threshold的word的时候与以往的都重复了，而且重复的概率应该比没有重复的概率高，所以结果是整体变小了。
## onece test len(word2id)      > 90000     (< 100000)  
#             len(word_count)   > 330000    (<340000)

	with open(vpath_word_count,'rb') as f:
		data =cPickle.load(f)
		print("word_count length",len(data))  


def fGet_word():
	""" return word2id, word_count"""
	with open(vpath_word2id,'rb') as f:  
		data_id =cPickle.load(f)            
		print("word2id length",len(data_id))
## 有可能在分解低于threshold的word的时候与以往的都重复了，而且重复的概率应该比没有重复的概率高，所以结果是整体变小了。
## onece test len(word2id)      > 90000     (< 100000)  
#             len(word_count)   > 330000    (<340000)

	with open(vpath_word_count,'rb') as f:
		data_count =cPickle.load(f)
		print("word_count length",len(data_count))  
	
	return data_id, data_count

def fGet_pretrainedWord(path = vpath_pretrained):
	""" return: pretrainedWord"""
	with open(path,'rb') as f:
		data = cPickle.load(f)
		print ("pretrained data length",len(data))

	return data

def handle_data_for_train(one,fpadding,fapdding_anser,Hypara,mode,shuffle_answer=True):
	assert mode == 'train' or mode == 'eval'
	Hypara = Hypara
	if mode != 'inference':
		if Hypara['datalogic'] == 'logic1':
			one_ex_label = [x[-2] for x in one] # one[4](eval is word of candidate)
									# so wo use [-2] [-1] 
			one_ex_state = [x[-1] for x in one]  # may be used after
			# print "one_ex_label",one_ex_label
			# print "one_ex_state",one_ex_state
		elif Hypara['datalogic'] != 'direct':
			return
	padding = fpadding
	pad_answer = fapdding_anser

	query, _ = padding([x[0] for x in one], max_len=None) # 50 -> None
	answer = pad_answer([x[2] for x in one])
	ids = [x[3] for x in one]
								  ## 350 -> None
	passage = [x[1] for x in one] ## [N,L] --> 不同长度的batch划分结果不一样，所以采用batch=1,然后多次batch去backward取平均值。

	n = []
	SWid = [6094,26301,73247,20460,14608] ## [1,L] -> [1,N,M]
	# ，6094 # 。26301# ；73247 # ？ 20460 # ：14608
	def separate_one(SWid,content):
		p_ = []
		SWidlist = []
		for idx,word in enumerate(content): ## Swid and content cannot exchange position
			for wid in SWid:
				if wid == word:
					SWidlist.append(idx)
		i = -1
		lastid = -1  ## 如果没有一个标点符号，则全部放入。
		for idx in SWidlist:
			m = content[i+1:idx+1]
			if m != []:  ## 移除紧邻的错误标点符号。
				p_.append(m) ## 包含p[0][idx]的后一个，不包含前一个（除非是0)
			i = idx
			lastid = idx
		if (lastid+1) < len(content):
			p_.append(content[lastid+1:])
		return p_
	
	## check output of separate is almost be equal to input ?
	for i in range(len(passage)):
		p_ = separate_one(SWid,passage[i])
		n.append(p_)
	# def checkleng(p):
	# 	E=0
	# 	for i in range(len(p)):
	# 		E += len(p[i])
	# 	return E	
	# print checkleng(n[0]),len(passage[0])
	# print n[0],ids[0]
	# print passage[0]
	# print n[1],ids[1]
	# print passage[1]
	# exit()

	passage = n
	# print passage[0]
	if mode == 'inference':
		assert shuffle_answer == False
		label = None
	if shuffle_answer:
		if Hypara['datalogic'] == 'logic1':
			label,answer = shuffle_answer__(answer,one_ex_label)
			label = np.concatenate([ label,np.array(one_ex_label)[:,3:,:] ],-2)
			# print "label",label
			# print "answer",answer
		else:
			label,answer = shuffle_answer__(answer)

	return query,passage,answer,ids,label



if __name__ == "__main__":
	parser = argparse.ArgumentParser(description='some utils')

	parser.add_argument('-i',type=int,default=-1,
						help="id")

	parser.add_argument('-w',type=str,default=None,
						help="word")

	parser.add_argument('-v',action="store_true",
						help="verbose save id and count to file")

	parser.add_argument('-s',action="store_true",
						help="status about length of word")

	parser.add_argument('-pre',action="store_true",
						help="build pre-trained word vector")
	parser.add_argument('-prepath',type=str,default=None,
						help="path that will build pre-trained word vector")

	parser.add_argument('-pre_test',action="store_true",
						help="test pre-trained word vector")

	args = parser.parse_args()

	if args.s:
		fPrint_wordstatus()

	if args.i is not -1:
		print fSearch_id2word(args.i)
		
	if args.w is not None:
		print fSearch_word2id(list(jieba.cut(args.w))[0])  ## ERROR!! if use args.w directly

	if args.v:
		printWord()
	
	if args.pre:
		word2id,word_count = fGet_word()
		sgnspath = "/mnt/data/ALL/dataset/sgns.target.word-word.dynwin5.thr10.neg5.dim300.iter5"
		tencent200 = "/mnt/data/ALL/dataset/Tencet_AILab_ChineseEmbedding/Tencent_AILab_ChineseEmbedding/all/"

		## We need to use a function to execute, or (e.g. use global Load_embedding),memory will insufficient
		def execute(v):
			cload_embedding = Load_embedding()
			fload_embedding = cload_embedding.fload_embedding

			if args.prepath != 'sgns' and args.prepath != 'tencent':
				raise Exception('not supported! sgns or tencent ?')
			elif args.prepath == 'sgns':
				path = sgnspath
				predict,undef = fload_embedding(path,word2id)
			else:
				path = tencent200
				cload_embedding.nowrite = True
				predict,undef = fload_embedding(path+v,word2id,des=None,hold_undef_to_1=False)
			return predict,undef

		all_predict = {}
		des = vpath_pretrained_tencent
		files = os.listdir(tencent200)
		for v in files:
			predict,undef = execute(v)
			all_predict.update(predict)

		print("word2id length:",len(word2id))
		print("extracted pretrained length:",len(all_predict))
		print("undef length",len(word2id) - len(all_predict))
		print("wirte all predict to ",des)
		with open(des,'wb') as f:
			cPickle.dump(all_predict,f)

	if args.pre_test:
		datadict = fGet_pretrainedWord()
		print len(datadict)
		datadict = fGet_pretrainedWord(path=vpath_pretrained_tencent)
		print len(datadict)


"""
record :
	sgns.target.word-word.dynwin5.thr10.neg5.dim300.iter5
		('word2id length', 96973)
		('word_count length', 332265)
		pre-load file lines count 635964
		counts of word in word2id but not worddict 19688
	命中率:
		(96973-19688)/96973  = 79.7 %

	head -n 1 sgns.target.word-word.dynwin5.thr10.neg5.dim300.iter5
	636013 300
	head -n 1 Tencent_AILab_ChineseEmbedding.txt 
	8824330 200

	比率: 8824330/636013 = 13.87倍

	tencent:
	ka@ka-15-ce0xx:/mnt/u16/home/ka/Documents/proj/aichallenge2018/opinion1$ python painternerCollection/utils.py -pre -prepath tencent
	('word2id length', 96973)
	('word_count length', 332265)
	pre-load file lines count 1103000
	counts of word in word2id but not worddict 8262
	pre-load file lines count 1103000
	counts of word in word2id but not worddict 94747
	pre-load file lines count 1103000
	counts of word in word2id but not worddict 96305
	pre-load file lines count 1103000
	counts of word in word2id but not worddict 96684
	pre-load file lines count 1103000
	counts of word in word2id but not worddict 96832
	pre-load file lines count 1103000
	counts of word in word2id but not worddict 96904
	pre-load file lines count 1103000
	counts of word in word2id but not worddict 96926
	pre-load file lines count 1103331
	counts of word in word2id but not worddict 96937
	('word2id length:', 96973)
	('extracted pretrained length:', 92187)
	('undef length', 4786)
	('wirte all predict to ', 'data/pretrained_word2id_tencent.obj')

	ka@ka-15-ce0xx:/mnt/u16/home/ka/Documents/proj/aichallenge2018/opinion1$ bc
	bc 1.07.1
	Copyright 1991-1994, 1997, 1998, 2000, 2004, 2006, 2008, 2012-2017 Free Software Foundation, Inc.
	This is free software with ABSOLUTELY NO WARRANTY.
	For details type `warranty'.
	96937+96926+96904+96832+96684+96305+94747+8262 - 96973*8
	-92187

	命中率: 92187/96973 = 95.1 %
	说明一点: 这个预训练vec中有</s> ，可能还有<s>, 或许可以来使用当作边界的标记

"""
